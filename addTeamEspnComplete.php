<?php

    require_once("common.php");

    $playersText = isset($_REQUEST["players-text"]) ? $_REQUEST["players-text"] : null;
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : null;
    $shortName = isset($_REQUEST["short-name"]) ? strtoupper(substr(str_replace(" ", "", $_REQUEST["short-name"]), 0, 4)) : strtoupper(substr(str_replace(" ", "", $name), 0, 4));

    $playersAdded = array();

    $teamId = addTeam($name, $shortName);
    if ($teamId) {
        $players = serviceRequestMatchPlayersInString($playersText);

        foreach ($players as $player) {
            $name = nameLastCommaFirstToFirstLast($player["DISPLAY_LAST_COMMA_FIRST"]);

            $playerId = addPlayerToList($player["PERSON_ID"], $name, array($teamId));
            if ($playerId) {
                $playersAdded[] = $name;
            }
        }
    }

    printHeader();
?>

    <h1>Adding an ESPN team</h1>

    <?php

        if ($teamId) {
            echo '<div class="alert alert-success alert-dismissible" role="alert"><strong>Success!</strong> Your ESPN team has been added.</div>';

            if (count($playersAdded) > 0) {
                echo "<p>" . count($playersAdded) . " players were matched:</p>";

                echo "<ul>";
                foreach ($playersAdded as $playerName) {
                    echo "<li>" . $playerName . "</li>";
                }
                echo "</ul>";

                echo "<p>There may be more players on your team that could not be matched. Please check and <a href='searchPlayers.php'>add any others manually</a>.</p>";

                echo "<p>Please note that your team will not regularly synchronise with ESPN. When your roster changes, this will need to be manually
                     updated in <strong>Flance</strong>.</p>";
            }
            else {
                echo "<p>No players could be matched. Please <a href='searchPlayers.php'>add your players manually</a>.</p>";
            }
        }
        else {
            echo '<div class="alert alert-warning alert-dismissible" role="alert">'
                . '<strong>Sorry</strong>, your team can not be added at this time. Please try again, or try <a href="addTeam.php">manually adding your teams</a>.'
                . '</div>';
        }

    ?>

    <p class="breadcrumbs"><a href="editTeams.php">&lt; Back to Edit my teams</a></p>

<?php
    printFooter();
