<?php

    require_once("common.php");

    echo "Starting to update player list.<br />" . PHP_EOL;

    /*
    New way to update this:
    - Go to https://www.nba.com/players
    - Inspect
    - Find the request like https://stats.nba.com/stats/playerindex?College=&Country=&DraftPick=&DraftRound=&DraftYear=&Height=&Historical=1&LeagueID=00&Season=2020-21&SeasonType=Regular%20Season&TeamID=0&Weight=
    - Save the response (you can't open it in browser)

    Replicated this via command line cURL below
    */

    $contents = exec("curl 'https://stats.nba.com/stats/playerindex?College=&Country=&DraftPick=&DraftRound=&DraftYear=&Height=&Historical=0&LeagueID=00&Season=" . NBA_STATS_API_CURRENT_SEASON_CODE . "&SeasonType=Regular%20Season&TeamID=0&Weight=' \
  -H 'Connection: keep-alive' \
  -H 'Pragma: no-cache' \
  -H 'Cache-Control: no-cache' \
  -H 'sec-ch-ua: \"Chromium\";v=\"88\", \"Google Chrome\";v=\"88\", \";Not A Brand\";v=\"99\"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36' \
  -H 'Accept: */*' \
  -H 'Origin: https://www.nba.com' \
  -H 'Sec-Fetch-Site: same-site' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Referer: https://www.nba.com/' \
  -H 'Accept-Language: en-US,en;q=0.9,und;q=0.8' \
  --compressed");

    if ($contents) {
        $json = json_decode($contents, true);
        if (is_array($json)) {
            if (file_put_contents("data/playersList.json", $contents)) {
                echo "Success: updated <a href='data/playersList.json'>playersList.json</a><br />" . PHP_EOL;
            }
            else {
                echo "Error: new file contents could not be saved.<br />" . PHP_EOL;
            }
        }
        else {
            echo "Error: new list service call response is not in the right format.<br />" . PHP_EOL;
        }
    }
    else {
        echo "Error: new list could not be loaded.<br />" . PHP_EOL;
    }

    echo "Done" . PHP_EOL;
