<?php

    require_once("common.php");

    $mode = "add";
    $title = "Add a new team";
    $id = null;

    if (isset($_REQUEST["id"])) {
        $mode = "edit";
        $title = "Edit team";
        $id = $_REQUEST["id"];

        $teams = getTeamsForCurrentUser();
        $team = $teams[$id];
    }

    printHeader();
?>

    <h1><?php echo $title; ?></h1>

    <form class="form-horizontal well" method="post" action="doEditTeam.php">
        <input type="hidden" name="mode" value="<?php echo $mode; ?>" />
        <input type="hidden" name="id" value="<?php echo $id; ?>" />

        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="The full name of the team" value="<?php echo ($team ? $team["name"] : null); ?>" />
          </div>
        </div>
        <div class="form-group">
          <label for="short-name" class="col-sm-2 control-label">Short name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="short-name" name="short-name" placeholder="A short nickname - max 4 characters" value="<?php echo ($team ? $team["short_name"] : null); ?>" aria-describedby="short-name-help" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Save</button>
          </div>
        </div>
    </form>

    <p class="breadcrumbs"><a href="editTeams.php">&lt; Back to Edit my teams</a></p>

<?php
    printFooter();
