<?php

    function writeToDataUpdateLog($message) {
        file_put_contents("data/updateLog.log", date("Y-m-d H:i:s") . " - " . $message . PHP_EOL, FILE_APPEND);

        $quietMode = (isset($_REQUEST["quiet"]) && $_REQUEST["quiet"] == "1");
        if (!$quietMode) {
            echo date("Y-m-d H:i:s") . " - " . $message . PHP_EOL;
        }
    }

    writeToDataUpdateLog("Running data update");

    require_once("common.php");

    writeToDataUpdateLog("Loaded common");

    $dateObject = getEffectiveTodayDateObject();
    //$dateObject = new DateTime("2017-03-31", new DateTimeZone('America/Los_Angeles'));

    writeToDataUpdateLog("Date used: " . $dateObject->format('Y-m-d'));

    writeToDataUpdateLog("Calling scoreboard");
    $scoreboardData = file_get_contents(nbaServiceRequestConfigScoreboardUrl($dateObject));
    if ($scoreboardData) {
        $filename = nbaServiceRequestConfigScoreboardFilename($dateObject);

        if (file_exists($filename)) {
            writeToDataUpdateLog("Checking existing file content");
            $existingData = file_get_contents($filename);
            writeToDataUpdateLog("Data is" . ($scoreboardData != $existingData ? "" : " NOT") . " different");
        }

        writeToDataUpdateLog("Saving scoreboard");
        $result = file_put_contents($filename, $scoreboardData);
        writeToDataUpdateLog("Scoreboard save " . ($result ? "" : "UN") . "successful!");
    }

    // Determine which games to get
    $serviceGamesData = serviceRequestNbaGetGamesData($dateObject);
    if (!isset($serviceGamesData["games"])) {
        writeToDataUpdateLog("No game data found");
    }
    else {
        $gameIds = array_keys($serviceGamesData["games"]);
        if (count($gameIds) == 0) {
            writeToDataUpdateLog("No games found");
        }
        else {
            writeToDataUpdateLog("Found " . count($gameIds) . " games: " . implode(", ", $gameIds));

            foreach ($gameIds as $gameId) {
                $url = nbaServiceRequestConfigBoxscoreUrl($dateObject, $gameId);

                writeToDataUpdateLog("Calling boxscore for game " . $gameId . ". URL is " . $url);
                $boxscoreData = file_get_contents(nbaServiceRequestConfigBoxscoreUrl($dateObject, $gameId));
                if ($boxscoreData) {
                    writeToDataUpdateLog("Saving boxscore");
                    $result = file_put_contents(nbaServiceRequestConfigJsonBoxscoreFilename($dateObject, $gameId), $boxscoreData);
                    writeToDataUpdateLog("Boxscore save " . ($result ? "" : "UN") . "successful!");
                }
                else {
                    writeToDataUpdateLog("Error: Could not load boxscore");
                }
            }
        }
    }

    writeToDataUpdateLog("Updating player news");
    if (!updateRotoworldPlayerNewsFeed()) {
        writeToDataUpdateLog("Error: Could not update news");
    }

    writeToDataUpdateLog("Done");
