<?php

    require_once("common.php");

    if (!isset($_REQUEST["id"])) {
        header("Location: editPlayers.php");
        exit;
    }

    $referrer = (isset($_REQUEST["referrer"]) ? $_REQUEST["referrer"] : "editPlayers.php?edited=1");

    $id = $_REQUEST["id"];
    $teams = isset($_REQUEST["teams"]) ? $_REQUEST["teams"] : array();

    editPlayerTeams($id, $teams);

    header("Location: " . $referrer);
