<?php

define("URL", "flance.invitationstation.org");

define("OAUTH_CONSUMER_KEY", "dj0yJmk9NmhpdVdocFN4MzQ0JmQ9WVdrOWRIZFNlRXBRTXpJbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1hOQ--");
define("OAUTH_CONSUMER_SECRET", "a9bdaee4afed1475a395154a062a8571a1f39064");

//define("NBA_STATS_API_CURRENT_SEASON_CODE", "2018-19");
//define("NBA_STATS_API_PREVIOUS_SEASON_CODE", "2017-18");
define("NBA_STATS_API_CURRENT_SEASON_CODE", "2024-25");
define("NBA_STATS_API_PREVIOUS_SEASON_CODE", "2023-24");
define("NBA_STATS_API_REGULAR_SEASON", "Regular Season");
define("NBA_STATS_API_PRE_SEASON", "Pre Season");
define("NBA_SITE_SEASON_CODE", "2023");
define("YAHOO_API_SEASON_CODE", "385");

define("MATCH_STATUS_UPCOMING", "Upcoming");
define("MATCH_STATUS_IN_PROGRESS", "In progress");
define("MATCH_STATUS_COMPLETE", "Complete");

define("STAT_ID_FG", "FG");
define("STAT_ID_FGM", "FGM");
define("STAT_ID_FGA", "FGA");
define("STAT_ID_FGP", "FG_PCT");
define("STAT_ID_FT", "FT");
define("STAT_ID_FTM", "FTM");
define("STAT_ID_FTA", "FTA");
define("STAT_ID_FTP", "FT_PCT");
define("STAT_ID_FG3", "FG3");
define("STAT_ID_FG3M", "FG3M");
define("STAT_ID_FG3A", "FG3A");
define("STAT_ID_FG3P", "FG3P");
define("STAT_ID_PTS", "PTS");
define("STAT_ID_REB", "REB");
define("STAT_ID_AST", "AST");
define("STAT_ID_STL", "STL");
define("STAT_ID_BLK", "BLK");
define("STAT_ID_TO", "TOV");
define("STAT_ID_MIN", "MIN");
define("STAT_ID_PF", "PF");
define("STAT_ID_GP", "GP");
