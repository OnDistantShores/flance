<?php

    require_once("common.php");

    if (!isset($_REQUEST["id"])) {
        header("Location: editTeams.php");
        exit;
    }

    deleteTeam($_REQUEST["id"]);

    header("Location: editTeams.php?deleted=1");
