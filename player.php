<?php

    require_once("common.php");

    ensureLoggedIn();

    $user = getUser(getSessionParameter("user_id"));

    $referrer = (isset($_REQUEST["referrer"]) ? $_REQUEST["referrer"] : "home.php");

    $error = "";

    $nbaId = isset($_REQUEST["nba_id"]) ? $_REQUEST["nba_id"] : null;
    if (!$nbaId) {
        $error = "incorrect URL";
    }
    else {
        // Find the player's high level details
        $allPlayers = serviceRequestNbaGetPlayerListById();

        if (!isset($allPlayers[$nbaId])) {
            $error = "could not load player details";
        }
        else {
            $playerDetails = $allPlayers[$nbaId];

            //$gameLog = serviceRequestNbaGetPlayerGameLog($nbaId, NBA_STATS_API_CURRENT_SEASON_CODE);
            //$gameLog = array_merge($gameLog, serviceRequestNbaGetPlayerGameLog($nbaId, NBA_STATS_API_CURRENT_SEASON_CODE, NBA_STATS_API_PRE_SEASON));
            /*if (count($gameLog) == 0) {
                // Only show last year's games if that's all we have!
                $gameLog = array_merge($gameLog, serviceRequestNbaGetPlayerGameLog($nbaId, NBA_STATS_API_PREVIOUS_SEASON_CODE));
            }*/

            // Manual workaround for the fact that stats.nba.com won't respond in the live environment!

            $gameLog = array();

            $allFilesInDataDirectory = scandir("data/");
            foreach (array_reverse($allFilesInDataDirectory) as $filename) {
                if (strpos($filename, "new-boxscore") !== false && strpos($filename, "json") !== false) {

                    // Only look at games from the last 2 years
                    if (filemtime("data/" . $filename) > (time() - (2 * 365 * 24 * 60 * 60))) {

                        $jsonString = file_get_contents("data/" . $filename);
                        if ($jsonString) {
                            $players = getBoxscorePlayersFromNbaJson($jsonString);
                            if (isset($players[$playerDetails["PERSON_ID"]])) {
                                if ($players[$playerDetails["PERSON_ID"]]["MIN"] > 0) {
                                    $gamedate = strtotime(substr($filename, 13, 4) . "-" . substr($filename, 17, 2) . "-" . substr($filename, 19, 2));

                                    // Store games by gameId, as sometimes we get duplicate boxscore files on different days for the same game.
                                    // Only keep the first one we find as we're going in reverse chronological order
                                    $gameId = substr($filename, 27, 5);
                                    if (!isset($gameLog[$gameId])) {
                                        $gameLog[$gameId] = array_merge(array(
                                            "GAME_DATE" => date("j/n/y", $gamedate)
                                        ), $players[$playerDetails["PERSON_ID"]]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    $userPlayers = getPlayersForCurrentUser(true);
    $userPlayer = null;
    foreach ($userPlayers as $player) {
        if ($nbaId == $player["nba_id"]) {
            $userPlayer = $player;
            break;
        }
    }

    $userTeams = getTeamsForCurrentUser();

    printHeader();
?>

    <?php if ($error) { ?>
        <div class="alert alert-danger" role="alert">
            There was a problem: <?php echo $error; ?>.
        </div>
    <?php } else { ?>

        <h1><?php echo $playerDetails["PLAYER_FIRST_NAME"] . " " . $playerDetails["PLAYER_LAST_NAME"] . " (" . $playerDetails["TEAM_ABBREVIATION"] . ")"; ?></h1>

        <?php if ($userPlayer): ?>

            <p>
                <form method='post' action='doEditPlayer.php'>
                    <input type='hidden' name='id' value='<?php echo $userPlayer["id"]; ?>' />
                    <input type='hidden' name='referrer' value='player.php?nba_id=<?php echo $nbaId; ?>' />
                    <?php foreach ($userTeams as $team): ?>
                        <label>
                            <input type="checkbox" name="teams[]" value="<?php echo $team["id"]; ?>" <?php echo (in_array($team["id"], $userPlayer["teams"]) ? "checked" : ""); ?> /> <?php echo $team["short_name"]; ?>&nbsp;&nbsp;
                        </label>
                    <?php endforeach; ?>
                    <button type='submit' class='btn btn-primary btn-xs'>Save</button>
                    <a href='doDeletePlayer.php?id=<?php echo $userPlayer["id"]; ?>'><button type='button' class='btn btn-danger btn-xs'>Delete from my list</button></a>
                    <a href='doHidePlayer.php?nba_id=<?php echo $nbaId; ?>'><button type='button' class='btn btn-info btn-xs'><?php echo (!isPlayerHiddenForToday($nbaId) ?  "Hide for today" : "Unhide for today"); ?></button></a>
                </form>
            </p>

        <?php else: ?>

            <p>
                <a href='addPlayer.php?referrer=<?php echo urlencode("player.php?nba_id=" . $nbaId) . "&nba_id=" . $nbaId . " &name=" . urlencode($playerDetails["PLAYER_FIRST_NAME"] . " " . $playerDetails["PLAYER_LAST_NAME"]); ?>'><button type='button' class='btn btn-primary btn-sm'>Add to my list</button></a>
                <a href='doHidePlayer.php?nba_id=<?php echo $nbaId; ?>'><button type='button' class='btn btn-info btn-sm'><?php echo (!isPlayerHiddenForToday($nbaId) ?  "Hide for today" : "Unhide for today"); ?></button></a>
            </p>

        <?php endif; ?>

        <p class="breadcrumbs"><a href="<?php echo $referrer; ?>">&lt; Back</a></p>

        <?php if (count($gameLog) == 0) { ?>
            <div class="alert alert-info" role="alert">
                No game history could be found for this player.
            </div>
        <?php } else { ?>
            <div class="row">
                <table class="table playersData">
                    <tr>
                        <th>Match</th>
                        <?php printGameDataHeaders($user["show_turnovers"]); ?>
                    </tr>
                    <?php

                        foreach ($gameLog as $game) {
                            $gameDateChunks = explode(", ", $game["GAME_DATE"]);
                            $matchupChunks = explode(" ", $game["MATCHUP"]);

                            $totalLineValue = getFullLineStatRankingValue($game, $user["show_turnovers"]);
                            $colorString = getCssColourStringForStatRankingValue($totalLineValue);

                            echo "<td class='match-info'" . ($user["show_colourful_view"] ? " style='background-color: " . $colorString . ";'" : "") . ">" . $gameDateChunks[0] . " " . $matchupChunks[1] . " " . $matchupChunks[2] . "</td>" . PHP_EOL;

                            if ($user["show_colourful_view"]) {
                                printGameDataRowColourful($game, $user["show_turnovers"]);
                            }
                            else {
                                printGameDataRowSimple($game, $user["show_turnovers"]);
                            }

                            echo "</tr>" . PHP_EOL;
                        }
                    ?>
                </table>
            </div>
        <?php } ?>

    <?php } ?>

<?php
    printFooter();
