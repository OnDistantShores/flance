<?php

    require_once("common.php");

    $existingUserId = getSessionParameter("user_id");
    $password = isset($_REQUEST["password"]) ? $_REQUEST["password"] : null;
    $confirmPassword = isset($_REQUEST["confirm-password"]) ? $_REQUEST["confirm-password"] : null;

    $errorMessage = null;
    if (strlen($password) < 6 || strlen($password) > 20) {
        $errorMessage = "the password is not between 6 and 20 characters";
    }
    else if ($password != $confirmPassword) {
        $errorMessage = "the passwords do not match";
    }

    if ($errorMessage) {
        header("Location: settings.php?error=" . urlencode($errorMessage));
        exit;
    }

    if (editPassword($existingUserId, $password)) {
        header("Location: settings.php?passwordChanged=1");
    }
    else {
        header("Location: settings.php?error=" . urlencode("internal error, please try again later"));
    }