<?php

    require_once("common.php");

    if (!isset($_REQUEST["mode"])) {
        header("Location: editTeams.php");
        exit;
    }

    $mode = $_REQUEST["mode"];
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : null;
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : null;
    $shortName = isset($_REQUEST["short-name"]) ? strtoupper(substr(str_replace(" ", "", $_REQUEST["short-name"]), 0, 4)) : strtoupper(substr(str_replace(" ", "", $name), 0, 4));

    if ($mode == "edit") {
        if (!$id) {
            header("Location: editTeams.php");
            exit;
        }

        editTeam($id, $name, $shortName);

        header("Location: editTeams.php?edited=1");
    }
    else {
        addTeam($name, $shortName);

        header("Location: editTeams.php?added=1");
    }
