<?php

    require_once("common.php");

    printHeader();
?>
    <div class="jumbotron">
        <h1>Flance</h1>

        <p>It's your fantasy basketball life at a glance. It's the one place to keep track of all your players of interest.</p>
    </div>

    <div class="promo row">
        <div class="col-md-4 col-md-offset-1 col-sm-6">
            <div class="screenshot"><img src="images/sampleScreenshot2.png" class="img-responsive" alt="Flance dashboard example" /></div>
        </div>
        <div class="col-md-6 col-sm-6">
            <p><ul class="features">
                <li>See all players from your Yahoo, ESPN and other teams all in one place</li>
                <li>Optimised views for both mobile and desktop</li>
                <li>View the most relevant stat lines first</li>
                <li>Colour coded stat lines means it's easy to tell who is doing well and who isn't</li>
                <li>Track players from your teams and your custom watch list</li>
                <li>See a list of the day's best lines from throughout the league</li>
                <li>Easily view players' game logs on the go</li>
            </ul></p>

            <?php if (!getSessionParameter("user_id")): ?>
                <div class="buttons">
                    <p>
                        <a href='login.php'><button class="btn btn-primary">Log in</button></a>
                        &nbsp;&nbsp;or&nbsp;&nbsp; <a href='setupUser.php'><button class="btn btn-primary">Create a new account</button></a>
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php
    printFooter();
