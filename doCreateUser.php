<?php

    require_once("common.php");

    $existingUserId = getSessionParameter("user_id");
    $email = isset($_REQUEST["email"]) ? $_REQUEST["email"] : null;
    $password = isset($_REQUEST["password"]) ? $_REQUEST["password"] : null;
    $confirmPassword = isset($_REQUEST["confirm-password"]) ? $_REQUEST["confirm-password"] : null;

    $errorMessage = null;
    if (checkIfUserByEmailExists($email)) {
        $errorMessage = "a user for this email address already exists";
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errorMessage = "the email address is not in the correct format";
    }
    else if (strlen($password) < 6 || strlen($password) > 20) {
        $errorMessage = "the password is not between 6 and 20 characters";
    }
    else if ($password != $confirmPassword) {
        $errorMessage = "the passwords do not match";
    }

    if ($errorMessage) {
        header("Location: setupUser.php?email=" . urlencode($email) . "&error=" . urlencode($errorMessage));
        exit;
    }

    if ($existingUserId) {
        if (addLoginCredentialsToUser($existingUserId, $email, $password)) {
            header("Location: home.php?userCreated=1");
        }
        else {
            header("Location: setupUser.php?email=" . urlencode($email) . "&error=" . urlencode("internal error, please try again later"));
        }
    }
    else {
        $newUser = createUserFromEmailPassword($email, $password);
        if ($newUser) {
            $_SESSION["user_id"] = $newUser["id"];

            header("Location: home.php?userCreated=1");
        }
        else {
            header("Location: setupUser.php?email=" . urlencode($email) . "&error=" . urlencode("internal error, please try again later"));
        }
    }
