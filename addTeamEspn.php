<?php

    require_once("common.php");

    printHeader();
?>

    <h1>Add an ESPN team</h1>

    <div class="row">
        <div class="col-md-6">
            <p>To add your team, simply copy and paste the list of players from your "My team" tab on the ESPN website. Just like in the picture below, copy from your
                players list. It doesn't matter where you select exactly, just make sure you capture all of the players' names. Paste it in the field below - it may look like a mess,
                but that's OK. Enter the team name and "short name", then just hit 'Save'.</p>

            <div class="screenshot"><img src="images/espnScreenshot.jpg" class="img-responsive" alt="ESPN 'My team' tab" /></div>
        </div>
        <div class="col-md-6">
            <form class="form-horizontal well" method="post" action="addTeamEspnComplete.php">

                <div class="form-group">
                  <label for="players-text" class="col-sm-2 control-label">Content page content</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="players-text" name="players-text" rows="4"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="The full name of the team" value="<?php echo ($team ? $team["name"] : null); ?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="short-name" class="col-sm-2 control-label">Short name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="short-name" name="short-name" placeholder="A short nickname - max 4 characters" value="<?php echo ($team ? $team["short_name"] : null); ?>" aria-describedby="short-name-help" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Save</button>
                  </div>
                </div>
            </form>
        </div>
    </div>

    <p class="breadcrumbs"><a href="editTeams.php">&lt; Back to Edit my teams</a></p>

<?php
    printFooter();
