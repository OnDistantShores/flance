<?php

require_once("common.php");

if ($_REQUEST["pass"] != "boomshakalaka") {
    echo "Access denied";
    exit;
}

function getRow($sql) {
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        if ($r = mysqli_fetch_assoc($q)) {
            return $r;
        }
    }
    return null;
}

function getRows($sql) {
    $rows = array();
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        while ($r = mysqli_fetch_assoc($q)) {
            $rows[] = $r;
        }
    }
    return $rows;
}

function getOneValue($sql) {
    if ($r = getRow($sql)) {
        $keys = array_keys($r);
        return $r[$keys[0]];
    }
    return null;
}
?>
<html>
    <head>
        <title>Flance dashboard</title>
    </head>
    <body>
        <h2>Flance dashboard</h2>

        <h3>Stats</h3>

        <?php

            $sql = " SELECT COUNT(*) FROM user ";
            echo "<p>Total users: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM user WHERE date_registered > DATE_SUB(NOW(), INTERVAL 24 HOUR) ";
            echo "<p>Total users registered in the last 24 hours: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM user WHERE email IS NOT NULL ";
            echo "<p>Total users with new login setup: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM user WHERE date_last_load > DATE_SUB(NOW(), INTERVAL 24 HOUR) ";
            echo "<p>Total users logged in in the last 24 hours: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM team ";
            echo "<p>Total teams: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM (SELECT user_id, COUNT(*) FROM team GROUP BY user_id HAVING COUNT(*) >= 2) temp ";
            echo "<p>Total users with 2 or more teams: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM player ";
            echo "<p>Total players: " . getOneValue($sql) . "</p>" . PHP_EOL;

            $sql = " SELECT COUNT(*) FROM (SELECT team_id, COUNT(*) FROM player_team GROUP BY team_id HAVING COUNT(*) >= 10) temp ";
            echo "<p>Total teams with 10 or more players: " . getOneValue($sql) . "</p>" . PHP_EOL;

        ?>

        <h3>Actions</h3>

        <div><a href="dataUpdate.php">Force a data update</a></div>
        <div><a href="data/updateLog.log">View the data update log</a></div>
        <div><a href="dataUpdate.php">Update player IDs</a></div>
        <div><a href="updatePlayerList.php">Update player list</a></div>


        <?php

            $dateObject = new DateTime("now", new DateTimeZone('America/Los_Angeles'));

            $filename = nbaServiceRequestConfigScoreboardFilename($dateObject);
            echo '<div><a href="' . $filename . '">Get the current Flance scoreboard JSON</a></div>';

            echo '<div><a href="http://stats.nba.com/stats/scoreboard/?GameDate=' . $dateObject->format('m/d/Y') . '&LeagueID=00&DayOffset=0&unique=' . microtime(true) . '">Get the current stats.nba.com scoreboard JSON</a></div>';

        ?>

    </body>
</html>
