<?php

//
////////// My custom functions //////////
//

////// Basketball stuff

// Returns the time in the game as a percentage number
// E.g. game not started = 0
// E.g. game finished = 100
// E.g. Q2, 6 minutes in = 37(.5)
// E.g. game in OT = 100 (because we don't know how much more is left)
function getGameTimePercentage($quarter, $clock) {
    $maxMinuteValue = 4 * 13;
    $value = 0;
    if ($quarter) {
        if ($quarter >= 1 && $quarter <= 4) {
            $value += (($quarter - 1) * 13);
            if ($clock) {
                if (strpos($clock, ":") !== false) {
                    list($minutes, $seconds) = explode(":", $clock);
                    if (!$minutes) {
                        $minutes = 0;
                    }
                    $value += (12 - $minutes);
                }
                else {
                    $value += 12;
                }
            }
            else if (($quarter == 1 || $quarter == 2 || $quarter == 3) && !$clock) {
                // Half time doesn't show a time for some reason, just says "Q2"
                $value += 13;
            }
        }
        else if (strpos($quarter, "OT") !== false || $quarter > 4) {
            $value = $maxMinuteValue;
        }
    }
    return round(($value / $maxMinuteValue) * 100);
}


function printGameDataHeaders($showTurnovers = false) {
    ?>
        <th class='data'>FG</th>
        <th class='data'>FT</th>
        <th class='data'>3PM</th>
        <th class='data'>PTS</th>
        <th class='data'>REB</th>
        <th class='data'>AST</th>
        <th class='data'>STL</th>
        <th class='data'>BLK</th>
        <?php if ($showTurnovers) echo "<th class='data'>TO</th>"; ?>
        <th class='data'>M</th>
        <th class='data'>F</th>
    <?php
}

function getStatsConfig($showTurnovers = false) {
    $statsConfig = array(
        STAT_ID_FGP => array(
            "type" => "percentage",
            "direction" => "positive",
            "cssClassExtension" => "fg",
            "madeTotalId" => STAT_ID_FGM,
            "attemptedTotalId" => STAT_ID_FGA,
            "average" => 0.459,
            "stddev" => 0.06, // Increase this to make sure the scaling isn't too dramatic
            "attemptedAverage" => 14, // Increase this to make sure the scaling isn't too dramatic
            //"attemptedStddev" => 3.6,
            "attemptedStddev" => 4, // Increase this to make sure the scaling isn't too dramatic
            "simpleAverageStart" => 0.401,
            "simpleAverageEnd" => 0.499,
        ),
        STAT_ID_FTP => array(
            "type" => "percentage",
            "direction" => "positive",
            "cssClassExtension" => "ft",
            "madeTotalId" => STAT_ID_FTM,
            "attemptedTotalId" => STAT_ID_FTA,
            "average" => 0.795,
            //"stddev" => 0.091,
            "stddev" => 0.150, // Increase this to make sure numbers like 1/4 don't look too dramatic
            "attemptedAverage" => 4, // Increase this to make sure the scaling isn't too dramatic
            //"attemptedStddev" => 1.8,
            "attemptedStddev" => 3, // Increase this to make sure the scaling isn't too dramatic
            "simpleAverageStart" => 0.601,
            "simpleAverageEnd" => 0.799,
        ),
        STAT_ID_FG3M => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "3pt",
            "average" => 1.3,
            "stddev" => 0.8,
            "simpleAverageStart" => 1,
            "simpleAverageEnd" => 1,
        ),
        STAT_ID_PTS => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "pts",
            "average" => 13.3,
            "stddev" => 4.3,
            "simpleAverageStart" => 9,
            "simpleAverageEnd" => 15,
        ),
        STAT_ID_REB => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "reb",
            "average" => 5,
            "stddev" => 2.7,
            "simpleAverageStart" => 4,
            "simpleAverageEnd" => 9,
        ),
        STAT_ID_AST => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "ast",
            "average" => 2.6,
            "stddev" => 2.1,
            "simpleAverageStart" => 2,
            "simpleAverageEnd" => 4,
        ),
        STAT_ID_STL => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "stl",
            "average" => 1,
            "stddev" => 0.4,
            "simpleAverageStart" => 1,
            "simpleAverageEnd" => 1,
        ),
        STAT_ID_BLK => array(
            "type" => "integer",
            "direction" => "positive",
            "cssClassExtension" => "blk",
            "average" => 0.5,
            "stddev" => 0.6,
            "simpleAverageStart" => 1,
            "simpleAverageEnd" => 1,
        ),
    );

    if ($showTurnovers) {
        $statsConfig[STAT_ID_TO] = array(
            "type" => "integer",
            "direction" => "negative",
            "cssClassExtension" => "to",
            "average" => 1,
            "stddev" => 1,
            "simpleAverageStart" => 1,
            "simpleAverageEnd" => 1,
        );
    }

    return $statsConfig;
}

function printGameDataRowSimple($data, $showTurnovers = false, $percentageGameComplete = 100, $overNumberOfGames = 1) {
    $statsConfig = getStatsConfig($showTurnovers);

    foreach ($statsConfig as $statId => $statConfig) {

        // If we have specified how far through the game we are, then we need to calculate what the expected average & SD would be, relative to game progress
        if ($percentageGameComplete != null && $statConfig["type"] != "percentage") {
            $statConfig["simpleAverageStart"] = $statConfig["simpleAverageStart"] * ($percentageGameComplete / 100);
            $statConfig["simpleAverageEnd"] = $statConfig["simpleAverageEnd"] * ($percentageGameComplete / 100);
        }

        $dataValue = $data[$statId];
        // If this is over a number of games, make it an average
        if ($overNumberOfGames > 1) {
            // Percentages are already an average
            if ($statConfig["type"] != "percentage") {
                $dataValue = round($dataValue / $data[STAT_ID_GP], 1);
            }
        }

        $class = "average-value";
        if ($statConfig["direction"] == "negative") {
            if ($data[$statId] < $statConfig["simpleAverageStart"]) {
                $class = "good-value";
            }
            else if ($data[$statId] > $statConfig["simpleAverageEnd"]) {
                $class = "bad-value";
            }
        }
        else {
            if ($data[$statId] < $statConfig["simpleAverageStart"]) {
                $class = "bad-value";
            }
            else if ($data[$statId] > $statConfig["simpleAverageEnd"]) {
                $class = "good-value";
            }
        }

        $displayValue = $data[$statId];
        if ($statConfig["type"] == "percentage") {
            $displayValue = $data[$statConfig["madeTotalId"]] . "/" . $data[$statConfig["attemptedTotalId"]];
        }

        echo "<td class='data stat-" . $statConfig["cssClassExtension"] . "'><span class='" . $class . "'>" . $displayValue . "</span></td>" . PHP_EOL;
    }

    echo "<td class='data stat-min'>" . $data[STAT_ID_MIN] . "</td>" . PHP_EOL;
    echo "<td class='data stat-pf'>" . $data[STAT_ID_PF] . "</td>" . PHP_EOL;
}

function printGameDataRowColourful($data, $showTurnovers = false, $percentageGameComplete = 100, $overNumberOfGames = 1) {
    $statsConfig = getStatsConfig($showTurnovers);

    foreach ($statsConfig as $statId => $statConfig) {
        if (isset($data[$statId])) {
            // Calculate how good or bad this number is

            $statRankingValue = getStatRankingValue($data[$statId], $statConfig["average"], $statConfig["stddev"],
                                             $percentageGameComplete,
                                             $overNumberOfGames,
                                             ($statConfig["direction"] != "negative"),
                                             ($statConfig["type"] == "percentage"),
                                             (isset($statConfig["attemptedTotalId"]) ? $data[$statConfig["attemptedTotalId"]] : null),
                                             (isset($statConfig["attemptedAverage"]) ? $statConfig["attemptedAverage"] : null),
                                             (isset($statConfig["attemptedStddev"]) ? $statConfig["attemptedStddev"] : null));

            $colorString = getCssColourStringForStatRankingValue($statRankingValue);

            // Display the line

            $dataValue = $data[$statId];
            if ($statConfig["type"] == "percentage") {
                $dataValue = "<span class='data-percentage view-made-attempts'>" . $data[$statConfig["madeTotalId"]] . "/" . $data[$statConfig["attemptedTotalId"]] . "</span>"
                    . "<span class='data-percentage view-percentage' style='display: none;'>" . number_format($data[$statId], 3) . "</span>";
            }

            echo "<td class='data stat-" . $statConfig["cssClassExtension"] . "' style='background-color: " . $colorString . ";'>" . $dataValue . "</td>" . PHP_EOL;
        }
    }

    echo "<td class='data stat-min'>" . $data[STAT_ID_MIN] . "</td>" . PHP_EOL;
    echo "<td class='data stat-pf'>" . $data[STAT_ID_PF] . "</td>" . PHP_EOL;
}

function getCssColourStringForStatRankingValue($statRankingValue) {
    // Calculate the colors for this. 0 is red, 120 is green. Let's not go all the way to full red, it looks terrible.

    $redToGreenScale = round((115 * ($statRankingValue / 100)), 2) + 5;
    return "hsl(" . $redToGreenScale . ", 100%, 50%)";

    /*
    if ($statRankingValue == 50) {
        return "rgb(255, 255, 255)";

        //return "rgb(189, 189, 189)";
    }
    else if ($statRankingValue > 50) {
        $green = 255 - ((255 * ($statRankingValue - 50)) / 50);
        return "rgb(" . round($green) . ", 255, " . round($green) . ")";

        //$percentage = ($statRankingValue - 50) / 50;
        //return "rgb(" . round(189 * (1 - $percentage)) . ", " . round(189 + (66 * $percentage)) . ", " . round(189 * (1 - $percentage)) . ")";
    }
    else if ($statRankingValue < 50) {
        $red = 255 - ((255 * (50 - $statRankingValue)) / 50);
        return "rgb(255, " . round($red) . ", " . round($red) . ")";

        //$percentage = ($statRankingValue - 50) / 50;
        //return "rgb(" . round(189 * (1 - $percentage)) . ", " . round(189 + (66 * $percentage)) . ", " . round(189 * (1 - $percentage)) . ")";
    }
    */
}

// The last 3 params are only relevant when $isPercent == true
// Result is expressed as a percentage. 50 is average, 100 is best, 0 is worst.
function getStatRankingValue($stat, $average, $standardDeviation,
                             $percentageGameComplete = null, $overNumberOfGames = 1,
                             $isPositive = true,
                             $isPercent = false, $statAttempts = null, $attemptsAverage = null, $attemptsStandardDeviation = null) {

    // If this is over a number of games, make it an average
    if ($overNumberOfGames > 1) {
        // Percentages are already an average
        if (!$isPercent) {
            $stat = round($stat / $overNumberOfGames, 1);
        }
    }

    // This determines the range of extremes. The best possible ranking is given to numbers {$stddevsForMaxOrMinValue} times the standard deviation from the average.
    $stddevsForMaxOrMinValue = 10;

    // If we have specified how far through the game we are, then we need to calculate what the expected average & SD would be, relative to game progress
    if ($percentageGameComplete != null && !$isPercent) {
        $average = $average * ($percentageGameComplete / 100);
        $standardDeviation = $standardDeviation * ($percentageGameComplete / 100);
    }

    $diffFromAverage = $stat - $average;
    if ($isPercent && $statAttempts == 0) {
        $diffFromAverage = 0;
    }

    $diffFromAverageStdDev = $diffFromAverage / $standardDeviation;

    // Percentages need to be weighted by attempts if it's just for one game
    if ($isPercent && $overNumberOfGames == 1) {

        // Work out how far above/below the average the attempts are
        $attemptsDiffFromAverage = $statAttempts - $attemptsAverage;
        $attemptsDiffFromAverageStdDev = $attemptsDiffFromAverage / $attemptsStandardDeviation;

        // Standard multiplier is exactly 1 - no change
        $multiplier = 1;
        if ($attemptsDiffFromAverageStdDev > 0) {
            // A standard deviation diff above 0 means we want to increase by this weighting. This will produce a multiplier above 1.
            $multiplier = 1 + $attemptsDiffFromAverageStdDev;
        }
        else {
            // A standard deviation diff below 0 means we need to decrease by this weighting. This will produce a multiplier between 0 and 1.
            $multiplier = 1 / (1 + (-1 * $attemptsDiffFromAverageStdDev));
        }

        $diffFromAverageStdDev = $diffFromAverageStdDev * $multiplier;
    }

    // Put a cap on how extreme the values are - no more than {$stddevsForMaxOrMinValue} standard deviations
    if ($diffFromAverageStdDev > $stddevsForMaxOrMinValue) $diffFromAverageStdDev = $stddevsForMaxOrMinValue;
    if ($diffFromAverageStdDev < (-1 * $stddevsForMaxOrMinValue)) $diffFromAverageStdDev = (-1 * $stddevsForMaxOrMinValue);

    if (!$isPositive) {
        $diffFromAverageStdDev = -1 * $diffFromAverageStdDev;
    }

    // Make it on a scale 0-100. 50 is average, so 100 is {$stddevsForMaxOrMinValue} times the average
    return round(($diffFromAverageStdDev + $stddevsForMaxOrMinValue) * (100 / ($stddevsForMaxOrMinValue * 2)));
}

function getFullLineStatRankingValue($gameData, $showTurnovers = false, $percentageGameComplete = 100, $overNumberOfGames = 1) {
    $total = 0;
    $statsConfig = getStatsConfig($showTurnovers);

    foreach ($statsConfig as $statId => $statConfig) {
        if (isset($gameData[$statId])) {
            $statRankingValue = getStatRankingValue($gameData[$statId], $statConfig["average"], $statConfig["stddev"],
                                             $percentageGameComplete,
                                             $overNumberOfGames,
                                             ($statConfig["direction"] != "negative"),
                                             ($statConfig["type"] == "percentage"),
                                             (isset($statConfig["attemptedTotalId"]) ? $gameData[$statConfig["attemptedTotalId"]] : null),
                                             (isset($statConfig["attemptedAverage"]) ? $statConfig["attemptedAverage"] : null),
                                             (isset($statConfig["attemptedStddev"]) ? $statConfig["attemptedStddev"] : null));
            $total += $statRankingValue;
        }
    }

    // Determine the average value for this line across all categories
    $playerValue = $total / count($statsConfig);

    // It's not realistic to expect lines to be so ridiculous in every category that they get 0 or 100. We need to normalise these a bit so that full line colours are more meaningful.
    // Rationalise the value to the floor & ceiling values, using 50 as the average/midway point in the spectrum
    $floor = 30;
    $ceiling = 70;

    $averageValue = 50;
    if ($playerValue < $averageValue) {
        if ($playerValue < $floor) $playerValue = $floor;

        $weightedValue = ($averageValue - $playerValue) / ($averageValue - $floor);
        $playerValue = $averageValue - ($weightedValue * $averageValue);
    }
    else if ($playerValue > 50) {
        if ($playerValue > $ceiling) $playerValue = $ceiling;

        $weightedValue = ($playerValue - $averageValue) / ($ceiling - $averageValue);
        $playerValue = $averageValue + ($weightedValue * $averageValue);
    }

    return $playerValue;
}

////// DB stuff

global $dbConnection;
function getDbConnection() {
    global $dbConnection;
    return $dbConnection;
}

function connectToDb() {
    global $dbConnection;
    $dbConnection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD);
    if (!$dbConnection) {
        die("Sorry, could not connect to DB server! Error = " . mysqli_connect_error() . PHP_EOL);
    }
    if (!mysqli_select_db(getDbConnection(), DB_NAME)) {
        die("Sorry, could not select database! Error = " . mysqli_connect_error() . PHP_EOL);
    }
}

function getUser($id) {
    $sql = "SELECT id, email, yahoo_guid, league_key, show_turnovers, show_colourful_view, show_match_scores, show_ranking_by_time FROM user WHERE id = '" . mysqli_real_escape_string(getDbConnection(), $id) . "'";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        if ($r = mysqli_fetch_assoc($q)) {
            return array(
                "id" => $r["id"],
                "email" => $r["email"],
                "yahoo_guid" => $r["yahoo_guid"],
                "league_key" => $r["league_key"],
                "show_turnovers" => ($r["show_turnovers"] ? true : false),
                "show_colourful_view" => ($r["show_colourful_view"] ? true : false),
                "show_match_scores" => ($r["show_match_scores"] ? true : false),
                "show_ranking_by_time" => ($r["show_ranking_by_time"] ? true : false),
            );
        }
    }
    return null;
}

function validateAndGetUser($email, $password) {
    $sql = "SELECT id, email, show_turnovers, show_colourful_view, show_match_scores, show_ranking_by_time FROM user WHERE email = '" . mysqli_real_escape_string(getDbConnection(), $email) . "' AND password = '" . md5($password) . "'";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        if ($r = mysqli_fetch_assoc($q)) {
            return array(
                "id" => $r["id"],
                "email" => $r["email"],
                "show_turnovers" => ($r["show_turnovers"] ? true : false),
                "show_colourful_view" => ($r["show_colourful_view"] ? true : false),
                "show_match_scores" => ($r["show_match_scores"] ? true : false),
                "show_ranking_by_time" => ($r["show_ranking_by_time"] ? true : false),
            );
        }
    }
    return null;
}

function checkIfUserByEmailExists($email) {
    $sql = "SELECT email FROM user WHERE email = '" . mysqli_real_escape_string(getDbConnection(), $email) . "'";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        if ($r = mysqli_fetch_assoc($q)) {
            return true;
        }
    }
    return false;
}

function addLoginCredentialsToUser($userId, $email, $password) {
    $sql = "UPDATE user "
            . " SET email = '" . mysqli_real_escape_string(getDbConnection(), $email) . "', "
                . " password = '" . md5($password) . "', "
                . " date_last_login = '" . gmdate("Y-m-d H:i:s") . "' "
            . " WHERE id = " . $userId;
    return mysqli_query(getDbConnection(), $sql);
}

function editPassword($userId, $password) {
    $sql = "UPDATE user "
            . " SET password = '" . md5($password) . "' "
            . " WHERE id = " . $userId;
    return mysqli_query(getDbConnection(), $sql);
}

function createUserFromEmailPassword($email, $password) {
    $sql = "INSERT INTO user (email, password, date_registered, date_last_login) "
            . " VALUES ('" . mysqli_real_escape_string(getDbConnection(), $email) . "', '" . md5($password) . "', '" . gmdate("Y-m-d H:i:s") . "', '" . gmdate("Y-m-d H:i:s") . "')";
    if (mysqli_query(getDbConnection(), $sql)) {
        return array(
            "id" => mysqli_insert_id(getDbConnection()),
            "email" => $email,
            "show_turnovers" => false,
        );
    }

    return null;
}

function updateUserLastLogin() {
    $userId = getSessionParameter("user_id");

    $sql = "UPDATE user "
            . " SET date_last_login = '" . gmdate("Y-m-d H:i:s") . "' "
            . " WHERE id = " . $userId;
    mysqli_query(getDbConnection(), $sql);
}
function updateUserLastLoad() {
    $userId = getSessionParameter("user_id");

    $sql = "UPDATE user "
            . " SET date_last_load = '" . gmdate("Y-m-d H:i:s") . "' "
            . " WHERE id = " . $userId;
    mysqli_query(getDbConnection(), $sql);
}

function updateUserEmail($email) {
    $userId = getSessionParameter("user_id");

    $sql = "UPDATE user "
            . " SET email = '" . mysqli_real_escape_string(getDbConnection(), $email) . "' "
            . " WHERE id = " . $userId;
    mysqli_query(getDbConnection(), $sql);
}

function getOrCreateUserFromYahooGuid($yahooGuid) {
    $sql = "SELECT id, yahoo_guid, league_key, show_turnovers FROM user WHERE yahoo_guid = '" . mysqli_real_escape_string(getDbConnection(), $yahooGuid) . "'";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        if (mysqli_num_rows($q) > 0) {
            if ($r = mysqli_fetch_assoc($q)) {
                return array(
                    "id" => $r["id"],
                    "yahoo_guid" => $r["yahoo_guid"],
                    "email" => $r["email"],
                    "league_key" => $r["league_key"],
                    "show_turnovers" => ($r["show_turnovers"] ? true : false),
                );
            }
        }
        else {
            $leagues = serviceRequestYahooGetLeagues();
            $defaultLeague = $leagues[0];

            $sql = "INSERT INTO user (yahoo_guid, date_registered, date_last_login, league_key) "
                    . " VALUES ('" . mysqli_real_escape_string(getDbConnection(), $yahooGuid) . "', '" . gmdate("Y-m-d H:i:s") . "', '" . gmdate("Y-m-d H:i:s") . "', '" . $defaultLeague["league_key"] . "')";
            if (mysqli_query(getDbConnection(), $sql)) {
                return array(
                    "id" => mysqli_insert_id(getDbConnection()),
                    "email" => null,
                    "yahoo_guid" => $yahooGuid,
                    "league_key" => $defaultLeague["league_key"],
                    "show_turnovers" => false,
                );
            }
        }
    }
    return null;
}

function addPlayerToList($nbaId, $name = null, $teams = array()) {
    $playerId = null;
    $userId = getSessionParameter("user_id");

    // Check if the player is already on this user's list
    $sql = "SELECT id FROM player WHERE user_id = " . $userId . " AND nba_id = " . $nbaId;
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        while ($r = mysqli_fetch_assoc($q)) {
            $playerId = $r["id"];
        }
    }

    // Add the player if they don't already exist
    if (!$playerId) {
        $sql = "INSERT INTO player (user_id, nba_id, name) VALUES ('" . $userId . "', '" . mysqli_real_escape_string(getDbConnection(), $nbaId) . "', " . ($name ? "'" . mysqli_real_escape_string(getDbConnection(), $name) . "'" : "NULL") . ")";
        if (mysqli_query(getDbConnection(), $sql)) {
            $playerId = mysqli_insert_id(getDbConnection());
        }
    }

    // Assuming the above worked, add all the relevant teams
    if ($playerId) {
        foreach ($teams as $teamId) {
            if ($teamId != NULL) {
                $sql = "INSERT INTO player_team (player_id, team_id) VALUES ('" . $playerId . "', '" . $teamId . "')";
                mysqli_query(getDbConnection(), $sql);
            }
        }
    }

    return $playerId;
}

function editPlayerTeams($playerId, $teams = array()) {
    $sql = "DELETE FROM player_team WHERE player_id = " . $playerId;
    mysqli_query(getDbConnection(), $sql);

    foreach ($teams as $teamId) {
        $sql = "INSERT INTO player_team (player_id, team_id) VALUES ('" . $playerId . "', '" . $teamId . "')";
        mysqli_query(getDbConnection(), $sql);
    }
}

function addTeam($name, $shortName, $yahooTeamKey = null) {
    $userId = getSessionParameter("user_id");

    $sql = "INSERT INTO team (user_id, name, short_name, yahoo_team_key) "
        . " VALUES ('" . $userId . "', '" . mysqli_real_escape_string(getDbConnection(), $name) . "', '" . mysqli_real_escape_string(getDbConnection(), strtoupper($shortName)) . "', " . ($yahooTeamKey ? "'" . $yahooTeamKey . "'" : "NULL") . ")";
    if (mysqli_query(getDbConnection(), $sql)) {
        return mysqli_insert_id(getDbConnection());
    }

    return NULL;
}

function editTeam($id, $name, $shortName) {
    $sql = "UPDATE team "
            . " SET name = '" . mysqli_real_escape_string(getDbConnection(), $name) . "', "
                . " short_name = '" . mysqli_real_escape_string(getDbConnection(), strtoupper($shortName)) . "' "
            . " WHERE id = " . $id;
    mysqli_query(getDbConnection(), $sql);
}

function editSettings($showTurnovers, $showColourfulView, $showMatchScores, $showRankingByTime) {
    $userId = getSessionParameter("user_id");

    $sql = "UPDATE user "
            . " SET show_turnovers = '" . mysqli_real_escape_string(getDbConnection(), $showTurnovers) . "', "
                . " show_colourful_view = '" . mysqli_real_escape_string(getDbConnection(), $showColourfulView) . "', "
                . " show_match_scores = '" . mysqli_real_escape_string(getDbConnection(), $showMatchScores) . "', "
                . " show_ranking_by_time = '" . mysqli_real_escape_string(getDbConnection(), $showRankingByTime) . "' "
            . " WHERE id = " . $userId;
    mysqli_query(getDbConnection(), $sql);
}

function deleteTeam($id) {
    $sql = "DELETE FROM team "
            . " WHERE id = " . $id;
    mysqli_query(getDbConnection(), $sql);
}

function deletePlayer($id) {
    $sql = "DELETE FROM player "
            . " WHERE id = " . $id;
    mysqli_query(getDbConnection(), $sql);
}

function deleteAllPlayers() {
    $userId = getSessionParameter("user_id");

    $sql = "DELETE FROM player "
            . " WHERE user_id = " . $userId;
    mysqli_query(getDbConnection(), $sql);
}

function getTeamsForCurrentUser() {
    $teams = array();

    $userId = getSessionParameter("user_id");

    $sql = "SELECT id, short_name, name, yahoo_team_key FROM team WHERE user_id = " . $userId;
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        while ($r = mysqli_fetch_assoc($q)) {
            $teams[$r["id"]] = array(
                "id" => $r["id"],
                "short_name" => $r["short_name"],
                "name" => $r["name"],
                "yahoo_team_key" => $r["yahoo_team_key"],
            );
        }
    }

    return $teams;
}

function getYahooLeagueKeyFromTeamKey($teamKey) {
    $teamKeyParts = explode('.', $teamKey);
    return implode('.', array_slice($teamKeyParts, 0, 3));
}

function getSyncedLeagueDataFileName($leagueKey) {
    return 'data/yahooLeagueData-' . $leagueKey . '.json';
}

function getSyncedLeagueData($userTeams) {
    $syncedLeagueData = array();

    foreach ($userTeams as $userTeam) {
        if ($userTeam["yahoo_team_key"]) {
            $leagueKey = getYahooLeagueKeyFromTeamKey($userTeam["yahoo_team_key"]);

            $leagueSyncDataFileName = getSyncedLeagueDataFileName($leagueKey);
            if (file_exists($leagueSyncDataFileName)) {
                $leagueDataFileContents = file_get_contents($leagueSyncDataFileName);
                if ($leagueDataFileContents) {
                    $leagueDataJson = json_decode($leagueDataFileContents, true);
                    if ($leagueDataJson && is_array($leagueDataJson) && $leagueDataJson["teams"]) {
                        $leagueData = array(
                            "ownedPlayers" => array(),
                            "otherTeams" => array(),
                            "userTeamPlayers" => array()
                        );

                        foreach ($leagueDataJson["teams"] as $team) {
                            foreach ($team["players"] as $player) {
                                $leagueData["ownedPlayers"][] = $player["nba_id"];

                                // Is this the user's team?
                                if ($team['team_key'] == $userTeam["yahoo_team_key"]) {
                                    $leagueData["userTeamPlayers"][$player["nba_id"]] = array(
                                        "nbaId" => $player["nba_id"],
                                        "status" => $player["status"],
                                        "isOnBench" => ($player["position"] == "BN" ? true: false)
                                    );
                                }
                                else {
                                    if (!isset($leagueData["otherTeams"][$team["team_name"]])) {
                                        $leagueData["otherTeams"][$team["team_name"]] = array();
                                    }
                                    $leagueData["otherTeams"][$team["team_name"]][] = $player["nba_id"];
                                }
                            }
                        }

                        $syncedLeagueData[$userTeam["yahoo_team_key"]] = $leagueData;
                    }
                }
            }
        }
    }

    return $syncedLeagueData;
}

function getPlayersForCurrentUser($loadTeams = false) {
    $players = array();

    $userId = getSessionParameter("user_id");

    $sql = "SELECT id, yahoo_id, nba_id, name FROM player WHERE user_id = " . $userId . " ORDER BY id";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        while ($r = mysqli_fetch_assoc($q)) {
            $players[$r["id"]] = array(
                "id" => $r["id"],
                "yahoo_id" => $r["yahoo_id"],
                "nba_id" => $r["nba_id"],
                "name" => $r["name"],
                "teams" => array(),
            );
        }
    }

    if ($loadTeams) {
        $sql = "SELECT player_id, team_id FROM player_team WHERE player_id IN (" . implode(", ", array_keys($players)) . ")";
        if ($q = mysqli_query(getDbConnection(), $sql)) {
            while ($r = mysqli_fetch_assoc($q)) {
                $players[$r["player_id"]]["teams"][] = $r["team_id"];
            }
        }
    }

    return $players;
}

////// Session stuff

function ensureLoggedIn() {
    // TODO Solve this...for some reason it keeps thinking the user is logged out when they're not...or is it just the actual session timing out when we should be trusting the cookies?
    if (!getSessionParameter("user_id")) {
        header("Location: index.php");
        exit;
    }

    // While we migrate to email/password account types, don't let someone into a secure page if they haven't yet set up their email/password login
    $user = getUser(getSessionParameter("user_id"));
    if (!$user["email"]) {
        header("Location: setupUser.php");
        exit;
    }
}

// This serves as a backup for if the PHP session expires
function saveSessionToCookiesIfExists() {
    $expiryTime = time()+60*60*24*30;

    if ($_SESSION["oauth_access_token"]) {
        setcookie("oauth_access_token", $_SESSION["oauth_access_token"], $expiryTime, "/");
    }
    if ($_SESSION["oauth_access_token_secret"]) {
        setcookie("oauth_access_token_secret", $_SESSION["oauth_access_token_secret"], $expiryTime, "/");
    }
    if ($_SESSION["oauth_session_handle"]) {
        setcookie("oauth_session_handle", $_SESSION["oauth_session_handle"], $expiryTime, "/");
    }
    if ($_SESSION["user_id"]) {
        setcookie("user_id", $_SESSION["user_id"], $expiryTime, "/");
    }
}
function cleanCookies() {
    unset($_COOKIE['oauth_access_token']);
    setcookie('oauth_access_token', null, -1, '/');

    unset($_COOKIE['oauth_access_token_secret']);
    setcookie('oauth_access_token_secret', null, -1, '/');

    unset($_COOKIE['oauth_session_handle']);
    setcookie('oauth_session_handle', null, -1, '/');

    unset($_COOKIE['user_id']);
    setcookie('user_id', null, -1, '/');
}

function getSessionParameter($name) {
    $value = null;

    if (isset($_SESSION[$name]) && $_SESSION[$name]) {
        $value = $_SESSION[$name];
    }

    if (!$value && isset($_COOKIE[$name]) && $_COOKIE[$name]) {
        $value = $_COOKIE[$name];
    }

    return $value;
}

function cleanSession() {
    unset($_SESSION["oauth_request_token"]);
    unset($_SESSION["oauth_request_token_secret"]);
    unset($_SESSION["oauth_access_token"]);
    unset($_SESSION["oauth_access_token_secret"]);
    unset($_SESSION["oauth_session_handle"]);
    unset($_SESSION["xoauth_yahoo_guid"]);
    unset($_SESSION["user_id"]);
}

////// Service calls for Yahoo

function serviceRequestYahooYQL($yql) {
    $access_token = getSessionParameter("oauth_access_token");
    $access_token_secret = getSessionParameter("oauth_access_token_secret");

    $retarr = call_yql(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $yql, $access_token, $access_token_secret, false, true);
    if (!isset($retarr[0]["http_code"]) || $retarr[0]["http_code"] != 200) {
        header("Location: oauth.php?action=refresh-access-token&redirect=" . urlencode($_SERVER["REQUEST_URI"]));
        exit;
    }

    return json_decode($retarr[2], true);
}

function serviceRequestYahooGetPlayerData($leagueId, $playerIds, $date) {
    $query = "select * from fantasysports.players.stats "
            . " where league_key='" . $leagueId . "' "
            . " and player_key in ('" . implode("', '", $playerIds) . "') "
            . " and stats_date = '" . $date . "' and stats_type = 'date'";

    $json = serviceRequestYahooYQL($query);

    return ($json["query"]["count"] > 1 ? $json["query"]["results"]["player"] : array($json["query"]["results"]["player"]));
}

// Not working! Requires HTTPS, but this breaks other things. Investigate.
function serviceRequestYahooGetEmail() {
    $query = "select * from social.profile where guid = me";

    $json = serviceRequestYahooYQL($query);

    // Hard to tell exactly where the email address will be - try a few places

    $email = null;
    if ($json["query"]) {
        if ($json["query"]["results"]) {
            if ($json["query"]["results"]["profile"]) {
                if ($json["query"]["results"]["profile"]["email"]) {
                    if ($json["query"]["results"]["profile"]["email"]["handle"]) {
                        $email = $json["query"]["results"]["profile"]["email"]["handle"];
                    }
                }
                else if ($json["query"]["results"]["profile"]["emails"]) {
                    if ($json["query"]["results"]["profile"]["emails"]["handle"]) {
                        $email = $json["query"]["results"]["profile"]["emails"]["handle"];
                    }
                    else if ($json["query"]["results"]["profile"]["emails"][0]) {
                        if ($json["query"]["results"]["profile"]["emails"][0]["handle"]) {
                            $email = $json["query"]["results"]["profile"]["emails"][0]["handle"];
                        }
                    }
                }
            }
        }
    }

    return $email;
}

function serviceRequestYahooGetLeagues() {
    $query = "select * from fantasysports.leagues where game_key='" . YAHOO_API_SEASON_CODE . "" . YAHOO_API_SEASON_CODE . "' and use_login = 1";

    $json = serviceRequestYahooYQL($query);

    return ($json["query"]["count"] > 1 ? $json["query"]["results"]["league"] : array($json["query"]["results"]["league"]));
}

function serviceRequestYahooGetTeams() {
    $query = "select * from fantasysports.teams where game_key='" . YAHOO_API_SEASON_CODE . "' and use_login = 1";

    $json = serviceRequestYahooYQL($query);

    return ($json["query"]["count"] > 1 ? $json["query"]["results"]["team"] : array($json["query"]["results"]["team"]));
}

function serviceRequestYahooGetPlayersByUserTeams() {
    $query = "select * from fantasysports.teams.roster where game_key='" . YAHOO_API_SEASON_CODE . "' and use_login = 1";

    $json = serviceRequestYahooYQL($query);

    return ($json["query"]["count"] > 1 ? $json["query"]["results"]["team"] : array($json["query"]["results"]["team"]));
}

function serviceRequestYahooSearchPlayers($searchText) {
    $access_token = getSessionParameter("oauth_access_token");
    $access_token_secret = getSessionParameter("oauth_access_token_secret");

    $user = getUser(getSessionParameter("user_id"));

    $url = "http://fantasysports.yahooapis.com/fantasy/v2/league/" . $user["league_key"] . "/players;sort=OR;search=" . urlencode($searchText);
    $params = array();

    $retarr = call_url(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $url, array(), $access_token, $access_token_secret, false, true);
    if (!isset($retarr[0]["http_code"]) || $retarr[0]["http_code"] != 200) {
        header("Location: oauth.php?action=refresh-access-token&redirect=" . urlencode($_SERVER["REQUEST_URI"]));
        exit;
    }

    $json = json_decode($retarr[2], true);

    // Consolidate the important information into a easier to use, flat array
    $players = $json["fantasy_content"]["league"][1]["players"];
    $playersOutput = array();
    foreach ($players as $player) {
        if (is_array($player["player"][0][0])) {
            $playerOutput = array();
            foreach ($player["player"][0] as $playerInfo) {
                $playerOutput = array_merge($playerOutput, $playerInfo);
            }
            $playersOutput[] = $playerOutput;
        }
    }

    return $playersOutput;
}

function serviceRequestYahooSaveTeams() {
    $userTeams = array();

    $teams = serviceRequestYahooGetPlayersByUserTeams();
    foreach ($teams as $team) {
        $userTeam = array(
            "id" => null,
            "name" => $team["name"],
            "shortName" => strtoupper(substr(str_replace(" ", "", $team["name"]), 0, 4)),
            "yahooTeamKey" => $team["team_key"],
            "playersAdded" => array(),
            "playersNotFound" => array(),
        );

        $userTeam["id"] = addTeam($userTeam["name"], $userTeam["shortName"], $teamKey);

        if ($team["roster"] && $team["roster"]["players"] && $team["roster"]["players"]["player"]) {
            foreach ($team["roster"]["players"]["player"] as $player) {
                $playerId = null;
                $playerName = $player["name"]["full"];

                $results = serviceRequestNbaSearchPlayers($playerName);
                if (count($results) > 0) {
                    // Assume the first result is right
                    $nbaPlayerRecord = $results[0];

                    $playerId = addPlayerToList($nbaPlayerRecord["PERSON_ID"], nameLastCommaFirstToFirstLast($nbaPlayerRecord["DISPLAY_LAST_COMMA_FIRST"]), array($userTeam["id"]));
                    if ($playerId) {
                        $userTeam["playersAdded"][] = $playerName;
                    }
                }

                if (!$playerId) {
                    $userTeam["playersNotFound"][] = $playerName;
                }
            }
        }

        $userTeams[] = $userTeam;
    }

    return $userTeams;
}

//////// Service requests for NBA.stats API

function nbaServiceRequestConfigBoxscoreUrl($dateObject, $gameId) {
    $formattedDate = $dateObject->format('Ymd');
    //return "https://neulionmdnyc-a.akamaihd.net/fs/nba/feeds_s2012/stats/" . NBA_SITE_SEASON_CODE . "/boxscore/" . $gameId . ".js?" . time();
    return "https://cdn.nba.com/static/json/liveData/boxscore/boxscore_" . $gameId . ".json?" . time();
}
function nbaServiceRequestConfigHtmlBoxscoreFilename($dateObject, $gameId) {
    $formattedDate = $dateObject->format('Ymd');
    return "data/boxscore-" . $formattedDate . "-" . $gameId . ".html";
}
function nbaServiceRequestConfigJsonBoxscoreFilename($dateObject, $gameId) {
    $formattedDate = $dateObject->format('Ymd');
    return "data/new-boxscore-" . $formattedDate . "-" . $gameId . ".json";
}
function serviceRequestNbaGetPlayerData($dateObject, array $gameIds) {
    $playersData = array();

    foreach ($gameIds as $gameId) {

        // Look for the (new) JSON format boxscores first

        $jsonString = null;

        if (file_exists(nbaServiceRequestConfigJsonBoxscoreFilename($dateObject, $gameId))) {
            $jsonString = file_get_contents(nbaServiceRequestConfigJsonBoxscoreFilename($dateObject, $gameId));
        }
        if ($jsonString) {
            $players = getBoxscorePlayersFromNbaJson($jsonString);
            $playersData = $playersData + $players;
        }
        else {

            // Only if we can't find JSON format boxscores should we look for the (old) HTML boxscores

            $html = null;

            // Look for the game on the given date...but if we don't find it, try the previous date, as if we're using au.global.nba.com, this could happen overnight
            if (file_exists(nbaServiceRequestConfigHtmlBoxscoreFilename($dateObject, $gameId))) {
                $html = file_get_contents(nbaServiceRequestConfigHtmlBoxscoreFilename($dateObject, $gameId));
            }
            else {
                $yesterdayDateObject = clone $dateObject;
                $yesterdayDateObject->sub(new DateInterval("P1D"));
                if (file_exists(nbaServiceRequestConfigHtmlBoxscoreFilename($yesterdayDateObject, $gameId))) {
                    $html = file_get_contents(nbaServiceRequestConfigHtmlBoxscoreFilename($yesterdayDateObject, $gameId));
                }
            }

            if ($html) {
                $players = getBoxscorePlayersFromNbaHtml($html);
                $playersData = $playersData + $players;
            }
        }
    }

    return $playersData;
}
function getBoxscorePlayersFromNbaJson($jsonString) {
    $players = array();

    $json = json_decode($jsonString, true);

    if (is_array($json) && isset($json["game"]) && isset($json["game"]["homeTeam"]) && isset($json["game"]["awayTeam"]) && isset($json["game"]["homeTeam"]["players"]) && isset($json["game"]["awayTeam"]["players"])) {
        $sourcePlayers = array_merge(
            $json["game"]["homeTeam"]["players"],
            $json["game"]["awayTeam"]["players"]
        );
        foreach ($sourcePlayers as $sourcePlayer) {
            $player = array(
                "NAME" => $sourcePlayer["name"],
                "PLAYER_CODE" => $sourcePlayer["personId"],
                "STATUS" => null,
            );

            // TODO Do we have a "status" (i.e. "DNP-injured" or whatever)

            // Only show on_court notifications if the game is happening currently
            $player["ON_COURT"] = (
                $json["game"] &&
                $json["game"]["gameStatus"] &&
                $json["game"]["gameStatus"] == 2 &&
                $json["game"]["gameClock"] != "" &&
                $sourcePlayer["oncourt"] == 1
            );

            $player[STAT_ID_MIN] = ltrim(preg_replace("/[^0-9]/", "", $sourcePlayer["statistics"]["minutesCalculated"]), "0");

            $player[STAT_ID_FG] = $sourcePlayer["statistics"]["fieldGoalsMade"] . "/" . $sourcePlayer["statistics"]["fieldGoalsAttempted"];
            $player[STAT_ID_FGM] = $sourcePlayer["statistics"]["fieldGoalsMade"];
            $player[STAT_ID_FGA] = $sourcePlayer["statistics"]["fieldGoalsAttempted"];
            $player[STAT_ID_FGP] = ($sourcePlayer["statistics"]["fieldGoalsAttempted"] == 0 ? 0 : round($sourcePlayer["statistics"]["fieldGoalsMade"] / $sourcePlayer["statistics"]["fieldGoalsAttempted"], 3));

            $player[STAT_ID_FG3] = $sourcePlayer["statistics"]["threePointersMade"] . "/" . $sourcePlayer["statistics"]["threePointersAttempted"];
            $player[STAT_ID_FG3M] = $sourcePlayer["statistics"]["threePointersMade"];
            $player[STAT_ID_FG3A] = $sourcePlayer["statistics"]["threePointersAttempted"];
            $player[STAT_ID_FG3P] = ($sourcePlayer["statistics"]["threePointersAttempted"] == 0 ? 0 : round($sourcePlayer["statistics"]["threePointersMade"] / $sourcePlayer["statistics"]["threePointersAttempted"], 3));

            $player[STAT_ID_FT] = $sourcePlayer["statistics"]["freeThrowsMade"] . "/" . $sourcePlayer["statistics"]["freeThrowsAttempted"];
            $player[STAT_ID_FTM] = $sourcePlayer["statistics"]["freeThrowsMade"];
            $player[STAT_ID_FTA] = $sourcePlayer["statistics"]["freeThrowsAttempted"];
            $player[STAT_ID_FTP] = ($sourcePlayer["statistics"]["freeThrowsAttempted"] == 0 ? 0 : round($sourcePlayer["statistics"]["freeThrowsMade"] / $sourcePlayer["statistics"]["freeThrowsAttempted"], 3));

            $player[STAT_ID_REB] = $sourcePlayer["statistics"]["reboundsTotal"];
            $player[STAT_ID_AST] = $sourcePlayer["statistics"]["assists"];
            $player[STAT_ID_PF] = $sourcePlayer["statistics"]["foulsPersonal"];
            $player[STAT_ID_STL] = $sourcePlayer["statistics"]["steals"];
            $player[STAT_ID_TO] = $sourcePlayer["statistics"]["turnovers"];
            $player[STAT_ID_BLK] = $sourcePlayer["statistics"]["blocks"];
            $player[STAT_ID_PTS] = $sourcePlayer["statistics"]["points"];

            $players[$sourcePlayer["personId"]] = $player;
        }
    }

    return $players;
}

function getBoxscorePlayersFromNbaHtml($html) {

    // Fix non-XHTML elements, such as <img> with no />
    $html = str_replace("<img src=\"http://i.cdn.turner.com/nba/nba/images/livestats/arrow.gif\">", "<img src=\"http://i.cdn.turner.com/nba/nba/images/livestats/arrow.gif\" />", $html);

    preg_match_all("/\<tr class\=\"odd\"\>(.*?)\<\/tr\>/s", $html, $oddRows);
    preg_match_all("/\<tr class\=\"even\"\>(.*?)\<\/tr\>/s", $html, $evenRows);

    $players = array();

    foreach (array($oddRows, $evenRows) as $rowSet) {
        foreach ($rowSet[0] as $rowHtml) {
            $rowElement = simplexml_load_string('<temp>' . utf8_encode(html_entity_decode($rowHtml)) . '</temp>');

            // Check it's an actual player row
            if (trim($rowElement->tr->td[0]->a) != "") {

                // Pull apart the link so we can get hte player code
                $link = $rowElement->tr->td[0]->a["href"];
                $linkChunks = explode("/", $link);
                $playerCode = strtolower($linkChunks[2]);

                $player = array(
                    "NAME" => (string)$rowElement->tr->td[0]->a,
                    "PLAYER_CODE" => $playerCode,
                    "STATUS" => null,
                    "ON_COURT" => false,
                );

                if ($rowElement->tr->td[0]->img) {
                    $player["ON_COURT"] = true;
                }

                if ($rowElement->tr->td->count() <= 2) {
                    $player["STATUS"] = (string)$rowElement->tr->td[1];
                }
                else {
                    $player[STAT_ID_MIN] = 0;
                    $minutes = (string)$rowElement->tr->td[2];
                    if ($minutes != "" && $minutes != "00:00") {
                        $minutesChunks = explode(":", $minutes);
                        if (isset($minutesChunks[0])) {
                            $player[STAT_ID_MIN] = intval($minutesChunks[0]);
                        }
                    }

                    $fgChunks = explode("-", (string)$rowElement->tr->td[3]);
                    $player[STAT_ID_FG] = $fgChunks[0] . "/" . $fgChunks[1];
                    $player[STAT_ID_FGM] = $fgChunks[0];
                    $player[STAT_ID_FGA] = $fgChunks[1];
                    $player[STAT_ID_FGP] = ($fgChunks[1] == 0 ? 0 : round($fgChunks[0] / $fgChunks[1], 3));

                    $fg3Chunks = explode("-", (string)$rowElement->tr->td[4]);
                    $player[STAT_ID_FG3] = $fg3Chunks[0] . "/" . $fg3Chunks[1];
                    $player[STAT_ID_FG3M] = $fg3Chunks[0];
                    $player[STAT_ID_FG3A] = $fg3Chunks[1];
                    $player[STAT_ID_FG3P] = ($fg3Chunks[1] == 0 ? 0 : round($fg3Chunks[0] / $fg3Chunks[1], 3));

                    $ftChunks = explode("-", (string)$rowElement->tr->td[5]);
                    $player[STAT_ID_FT] = $ftChunks[0] . "/" . $ftChunks[1];
                    $player[STAT_ID_FTM] = $ftChunks[0];
                    $player[STAT_ID_FTA] = $ftChunks[1];
                    $player[STAT_ID_FTP] = ($ftChunks[1] == 0 ? 0 : round($ftChunks[0] / $ftChunks[1], 3));

                    $player[STAT_ID_REB] = (string)$rowElement->tr->td[9];
                    $player[STAT_ID_AST] = (string)$rowElement->tr->td[10];
                    $player[STAT_ID_PF] = (string)$rowElement->tr->td[11];
                    $player[STAT_ID_STL] = (string)$rowElement->tr->td[12];
                    $player[STAT_ID_TO] = (string)$rowElement->tr->td[13];
                    $player[STAT_ID_BLK] = (string)$rowElement->tr->td[14];
                    $player[STAT_ID_PTS] = (string)$rowElement->tr->td[16];
                }

                $players[$playerCode] = $player;
            }
        }
    }

    return $players;
}

function rotoworldServiceRequestConfigNewsFilename() {
    return "data/playerNews.json";
}
function updateRotoworldPlayerNewsFeed() {
    $savedNewsItemIds = array();
    $savedNewsItems = array();

    $previousFileContents = file_get_contents(rotoworldServiceRequestConfigNewsFilename());
    if ($previousFileContents) {
        $newsItems = json_decode($previousFileContents, true);
        if ($newsItems && is_array($newsItems) && count($newsItems) > 0) {
            $savedNewsItems = $newsItems;
            foreach ($newsItems as $newsItem) {
                $savedNewsItemIds[] = $newsItem["guid"];
            }
        }
    }

    $playersList = serviceRequestNbaGetPlayerList();

    $newsFeedContents = file_get_contents("https://www.rotowire.com/rss/news.php?sport=NBA");
    //$newsFeedContents = file_get_contents("http://www.rotoworld.com/rss/feed.aspx?sport=nba&ftype=news&count=12&format=rss");
    //$newsFeedContents = file_get_contents("data/tempRotoworldNews.xml");
    if ($newsFeedContents) {
        $xml = simplexml_load_string($newsFeedContents);

        $newNewsItems = array();
        foreach ($xml->xpath("//channel/item") as $newsItem) {
            // Ensure we're not adding anything that's already on the list
            $guid = (string)$newsItem->guid;
            if (!in_array($guid, $savedNewsItemIds)) {
                $newsItem = (array)$newsItem;

                foreach ($playersList as $player) {
                    $playerName = $player["PLAYER_FIRST_NAME"] . " " . $player["PLAYER_LAST_NAME"];

                    if (stripos($newsItem["title"], $playerName) !== false) {
                        $newsItem["title"] = str_replace($playerName,
                                                            '<a href="/player.php?nba_id=' . $player["PERSON_ID"] . '">' . $playerName . '</a>',
                                                            $newsItem["title"]);
                    }
                    if (stripos($newsItem["description"], $playerName) !== false) {
                        $newsItem["description"] = str_replace($playerName,
                                                                '<a href="/player.php?nba_id=' . $player["PERSON_ID"] . '">' . $playerName . '</a>',
                                                                $newsItem["description"]);
                    }
                }

                $newsItem["description"] = str_replace(PHP_EOL, "", $newsItem["description"]);
                $newsItem["description"] = str_replace("Visit RotoWire.com for more analysis on this update.", "", $newsItem["description"]);

                $newNewsItems[] = $newsItem;
            }
        }

        $fullNewsItemList = array_slice(array_merge($newNewsItems, $savedNewsItems), 0, 300);

        file_put_contents(rotoworldServiceRequestConfigNewsFilename(), json_encode($fullNewsItemList));

        return true;
    }
    else {
      return false;
    }
}
function getRotoworldPlayerNewsFeed() {
    $outputNewsItems = array();

    $fileContents = file_get_contents(rotoworldServiceRequestConfigNewsFilename());
    if ($fileContents) {
        $newsItems = json_decode($fileContents, true);
        if ($newsItems && is_array($newsItems) && count($newsItems) > 0) {
            $outputNewsItems = $newsItems;
        }
    }

    return $outputNewsItems;
}

function nbaServiceRequestConfigScoreboardUrl($dateObject) {
    $formattedDate = $dateObject->format('m/d/Y');
    // TEMP - while NBA API is returning stale data
    //return "http://stats.nba.com/stats/scoreboard/?GameDate=" . $formattedDate . "&LeagueID=00&DayOffset=0&unique=" . microtime(true);
    //return "http://au.global.nba.com/stats2/scores/miniscoreboard.json?countryCode=AU&dst=0&locale=au&tz=%2B10&unique=" . microtime(true);
    return "https://cdn.nba.com/static/json/liveData/scoreboard/todaysScoreboard_00.json";
}
function nbaServiceRequestConfigScoreboardFilename($dateObject) {
    $formattedDate = $dateObject->format('mdY');
    return "data/scoreboard-" . $formattedDate . ".json";
}
function serviceRequestNbaGetGamesData($dateObject) {
    $filename = nbaServiceRequestConfigScoreboardFilename($dateObject);
    if (!file_exists($filename)) {
        return array();
    }

    $json = file_get_contents($filename);
    //$json = file_get_contents("sampleLiveScoreFeed3.json");
    $gamesData = json_decode($json, true);

    // TEMP - while NBA API is returning stale data
    //return array(
    //    "games" => nbaTableToArray($gamesData, "GameHeader", "GAME_ID"),
    //    "teams" => nbaTableToArray($gamesData, "LineScore", "TEAM_ID"),
    //);
    $output = array("games" => array());
    foreach ($gamesData["scoreboard"]["games"] as $game) {
        $output["games"][$game["gameId"]] = $game;
    }
    return $output;
}

function serviceRequestNbaGetPlayerListByName() {
    return serviceRequestNbaGetPlayerList("PLAYER_SLUG");
}
function serviceRequestNbaGetPlayerListById() {
    return serviceRequestNbaGetPlayerList("PERSON_ID");
}
function serviceRequestNbaGetPlayerListByCode() {
    $playerListByCode = serviceRequestNbaGetPlayerList("PLAYERCODE");
    return array_change_key_case($playerListByCode, CASE_LOWER);
}
function serviceRequestNbaGetPlayerList($indexField = null) {
    //$json = file_get_contents("http://stats.nba.com/stats/commonallplayers/?LeagueID=00&Season=" . NBA_STATS_API_CURRENT_SEASON_CODE . "&IsOnlyCurrentSeason=1");
    $json = file_get_contents("data/playersList.json");
    $playersData = json_decode($json, true);

    //return nbaTableToArray($playersData, "CommonAllPlayers", ($indexField ? $indexField : "PERSON_ID"));
    return nbaTableToArray($playersData, "PlayerIndex", ($indexField ? $indexField : "PERSON_ID"));
}
function serviceRequestNbaSearchPlayers($searchName) {
    $matches = array();

    $playerList = serviceRequestNbaGetPlayerList();
    foreach ($playerList as $player) {
        $playerName = $player["PLAYER_FIRST_NAME"] . " " . $player["PLAYER_LAST_NAME"];
        if (stripos($playerName, $searchName) !== false || stripos(nameLastCommaFirstToFirstLast($playerName), $searchName) !== false) {
            $matches[] = $player;
        }
    }

    return $matches;
}

// Given a blob of text (e.g. copy and paste from ESPN), find matching players by going through every name we know and seeing if it's in there
function serviceRequestMatchPlayersInString($playerText) {
    $matchedPlayers = array();

    $playersByName = serviceRequestNbaGetPlayerListByName();
    foreach ($playersByName as $playerName => $player) {
        $name = nameLastCommaFirstToFirstLast($playerName);
        if (stripos($playerText, $name) !== false) {
            $matchedPlayers[] = $player;
        }
    }

    return $matchedPlayers;
}

function serviceRequestNbaGetPlayerGameLog($nbaId, $season = NBA_STATS_API_CURRENT_SEASON_CODE, $seasonType = NBA_STATS_API_REGULAR_SEASON) {
    $json = do_statsnbacom_request("http://stats.nba.com/stats/playergamelog/?PlayerId=" . $nbaId . "&Season=" . $season . "&SeasonType=" . urlencode($seasonType) . "&LeagueId=00");
    $playerData = json_decode($json, true);

    return nbaTableToArray($playerData, "PlayerGameLog", "Game_ID");
}

function getMatchesByTeam($serviceGamesData) {
    $matchesByTeam = array();

    if ($serviceGamesData) {
        // TEMP - while NBA API is returning stale data
        /*
        foreach ($serviceGamesData["games"] as $game) {
            $matchSummary = array();

            if ($game["GAME_STATUS_ID"] == "1") {
                $matchSummary["status"] = MATCH_STATUS_UPCOMING;
            }
            else if ($game["GAME_STATUS_ID"] == "3") {
                $matchSummary["status"] = MATCH_STATUS_COMPLETE;
            }
            else if ($game["GAME_STATUS_ID"] == "2") {
                $matchSummary["status"] = MATCH_STATUS_IN_PROGRESS;
                $matchSummary["quarter"] = $game["LIVE_PERIOD"];
                $matchSummary["clock"] = $game["LIVE_PC_TIME"];
            }

            list($date, $teamCodes) = explode("/", $game["GAMECODE"]);
            $homeTeam = substr($teamCodes, 3, 3);
            $awayTeam = substr($teamCodes, 0, 3);

            $matchesByTeam[$homeTeam] = array_merge($matchSummary, array(
                "opponent" => $awayTeam,
                "home" => true
            ));
            $matchesByTeam[$awayTeam] = array_merge($matchSummary, array(
                "opponent" => $homeTeam,
                "home" => false
            ));
        }
        */

        foreach ($serviceGamesData["games"] as $game) {
            $matchSummary = array();

            if ($game["gameStatus"] == "1") {
                $matchSummary["status"] = MATCH_STATUS_UPCOMING;
            }
            else if ($game["gameStatus"] == "3") {
                $matchSummary["status"] = MATCH_STATUS_COMPLETE;
            }
            else if ($game["gameStatus"] == "2") {
                $matchSummary["status"] = MATCH_STATUS_IN_PROGRESS;
                $matchSummary["quarter"] = $game["period"];

                // Sometimes they put "Half" or "Halftime" or "End of 3rd Qtr" in the status text
                if (stripos($game["gameStatusText"], "half") !== false || stripos($game["gameStatusText"], "end") !== false) {
                    $matchSummary["clock"] = "";
                }
                else {
                    $statusTextChunks = explode(" ", trim($game["gameStatusText"]));
                    if ($statusTextChunks[1] == "Qtr") {
                         // Sometimes they put  e.g. "3rd Qtr" in the status and the clock, raw, in the "gameClock". Very inconsistent
                        $matchSummary["clock"] = $game["gameClock"];
                    }
                    else {
                        $matchSummary["clock"] = $statusTextChunks[1];
                    }
                }

                $currentGames = true;
            }

            if ($matchSummary["status"] == MATCH_STATUS_UPCOMING) {
                $matchSummary["minutesUntilStart"] = round((strtotime($game["gameTimeUTC"]) - time()) / 60);
                $matchSummary["startTime"] = strtotime($game["gameTimeUTC"]) ;
            }

            $homeTeam = $game["homeTeam"]["teamTricode"];
            $awayTeam = $game["awayTeam"]["teamTricode"];

            $matchesByTeam[$homeTeam] = array_merge($matchSummary, array(
                "opponent" => $awayTeam,
                "home" => true,
                "score" => $game["homeTeam"]["score"],
            ));
            $matchesByTeam[$awayTeam] = array_merge($matchSummary, array(
                "opponent" => $homeTeam,
                "home" => false,
                "score" => $game["awayTeam"]["score"],
            ));
        }
    }

    return $matchesByTeam;
}

function nbaTableToArray($response, $tableIdentifier, $keyName) {
    $tableArray = array();

    foreach ($response["resultSets"] as $resultSet) {
        // Look until we find the requested table
        if ($resultSet["name"] == $tableIdentifier) {
            $headers = $resultSet["headers"];

            foreach ($resultSet["rowSet"] as $row) {
                $key = null;
                $data = array();

                for ($i = 0; $i < count($row); $i++) {
                    $data[$headers[$i]] = $row[$i];

                    if ($headers[$i] == $keyName) {
                        $key = $row[$i];
                    }
                }

                $tableArray[$key] = $data;
            }

            break;
        }
    }

    return $tableArray;
}

function nameParts($name, $separator = " ") {
    $nameChunks = explode($separator, $name, 2);
    return array($nameChunks[0], (isset($nameChunks[1]) ? $nameChunks[1] : null));
}
function nameLastCommaFirstToFirstLast($name) {
    $nameChunks = nameParts($name, ", ");
    return $nameChunks[1] . " " . $nameChunks[0];
}

// Consider that the new "day" starts at 3am LA time. It shouldn't be midnight, as some
// games can run late, and you want to look at your day's stats after the day has finished.
// But this is necessary to be not too long after midnight, otherwise Paris games may start
// for the next day before the previous day has finished, which overwrites the previous days game data.
// The concept of "today" revolves around this effective date.
function getEffectiveTodayDateObject() {
    $dateObject = new DateTime("now", new DateTimeZone('America/Los_Angeles'));
    $dateObject->modify("- 3 hour");
    return $dateObject;
}

// Hide player cookie stuff

function getHiddenPlayerCookieName($nbaId) {
    $dateObject = getEffectiveTodayDateObject();
    return "hidePlayer" . $nbaId . "For" . $dateObject->format('Ymd');
}
function isPlayerHiddenForToday($nbaId) {
    return (isset($_COOKIE[getHiddenPlayerCookieName($nbaId)]) && $_COOKIE[getHiddenPlayerCookieName($nbaId)] == "hide");
}
function setPlayerHiddenForToday($nbaId, $setHidden = true) {
    $cookieName = getHiddenPlayerCookieName($nbaId);
    if ($setHidden) {
    	setcookie($cookieName, "hide" , (time() + (86400 * 30)), "/"); // 86400 = 1 day. Doesn't matter how long this is set because it will be scrubbed when the date in the $cookieID doesn't match
        $_COOKIE[$cookieName] = "hide";
    }
    else {
    	setcookie($cookieName, "", (time() - 3600), "/"); // Destroy the cookie
        unset($_COOKIE[$cookieName]);
    }
}

////// Page construction stuff

function printHeader() {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Flance</title>

        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/bootstrap-theme.css" rel="stylesheet" />

        <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <link href="main.css" rel="stylesheet" />

        <script type="text/javascript">

            $(document).ready(function() {

                // Support hover on desktop
                $("td.data.stat-fg, td.data.stat-ft").mouseover(function() {
                    $("span.data-percentage.view-made-attempts").hide();
                    $("span.data-percentage.view-percentage").show();
                });
                $("td.data.stat-fg, td.data.stat-ft").mouseout(function() {
                    $("span.data-percentage.view-made-attempts").show();
                    $("span.data-percentage.view-percentage").hide();
                });

                // Support tap-to-show on mobile
                var currentStateDefault = true;
                $("td.data.stat-fg, td.data.stat-ft").click(function() {
                    currentStateDefault = !currentStateDefault;

                    if (currentStateDefault) {
                        $("span.data-percentage.view-made-attempts").show();
                        $("span.data-percentage.view-percentage").hide();
                    }
                    else {
                        $("span.data-percentage.view-made-attempts").hide();
                        $("span.data-percentage.view-percentage").show();
                    }

                });

                // Ensure percentage is hidden to start
                $("span.data-percentage.view-percentage").hide();
            });

        </script>
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container">

              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Flance</a>
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php if (getSessionParameter("user_id")): ?>
                        <li><a href="home.php">Home</a></li>
                        <li><a href="searchPlayers.php">Find a player</a></li>
                        <li><a href="editPlayers.php">Edit my players</a></li>
                        <li><a href="editTeams.php">Edit my teams</a></li>
                        <li><a href="settings.php">Settings</a></li>
                        <li><a href="doLogout.php">Log out</a></li>
                    <?php else: ?>
                        <li><a href="login.php">Log in</a></li>
                        <li><a href="setupUser.php">Create a new account</a></li>
                    <?php endif; ?>
                </ul>
              </div>
            </div>
        </nav>

        <div class="container container-body">
<?php
}

function printFooter() {
?>
            <div class="footer">
                <hr />
                <p>
                    <a href="https://www.reddit.com/r/flance/">Discussion & support</a>
                     | <a href="mailto:help@flance.invitationstation.org">Contact us</a>
                     | &copy; Cameron Ross 2016
                </p>
            </div>

        </div>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-71811057-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>
<?php
}

function getTeamImageUrl($teamCode) {
    $teamImages = array(
        "ATL" => "https://cdn.nba.com/logos/nba/1610612737/global/L/logo.svg",
        "BKN" => "https://cdn.nba.com/logos/nba/1610612751/global/L/logo.svg",
        "BOS" => "https://cdn.nba.com/logos/nba/1610612738/global/L/logo.svg",
        "CHA" => "https://cdn.nba.com/logos/nba/1610612766/global/L/logo.svg",
        "CHI" => "https://cdn.nba.com/logos/nba/1610612741/global/L/logo.svg",
        "CLE" => "https://cdn.nba.com/logos/nba/1610612739/global/L/logo.svg",
        "DAL" => "https://cdn.nba.com/logos/nba/1610612742/global/L/logo.svg",
        "DEN" => "https://cdn.nba.com/logos/nba/1610612743/global/L/logo.svg",
        "DET" => "https://cdn.nba.com/logos/nba/1610612765/global/L/logo.svg",
        "GSW" => "https://cdn.nba.com/logos/nba/1610612744/global/L/logo.svg",
        "HOU" => "https://cdn.nba.com/logos/nba/1610612745/global/L/logo.svg",
        "IND" => "https://cdn.nba.com/logos/nba/1610612754/global/L/logo.svg",
        "LAC" => "https://cdn.nba.com/logos/nba/1610612746/global/L/logo.svg",
        "LAL" => "https://cdn.nba.com/logos/nba/1610612747/global/L/logo.svg",
        "MEM" => "https://cdn.nba.com/logos/nba/1610612763/global/L/logo.svg",
        "MIA" => "https://cdn.nba.com/logos/nba/1610612748/global/L/logo.svg",
        "MIL" => "https://cdn.nba.com/logos/nba/1610612749/global/L/logo.svg",
        "MIN" => "https://cdn.nba.com/logos/nba/1610612750/global/L/logo.svg",
        "NOP" => "https://cdn.nba.com/logos/nba/1610612740/global/L/logo.svg",
        "NYK" => "https://cdn.nba.com/logos/nba/1610612752/global/L/logo.svg",
        "OKC" => "https://cdn.nba.com/logos/nba/1610612760/global/L/logo.svg",
        "ORL" => "https://cdn.nba.com/logos/nba/1610612753/global/L/logo.svg",
        "PHI" => "https://cdn.nba.com/logos/nba/1610612755/global/L/logo.svg",
        "PHX" => "https://cdn.nba.com/logos/nba/1610612756/global/L/logo.svg",
        "POR" => "https://cdn.nba.com/logos/nba/1610612757/global/L/logo.svg",
        "SAS" => "https://cdn.nba.com/logos/nba/1610612759/global/L/logo.svg",
        "SAC" => "https://cdn.nba.com/logos/nba/1610612758/global/L/logo.svg",
        "TOR" => "https://cdn.nba.com/logos/nba/1610612761/global/L/logo.svg",
        "UTA" => "https://cdn.nba.com/logos/nba/1610612762/global/L/logo.svg",
        "WAS" => "https://cdn.nba.com/logos/nba/1610612764/global/L/logo.svg",
    );
    if (isset($teamImages[$teamCode])) {
        return $teamImages[$teamCode];
    }
    return null;
}

//
/////////// Yahoo API helper code functions ////////////
//

function logit($msg,$preamble=true)
{
  //date_default_timezone_set('America/Los_Angeles');
  $now = date(DateTime::ISO8601, time());
  error_log(($preamble ? "+++${now}:" : '') . $msg);
}

function do_statsnbacom_request($url) {
    list($info, $headers, $contents) = do_get(
        $url,
        80,
        array(
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
            "Accept-Language: en-US,en;q=0.8",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Pragma: no-cache",
            "Upgrade-Insecure-Requests: 1"
        )
    );

    return $contents;
}

/**
 * Do an HTTP GET
 * @param string $url
 * @param int $port (optional)
 * @param array $headers an array of HTTP headers (optional)
 * @return array ($info, $header, $response) on success or empty array on error.
 */
function do_get($url, $port=80, $headers=NULL)
{
  $retarr = array();  // Return value

  $curl_opts = array(CURLOPT_URL => $url,
                     CURLOPT_PORT => $port,
                     CURLOPT_POST => false,
                     CURLOPT_SSL_VERIFYHOST => false,
                     CURLOPT_SSL_VERIFYPEER => false,
                     CURLOPT_RETURNTRANSFER => true);

  if ($headers) { $curl_opts[CURLOPT_HTTPHEADER] = $headers; }

  $response = do_curl($curl_opts);

  if (! empty($response)) { $retarr = $response; }

  return $retarr;
}

/**
 * Do an HTTP POST
 * @param string $url
 * @param int $port (optional)
 * @param array $headers an array of HTTP headers (optional)
 * @return array ($info, $header, $response) on success or empty array on error.
 */
function do_post($url, $postbody, $port=80, $headers=NULL)
{
  $retarr = array();  // Return value

  $curl_opts = array(CURLOPT_URL => $url,
                     CURLOPT_PORT => $port,
                     CURLOPT_POST => true,
                     CURLOPT_SSL_VERIFYHOST => false,
                     CURLOPT_SSL_VERIFYPEER => false,
                     CURLOPT_POSTFIELDS => $postbody,
                     CURLOPT_RETURNTRANSFER => true);

  if ($headers) { $curl_opts[CURLOPT_HTTPHEADER] = $headers; }

  $response = do_curl($curl_opts);

  if (! empty($response)) { $retarr = $response; }

  return $retarr;
}

/**
 * Make a curl call with given options.
 * @param array $curl_opts an array of options to curl
 * @return array ($info, $header, $response) on success or empty array on error.
 */
function do_curl($curl_opts)
{
  global $debug;

  $retarr = array();  // Return value

  if (! $curl_opts) {
    if ($debug) { logit("do_curl:ERR:curl_opts is empty"); }
    return $retarr;
  }

  // Open curl session
  $ch = curl_init();
  if (! $ch) {
    if ($debug) { logit("do_curl:ERR:curl_init failed"); }
    return $retarr;
  }

  // Set curl options that were passed in
  curl_setopt_array($ch, $curl_opts);

  // Ensure that we receive full header
  curl_setopt($ch, CURLOPT_HEADER, true);

  if ($debug) {
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
  }

  // Send the request and get the response
  ob_start();
  $response = curl_exec($ch);
  $curl_spew = ob_get_contents();
  ob_end_clean();
  if ($debug && $curl_spew) {
    logit("do_curl:INFO:curl_spew begin");
    logit($curl_spew, false);
    logit("do_curl:INFO:curl_spew end");
  }

  // Check for errors
  if (curl_errno($ch)) {
    $errno = curl_errno($ch);
    $errmsg = curl_error($ch);
    if ($debug) { logit("do_curl:ERR:$errno:$errmsg"); }
    curl_close($ch);
    unset($ch);
    return $retarr;
  }

  if ($debug) {
    logit("do_curl:DBG:header sent begin");
    $header_sent = curl_getinfo($ch, CURLINFO_HEADER_OUT);
    logit($header_sent, false);
    logit("do_curl:DBG:header sent end");
  }

  // Get information about the transfer
  $info = curl_getinfo($ch);

  // Parse out header and body
  $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
  $header = substr($response, 0, $header_size);
  $body = substr($response, $header_size );

  // Close curl session
  curl_close($ch);
  unset($ch);

  if ($debug) {
    logit("do_curl:DBG:response received begin");
    if (!empty($response)) { logit($response, false); }
    logit("do_curl:DBG:response received end");
  }

  // Set return value
  array_push($retarr, $info, $header, $body);

  return $retarr;
}

/**
 * Pretty print some JSON
 * @param string $json The packed JSON as a string
 * @param bool $html_output true if the output should be escaped
 * (for use in HTML)
 * @link http://us2.php.net/manual/en/function.json-encode.php#80339
 */
function json_pretty_print($json, $html_output=false)
{
  $spacer = '  ';
  $level = 1;
  $indent = 0; // current indentation level
  $pretty_json = '';
  $in_string = false;

  $len = strlen($json);

  for ($c = 0; $c < $len; $c++) {
    $char = $json[$c];
    switch ($char) {
    case '{':
    case '[':
      if (!$in_string) {
        $indent += $level;
        $pretty_json .= $char . "\n" . str_repeat($spacer, $indent);
      } else {
        $pretty_json .= $char;
      }
      break;
    case '}':
    case ']':
      if (!$in_string) {
        $indent -= $level;
        $pretty_json .= "\n" . str_repeat($spacer, $indent) . $char;
      } else {
        $pretty_json .= $char;
      }
      break;
    case ',':
      if (!$in_string) {
        $pretty_json .= ",\n" . str_repeat($spacer, $indent);
      } else {
        $pretty_json .= $char;
      }
      break;
    case ':':
      if (!$in_string) {
        $pretty_json .= ": ";
      } else {
        $pretty_json .= $char;
      }
      break;
    case '"':
      if ($c > 0 && $json[$c-1] != '\\') {
        $in_string = !$in_string;
      }
    default:
      $pretty_json .= $char;
      break;
    }
  }

  return ($html_output) ?
    '<pre>' . htmlentities($pretty_json) . '</pre>' :
    $pretty_json . "\n";
}

/**
 * Get a request token.
 * @param string $consumer_key obtained when you registered your app
 * @param string $consumer_secret obtained when you registered your app
 * @param string $callback callback url can be the string 'oob'
 * @param bool $usePost use HTTP POST instead of GET
 * @param bool $useHmacSha1Sig use HMAC-SHA1 signature
 * @param bool $passOAuthInHeader pass OAuth credentials in HTTP header
 * @return array of response parameters or empty array on error
 */
function get_request_token($consumer_key, $consumer_secret, $callback, $usePost=false, $useHmacSha1Sig=true, $passOAuthInHeader=false)
{
  $retarr = array();  // return value
  $response = array();

  $url = 'https://api.login.yahoo.com/oauth/v2/get_request_token';
  $params['oauth_version'] = '1.0';
  $params['oauth_nonce'] = mt_rand();
  $params['oauth_timestamp'] = time();
  $params['oauth_consumer_key'] = $consumer_key;
  $params['oauth_callback'] = $callback;

  // compute signature and add it to the params list
  if ($useHmacSha1Sig) {
    $params['oauth_signature_method'] = 'HMAC-SHA1';
    $params['oauth_signature'] =
      oauth_compute_hmac_sig($usePost? 'POST' : 'GET', $url, $params,
                             $consumer_secret, null);
  } else {
    $params['oauth_signature_method'] = 'PLAINTEXT';
    $params['oauth_signature'] =
      oauth_compute_plaintext_sig($consumer_secret, null);
  }

  // Pass OAuth credentials in a separate header or in the query string
  if ($passOAuthInHeader) {
    $query_parameter_string = oauth_http_build_query($params, true);
    $header = build_oauth_header($params, "yahooapis.com");
    $headers[] = $header;
  } else {
    $query_parameter_string = oauth_http_build_query($params);
  }

  // POST or GET the request
  if ($usePost) {
    $request_url = $url;
    logit("getreqtok:INFO:request_url:$request_url");
    logit("getreqtok:INFO:post_body:$query_parameter_string");
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    $response = do_post($request_url, $query_parameter_string, 443, $headers);
  } else {
    $request_url = $url . ($query_parameter_string ?
                           ('?' . $query_parameter_string) : '' );
    logit("getreqtok:INFO:request_url:$request_url");
    $response = do_get($request_url, 443, $headers);
  }

  // extract successful response
  if (! empty($response)) {
    list($info, $header, $body) = $response;
    $body_parsed = oauth_parse_str($body);
    if (! empty($body_parsed)) {
      logit("getreqtok:INFO:response_body_parsed:");
      //print_r($body_parsed);
    }
    $retarr = $response;
    $retarr[] = $body_parsed;
  }

  return $retarr;
}

/**
 * Get an access token using a request token and OAuth Verifier.
 * @param string $consumer_key obtained when you registered your app
 * @param string $consumer_secret obtained when you registered your app
 * @param string $request_token obtained from getreqtok
 * @param string $request_token_secret obtained from getreqtok
 * @param string $oauth_verifier obtained from step 3
 * @param bool $usePost use HTTP POST instead of GET
 * @param bool $useHmacSha1Sig use HMAC-SHA1 signature
 * @param bool $passOAuthInHeader pass OAuth credentials in HTTP header
 * @return array of response parameters or empty array on error
 */
function get_access_token($consumer_key, $consumer_secret, $request_token, $request_token_secret, $oauth_verifier, $usePost=false, $useHmacSha1Sig=true, $passOAuthInHeader=true)
{
  $retarr = array();  // return value
  $response = array();

  $url = 'https://api.login.yahoo.com/oauth/v2/get_token';
  $params['oauth_version'] = '1.0';
  $params['oauth_nonce'] = mt_rand();
  $params['oauth_timestamp'] = time();
  $params['oauth_consumer_key'] = $consumer_key;
  $params['oauth_token']= $request_token;
  $params['oauth_verifier'] = $oauth_verifier;

  // compute signature and add it to the params list
  if ($useHmacSha1Sig) {
    $params['oauth_signature_method'] = 'HMAC-SHA1';
    $params['oauth_signature'] =
      oauth_compute_hmac_sig($usePost? 'POST' : 'GET', $url, $params,
                             $consumer_secret, $request_token_secret);
  } else {
    $params['oauth_signature_method'] = 'PLAINTEXT';
    $params['oauth_signature'] =
      oauth_compute_plaintext_sig($consumer_secret, $request_token_secret);
  }

  // Pass OAuth credentials in a separate header or in the query string
  if ($passOAuthInHeader) {
    $query_parameter_string = oauth_http_build_query($params, true);
    $header = build_oauth_header($params, "yahooapis.com");
    $headers[] = $header;
  } else {
    $query_parameter_string = oauth_http_build_query($params);
  }

  // POST or GET the request
  if ($usePost) {
    $request_url = $url;
    logit("getacctok:INFO:request_url:$request_url");
    logit("getacctok:INFO:post_body:$query_parameter_string");
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    $response = do_post($request_url, $query_parameter_string, 443, $headers);
  } else {
    $request_url = $url . ($query_parameter_string ?
                           ('?' . $query_parameter_string) : '' );
    logit("getacctok:INFO:request_url:$request_url");
    $response = do_get($request_url, 443, $headers);
  }

  // extract successful response
  if (! empty($response)) {
    list($info, $header, $body) = $response;
    $body_parsed = oauth_parse_str($body);
    if (! empty($body_parsed)) {
      logit("getacctok:INFO:response_body_parsed:");
      //print_r($body_parsed);
    }
    $retarr = $response;
    $retarr[] = $body_parsed;
  }

  return $retarr;
}

/**
 * Refresh an access token using an expired request token
 * @param string $consumer_key obtained when you registered your app
 * @param string $consumer_secret obtained when you registered your app
 * @param string $old_access_token obtained previously
 * @param string $old_token_secret obtained previously
 * @param string $oauth_session_handle obtained previously
 * @param bool $usePost use HTTP POST instead of GET (default false)
 * @param bool $useHmacSha1Sig use HMAC-SHA1 signature (default false)
 * @return response string with token or empty array on error
 */
function refresh_access_token($consumer_key, $consumer_secret, $old_access_token, $old_token_secret, $oauth_session_handle, $usePost=false, $useHmacSha1Sig=true, $passOAuthInHeader=true)
{
  $retarr = array();  // return value
  $response = array();

  $url = 'https://api.login.yahoo.com/oauth/v2/get_token';
  $params['oauth_version'] = '1.0';
  $params['oauth_nonce'] = mt_rand();
  $params['oauth_timestamp'] = time();
  $params['oauth_consumer_key'] = $consumer_key;
  $params['oauth_token'] = $old_access_token;
  $params['oauth_session_handle'] = $oauth_session_handle;

  // compute signature and add it to the params list
  if ($useHmacSha1Sig) {
    $params['oauth_signature_method'] = 'HMAC-SHA1';
    $params['oauth_signature'] =
      oauth_compute_hmac_sig($usePost? 'POST' : 'GET', $url, $params,
                             $consumer_secret, $old_token_secret);
  } else {
    $params['oauth_signature_method'] = 'PLAINTEXT';
    $params['oauth_signature'] =
      oauth_compute_plaintext_sig($consumer_secret, $old_token_secret);
  }

  // Pass OAuth credentials in a separate header or in the query string
  if ($passOAuthInHeader) {
    $query_parameter_string = oauth_http_build_query($params, true);
    $header = build_oauth_header($params, "yahooapis.com");
    $headers[] = $header;
  } else {
    $query_parameter_string = oauth_http_build_query($params);
  }

  // POST or GET the request
  if ($usePost) {
    $request_url = $url;
    logit("refacctok:INFO:request_url:$request_url");
    logit("refacctok:INFO:post_body:$query_parameter_string");
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    $response = do_post($request_url, $query_parameter_string, 443, $headers);
  } else {
    $request_url = $url . ($query_parameter_string ?
                           ('?' . $query_parameter_string) : '' );
    logit("refacctok:INFO:request_url:$request_url");
    $response = do_get($request_url, 443, $headers);
  }

  // extract successful response
  if (! empty($response)) {
    list($info, $header, $body) = $response;
    $body_parsed = oauth_parse_str($body);
    if (! empty($body_parsed)) {
      logit("getacctok:INFO:response_body_parsed:");
      //print_r($body_parsed);
    }
    $retarr = $response;
    $retarr[] = $body_parsed;
  }

  return $retarr;
}

/**
 * Call the Yahoo API with YQL
 * @param string $consumer_key obtained when you registered your app
 * @param string $consumer_secret obtained when you registered your app
 * @param string $yql The YQL to query
 * @param string $access_token obtained from getacctok
 * @param string $access_token_secret obtained from getacctok
 * @param bool $usePost use HTTP POST instead of GET
 * @param bool $passOAuthInHeader pass the OAuth credentials in HTTP header
 * @return response string with token or empty array on error
 */
function call_yql($consumer_key, $consumer_secret, $query, $access_token, $access_token_secret, $usePost=false, $passOAuthInHeader=true)
{
    $url = "http://query.yahooapis.com/v1/yql";
    $params = array('q' => $query);
    return call_url($consumer_key, $consumer_secret, $url, $params, $access_token, $access_token_secret, $usePost, $passOAuthInHeader);
}

/**
 * Call the Yahoo API with a URL
 * @param string $consumer_key obtained when you registered your app
 * @param string $consumer_secret obtained when you registered your app
 * @param string $url The URL to call
 * @param string $params The custom parameters to add to the call
 * @param string $access_token obtained from getacctok
 * @param string $access_token_secret obtained from getacctok
 * @param bool $usePost use HTTP POST instead of GET
 * @param bool $passOAuthInHeader pass the OAuth credentials in HTTP header
 * @return response string with token or empty array on error
 */
function call_url($consumer_key, $consumer_secret, $url, $params, $access_token, $access_token_secret, $usePost=false, $passOAuthInHeader=true)
{
  $retarr = array();  // return value
  $response = array();

  // Add common parameters for the call
  $params['format'] = 'json';
  $params['oauth_version'] = '1.0';
  $params['oauth_nonce'] = mt_rand();
  $params['oauth_timestamp'] = time();
  $params['oauth_consumer_key'] = $consumer_key;
  $params['oauth_token'] = $access_token;

  // compute hmac-sha1 signature and add it to the params list
  $params['oauth_signature_method'] = 'HMAC-SHA1';
  $params['oauth_signature'] =
      oauth_compute_hmac_sig($usePost? 'POST' : 'GET', $url, $params,
                             $consumer_secret, $access_token_secret);

  // Pass OAuth credentials in a separate header or in the query string
  if ($passOAuthInHeader) {
    $query_parameter_string = oauth_http_build_query($params, true);
    $header = build_oauth_header($params, "yahooapis.com");
    $headers[] = $header;
  } else {
    $query_parameter_string = oauth_http_build_query($params);
  }

  //print_r($params);
  //print_r($query_parameter_string);

  // POST or GET the request
  if ($usePost) {
    $request_url = $url;
    logit("call_yql:INFO:request_url:$request_url");
    logit("call_yql:INFO:post_body:$query_parameter_string");
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    $response = do_post($request_url, $query_parameter_string, 80, $headers);
  } else {
    $request_url = $url . ($query_parameter_string ?
                           ('?' . $query_parameter_string) : '' );
    logit("call_yql:INFO:request_url:$request_url");
    $response = do_get($request_url, 80, $headers);
  }

  // extract successful response
  if (! empty($response)) {
    list($info, $header, $body) = $response;
    if ($body) {
      logit("call_yql:INFO:response:");
      //print(json_pretty_print($body));
    }
    $retarr = $response;
  }

  return $retarr;
}
