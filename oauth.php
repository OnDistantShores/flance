<?php

require_once("common.php");

$urlModifier = ""; //"/new"; // TEMP

$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : die("OAuth action not specified");

if ($action == "get-request-token") {

    $callback = URL . $urlModifier . "/oauth.php?action=get-access-token";

    // Get the request token using HTTP GET and HMAC-SHA1 signature
    $retarr = get_request_token(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $callback, false, true, true);

    if (!empty($retarr)) {
        list($info, $headers, $body, $body_parsed) = $retarr;
        if ($info['http_code'] == 200 && !empty($body)) {
            //print "Have the user go to xoauth_request_auth_url to authorize your app\n" . rfc3986_decode($body_parsed['xoauth_request_auth_url']) . "\n";

            $_SESSION["oauth_request_token"] = $body_parsed["oauth_token"];
            $_SESSION["oauth_request_token_secret"] = $body_parsed["oauth_token_secret"];

            header("Location: " . rfc3986_decode($body_parsed['xoauth_request_auth_url']));
            exit;
        }
    }

}
else if ($action == "get-access-token") {

    $oauth_verifier = (isset($_REQUEST["oauth_verifier"]) ? $_REQUEST["oauth_verifier"] : null);
    if (!$oauth_verifier) {
        die("OAuth failed - missing callback parameter - 'oauth_verifier'.");
    }

    $request_token = $_SESSION["oauth_request_token"];
    $request_token_secret = $_SESSION["oauth_request_token_secret"];

    // Get the access token using HTTP GET and HMAC-SHA1 signature
    $retarr = get_access_token(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $request_token, $request_token_secret, $oauth_verifier, false, true, true);

    if (! empty($retarr)) {
        list($info, $headers, $body, $body_parsed) = $retarr;
        if ($info['http_code'] == 200 && !empty($body)) {
            //print "Use oauth_token as the token for all of your API calls:\n" . rfc3986_decode($body_parsed['oauth_token']) . "\n";

            $_SESSION["oauth_access_token"] = rfc3986_decode($body_parsed['oauth_token']);
            $_SESSION["oauth_access_token_secret"] = $body_parsed["oauth_token_secret"];
            $_SESSION["oauth_session_handle"] = $body_parsed["oauth_session_handle"];
            $_SESSION["xoauth_yahoo_guid"] = $body_parsed["xoauth_yahoo_guid"];

            header("Location: addTeamsYahoo.php");
            exit;
        }
    }
}
else if ($action == "refresh-access-token") {

    $old_access_token = getSessionParameter("oauth_access_token");
    $old_token_secret = getSessionParameter("oauth_access_token_secret");
    $oauth_session_handle = getSessionParameter("oauth_session_handle");

    // Refresh the access token using HTTP GET and HMAC-SHA1 signature
    $retarr = refresh_access_token(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $old_access_token, $old_token_secret, $oauth_session_handle, false, true, true);
    if (! empty($retarr)) {
        list($info, $headers, $body, $body_parsed) = $retarr;
        if ($info['http_code'] == 200 && !empty($body)) {
            //print "Use oauth_token as the token for all of your API calls:\n" . rfc3986_decode($body_parsed['oauth_token']) . "\n";

            $_SESSION["oauth_access_token"] = rfc3986_decode($body_parsed['oauth_token']);
            $_SESSION["oauth_access_token_secret"] = $body_parsed["oauth_token_secret"];
            $_SESSION["oauth_session_handle"] = $body_parsed["oauth_session_handle"];
            $_SESSION["xoauth_yahoo_guid"] = $body_parsed["xoauth_yahoo_guid"];

            header("Location: addTeamsYahoo.php");
            exit;
        }
    }
}

// If we can't successfully authenticate, go back to the teams page
header("Location: editTeams.php?yahooError=1");
exit;
