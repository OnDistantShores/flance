<?php

    require_once("common.php");

    echo "Starting" . PHP_EOL;

    $servicePlayerList = serviceRequestNbaGetPlayerListByName();

    echo "Retrieved " . count($servicePlayerList) . " players" . PHP_EOL;

    $sql = "SELECT id, yahoo_id, name FROM player ORDER BY id";
    if ($q = mysqli_query(getDbConnection(), $sql)) {
        while ($r = mysqli_fetch_assoc($q)) {
            $nameParts = nameParts($r["name"]);
            $nameLastFirst = $nameParts[1] . ", " . $nameParts[0];

            if (isset($servicePlayerList[$nameLastFirst])) {
                $sql = "UPDATE player "
                        . " SET nba_id = '" . $servicePlayerList[$nameLastFirst]["PERSON_ID"] . "' "
                        . " WHERE id = " . $r["id"];
                mysqli_query(getDbConnection(), $sql);

                echo "Player '" . $r["name"] . "' updated<br />" . PHP_EOL;
            }
            else {
                echo "Player '" . $r["name"] . "' NOT FOUND<br />" . PHP_EOL;
            }
        }
    }

    echo "Done" . PHP_EOL;
