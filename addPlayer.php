<?php

    require_once("common.php");

    if (!isset($_REQUEST["nba_id"])) {
        header("Location: searchPlayers.php");
        exit;
    }

    $referrer = (isset($_REQUEST["referrer"]) ? $_REQUEST["referrer"] : "home.php");

    $nbaId = isset($_REQUEST["nba_id"]) ? $_REQUEST["nba_id"] : null;
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : null;

    $teams = getTeamsForCurrentUser();
    if (count($teams) == 0) {
        header("Location: doAddPlayer.php?nba_id=" . urlencode($nbaId) ."&name=" . urlencode($name));
        exit;
    }

    printHeader();
?>

    <h1>Add player</h1>

    <form method="GET" action="doAddPlayer.php">
        <p><strong><?php echo $name; ?></strong> will be added to your list. Select the teams he should be added to.</p>
        <p><em>Note: If no teams are specified, he will be added to your "watch" list.</em></p>

        <?php foreach ($teams as $team): ?>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="teams[]" value="<?php echo $team["id"]; ?>" /><?php echo $team["name"]; ?>
                </label>
            </div>
        <?php endforeach; ?>

        <p><button type="submit" class="btn btn-success">Add player</button></p>

        <input type="hidden" name="nba_id" value="<?php echo $nbaId; ?>" />
        <input type="hidden" name="name" value="<?php echo $name; ?>" />
    </form>

    <p class="breadcrumbs"><a href="<?php echo $referrer; ?>">&lt; Back</a></p>

<?php
    printFooter();
