<?php

require_once("common.php");
//require_once("helper.php");

session_start();

define('CLIENT_ID', 'dj0yJmk9NmhpdVdocFN4MzQ0JmQ9WVdrOWRIZFNlRXBRTXpJbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1hOQ--');
define('CLIENT_SECRET', 'a9bdaee4afed1475a395154a062a8571a1f39064');
define('CALLBACK_URL', 'https://flance.invitationstation.org/leagueSync.php');
define('AUTHORIZATION_ENDPOINT', 'https://api.login.yahoo.com/oauth2/request_auth');
define('TOKEN_ENDPOINT', 'https://api.login.yahoo.com/oauth2/get_token');
define('API_BASE_URL', 'https://fantasysports.yahooapis.com/fantasy/v2');

/*
Logic:
- store league key in session
- if a refresh token stored in session, do oauth refresh stuff
- if a get code is provided, do oauth post-callback stuff
- if either of teh above two were successful, do the fetch data stuff
- otherwise, do the initial oauth call stuff, with this file as the callback
*/

$readyToLoadYahooApiData = false;

if (isset($_REQUEST['teamKey'])) {
    $_SESSION['teamKey'] = $_REQUEST['teamKey'];
}

// If we already have a refresh token, then use it to get a new access token. No new auth required
if (isset($_SESSION['refresh_token'])) {
    $ch = curl_init(TOKEN_ENDPOINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
        'client_id' => CLIENT_ID,
        'client_secret' => CLIENT_SECRET,
        'refresh_token' => $_SESSION['refresh_token'],
        'grant_type' => 'refresh_token'
    ]));
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/x-www-form-urlencoded'
    ]);

    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_code !== 200) {
        die("Error: HTTP code $http_code received from refresh call.");
    }

    $token_data = json_decode($response, true);
    $_SESSION['access_token'] = $token_data['access_token'];
    $_SESSION['refresh_token'] = $token_data['refresh_token'] ?? $_SESSION['refresh_token'];

    $readyToLoadYahooApiData = true;
}
// If we got a code, that means this is the callback from Yahoo. Get a new access token & store the refresh token
else if (isset($_GET['code'])) {
    $code = $_GET['code'];

    if (!isset($_GET['state']) || !isset($_SESSION['oauth_state']) || $_GET['state'] !== $_SESSION['oauth_state']) {
        die("Error: Invalid state parameter in callback.");
    }

    // Exchange code for access token
    $ch = curl_init(TOKEN_ENDPOINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
        'client_id' => CLIENT_ID,
        'client_secret' => CLIENT_SECRET,
        'redirect_uri' => CALLBACK_URL,
        'code' => $code,
        'grant_type' => 'authorization_code'
    ]));
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/x-www-form-urlencoded'
    ]);

    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_code !== 200) {
        die("Error: HTTP code $http_code received from token call in callback. Response: $response");
    }

    $token_data = json_decode($response, true);
    if (!isset($token_data['access_token'])) {
        die('Error: Access token not found in response');
    }

    $_SESSION['access_token'] = $token_data['access_token'];
    $_SESSION['refresh_token'] = $token_data['refresh_token'] ?? null;

    $readyToLoadYahooApiData = true;
}
// Otherwise, do the initial oauth call stuff, with this file as the callback
else {
    $state = bin2hex(random_bytes(16));
    $_SESSION['oauth_state'] = $state;

    $params = [
        'client_id' => CLIENT_ID,
        'redirect_uri' => CALLBACK_URL,
        'response_type' => 'code',
        'state' => $state,
        'language' => 'en-us',
        'scope' => 'openid fspt-r'  // Updated scope for read access to fantasy sports
    ];

    $auth_url = AUTHORIZATION_ENDPOINT . '?' . http_build_query($params);
    //echo "calling auth URL $auth_url";exit;
    header('Location: ' . $auth_url);
}

if ($readyToLoadYahooApiData) {

    $teamKey = $_SESSION['teamKey'];
    $leagueKey = getYahooLeagueKeyFromTeamKey($teamKey);

    class YahooFantasyAPI {
        private $access_token;

        public function __construct($access_token) {
            $this->access_token = $access_token;
        }

        private function makeRequest($endpoint) {
            $ch = curl_init(API_BASE_URL . $endpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $this->access_token,
                'Accept: application/xml'  // Changed to XML
            ]);

            $response = curl_exec($ch);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            //echo "code = $http_code, response = $response";

            if ($http_code !== 200) {
                throw new Exception('API request failed with status ' . $http_code . ': ' . $response);
            }

            // Convert XML to SimpleXMLElement
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($response);
            if ($xml === false) {
                $errors = libxml_get_errors();
                libxml_clear_errors();
                throw new Exception('XML parsing failed: ' . print_r($errors, true));
            }

            return $xml;
        }

        public function getUserLeagues() {
            return $this->makeRequest('/users;use_login=1/games;game_keys=nba/leagues');
        }

        public function getLeagueTeams($league_key) {
            return $this->makeRequest("/league/$league_key/teams");
        }

        public function getTeamRoster($team_key) {
            return $this->makeRequest("/team/$team_key/roster/players");
        }

        public function getFreeAgents($league_key) {
            return $this->makeRequest("/league/$league_key/players;status=FA");
        }

        public function getAllPlayers($league_key) {
            return $this->makeRequest("/league/$league_key/players");
        }
    }

    function matchPlayerIdFromYahooPlayer($player) {
        $fullName = (string)$player->name->full;

        $results = serviceRequestNbaSearchPlayers($fullName);
        if (count($results) > 0) {
            // Assume the first result is right
            $nbaPlayerRecord = $results[0];
            return $nbaPlayerRecord["PERSON_ID"];
        }

        return null;
    }

    try {
        if (!isset($_SESSION['access_token'])) {
            throw new Exception('Access token not found in session');
        }

        $api = new YahooFantasyAPI($_SESSION['access_token']);

        $export_data = [
            'last_updated' => date("Y-m-d H:i:s"),
            'teams' => [],
            'free_agents' => []
        ];

        //echo "<pre>";

        // Get teams in league
        $teams_data = $api->getLeagueTeams($leagueKey);
        $teams = $teams_data->league->teams->team;
        foreach ($teams as $team) {
            $team_key = (string)$team->team_key;
            $roster_data = $api->getTeamRoster($team_key);

            //print_r($roster_data);

            $team_data = [
                'team_name' => (string)$team->name,
                'team_key' => $team_key,
                'players' => [],
                'players_not_matched' => []
            ];

            // Parse roster players
            $players = $roster_data->team->roster->players->player;
            foreach ($players as $player) {
                $playerData = [
                    'yahoo_id' => (string)$player->player_id,
                    'nba_id' => null,
                    'name' => (string)$player->name->full,
                    'status' => ($player->status ? (string)$player->status : null),
                    'position' => ($player->selected_position  && $player->selected_position->position ? (string)$player->selected_position->position : null),
                ];

                $matchedPlayerId = matchPlayerIdFromYahooPlayer($player);
                if ($matchedPlayerId) {
                    $playerData['nba_id'] = $matchedPlayerId;
                    $team_data['players'][] = $playerData;
                }
                else {
                    $team_data['players_not_matched'][] = $playerData;
                }
            }

            $export_data['teams'][] = $team_data;
        }

        // Get free agents
        $free_agents_data = $api->getFreeAgents($leagueKey);
        $free_agents = $free_agents_data->league->players->player;
        foreach ($free_agents as $player) {
            $export_data['free_agents'][] = [
                'player_id' => (string)$player->player_id,
                'name' => (string)$player->name->full
            ];
        }

        //print_r($export_data);

        // Add error checking for file writing
        $leaguesDataFilename = getSyncedLeagueDataFileName($leagueKey);
        if (file_put_contents($leaguesDataFilename, json_encode($export_data, JSON_PRETTY_PRINT)) === false) {
            throw new Exception('Failed to write data to file');
        }

        // TODO Update roster?

        // Redirect back to the home page
        header('Location: home.php');
    }
    catch (Exception $e) {
        die("Error when fetching Yahoo data: " . $e->getMessage());
    }
}
