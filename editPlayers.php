<?php

    require_once("common.php");

    ensureLoggedIn();

    $players = getPlayersForCurrentUser(true);
    $teams = getTeamsForCurrentUser();

    printHeader();
?>

    <?php
        if (isset($_REQUEST["added"]) || isset($_REQUEST["edited"]) || isset($_REQUEST["deleted"]) || isset($_REQUEST["allDeleted"]) || isset($_REQUEST["hidden"])) {

            $message = null;
            if (isset($_REQUEST["edited"])) {
                $message = "Successfully edited player's teams.";
            }
            else if (isset($_REQUEST["deleted"])) {
                $message = "Player successfully deleted.";
            }
            else if (isset($_REQUEST["allDeleted"])) {
                $message = "All your players were successfully deleted.";
            }
            else if (isset($_REQUEST["hidden"])) {
                $message = "Player has been " . ($_REQUEST["hidden"] == 1 ? "hidden for today" : "unhidden");
            }
            else {
                $message = "Player successfully added.";
            }
    ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $message; ?>
        </div>
    <?php } ?>

    <h1>Edit my players</h1>

    <p><a href='searchPlayers.php'><button type='button' class='btn btn-primary btn-sm'>Add another player</button></a>
        <a href='deleteAllPlayers.php'><button type='button' class='btn btn-danger btn-sm'>Delete entire player list</button></a></p>

    <?php if (count($players) > 0): ?>
        <div class="row">
            <table class="table players">
                <tr>
                    <th>Name</th>
                    <?php
                        foreach ($teams as $team) {
                            echo "<th>" . $team["short_name"] . "</th>";
                        }
                    ?>
                    <th>Action</th>
                </tr>
                <?php
                    foreach ($players as $player) {

                        echo "<form method='post' action='doEditPlayer.php'>" . PHP_EOL;
                        echo "<input type='hidden' name='id' value='" . $player["id"] . "' />";

                        echo "<tr>" . PHP_EOL;

                        echo "<td><a href='player.php?referrer=" . urlencode("editPlayers.php") . "&nba_id=" . $player["nba_id"] . "'>" . $player["name"] . "</a></td>";

                        foreach ($teams as $team) {
                            $checked = in_array($team["id"], $player["teams"]);
                            echo "<td><input type='checkbox' name='teams[]' value='" . $team["id"] . "' " . ($checked ? "checked" : "") . " /></td>";
                        }

                        echo "<td>";
                            echo "<button type='submit' class='btn btn-primary btn-xs'>Save</button>";
                            echo " <a href='doDeletePlayer.php?id=" . $player["id"] . "'><button type='button' class='btn btn-danger btn-xs'>Delete</button></a>";
                        echo "</td>";

                        echo "</tr>" . PHP_EOL;

                        echo "</form>" . PHP_EOL;
                    }
                ?>
            </table>
        </div>
    <?php endif; ?>

    <p><em>Note: If no teams are specified for a player, he will be on your "watch" list.</em></p>

    <p class="breadcrumbs"><a href="home.php">&lt; Back to Home</a></p>

<?php
    printFooter();
