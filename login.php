<?php

    require_once("common.php");

    if (getSessionParameter("user_id")) {
        header("Location: home.php");
        exit;
    }

    $email = isset($_REQUEST["email"]) ? $_REQUEST["email"] : null;
    $errorMessage = isset($_REQUEST["error"]) ? $_REQUEST["error"] : null;

    printHeader();
?>

    <h1>Login to Flance</h1>

    <?php if ($errorMessage): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            There was a problem logging in: <?php echo $errorMessage; ?>
        </div>
    <?php endif; ?>

    <form class="form-horizontal well" method="post" action="doLogin.php">

        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email address</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="email" name="email" placeholder="Your email address" value="<?php echo $email; ?>" />
          </div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="Your password" value="" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Continue</button>
          </div>
        </div>
    </form>

    <p class="breadcrumbs"><a href="index.php">&lt; Back to landing page</a></p>

<?php
    printFooter();
