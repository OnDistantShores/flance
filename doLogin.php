<?php

    require_once("common.php");

    $email = isset($_REQUEST["email"]) ? $_REQUEST["email"] : null;
    $password = isset($_REQUEST["password"]) ? $_REQUEST["password"] : null;

    $errorMessage = null;
    if (!$email) {
        $errorMessage = "no email address was provided";
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errorMessage = "the email address entered was not valid";
    }
    else if (!$password) {
        $errorMessage = "no password was provided";
    }
    else if (!($user = validateAndGetUser($email, $password))) {
        $errorMessage = "the email address or password entered was not correct. Please try again.";
    }

    if ($errorMessage) {
        header("Location: login.php?email=" . urlencode($email) . "&error=" . urlencode($errorMessage));
        exit;
    }

    $_SESSION["user_id"] = $user["id"];

    updateUserLastLogin();

    header("Location: home.php");
