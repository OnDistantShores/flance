<?php

    // TODO Permissions of the OAuth library/Yahoo API boilerplate code? Acknowledge?

    require_once("common.php");

    ensureLoggedIn();

    updateUserLastLoad();

    // Shared functions

    function ratingSort($playerOne, $playerTwo) {
        if ($playerOne["rating"] > $playerTwo["rating"]) {
            return -1;
        }
        else {
            return 1;
        }
    }

    function rankPlayers($players) {
        global $user;

        $playerRatings = array();

        // Default to the starting list in case we can't rank them
        $rankedPlayers = $players;

        // Create a list of all players in the list with their rating. Save the original "player" object inside.
        foreach ($players as $playerCode => $player) {
            $totalLineValue = getFullLineStatRankingValue($player, $user["show_turnovers"]);
            $playerRatings[] = array(
                "playerCode" => $playerCode,
                "rating" => $totalLineValue,
                "player" => $player,
            );
        }

        if (count($playerRatings) > 0) {

            // Rank the players
            usort($playerRatings, "ratingSort");

            // Pull the original "player" object out of the rating object
            $rankedPlayers = array();
            foreach ($playerRatings as $playerRating) {
                $rankedPlayers[$playerRating["playerCode"]] = $playerRating["player"];
            }
        }

        return $rankedPlayers;
    }

    function printPlayersTable($players, $className = "", $showMatchInfo = true, $totalsLines = array()) {
        global $dateObject;
        global $user;
        global $matchesByTeam;
        global $userTeams;

        echo "<table class='table playersData " . $className . "'>";
        echo "<tr><th>Player</th>";
        printGameDataHeaders($user["show_turnovers"]);
        echo "</tr>";

        // Print the player lines

        foreach ($players as $player) {

            if (!isPlayerHiddenForToday($player['nba_id'])) {

                $gameTimeIndex = 0;

                $match = (isset($player["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player["TEAM_ABBREVIATION"])] : null);
                if ($match && isset($match["quarter"]) && isset($match["clock"])) {
                    $gameTimeIndex = getGameTimePercentage($match["quarter"], $match["clock"]);
                }

                $teamFilterString = (isset($player["teams"]) ? implode(",", array_intersect($player["teams"], array_keys($userTeams))) : "best");
                echo "<tr class='player " . (!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == "" ? "not-playing" : "") . "' data-teams='" . $teamFilterString . "'>" . PHP_EOL;

                $matchSummary = "";
                if ($match) {
                    $opponent = ($match["home"] ? "" : "@") . $match["opponent"];

                    if ($match["status"] == MATCH_STATUS_UPCOMING) {
                        $matchSummary = $opponent;
                    }
                    else if ($match["status"] == MATCH_STATUS_COMPLETE) {
                        $matchSummary = $opponent . " final";
                    }
                    else if ($match["status"] == MATCH_STATUS_IN_PROGRESS) {
                        $matchSummary = $opponent;

                        if ($match["quarter"] == 2 && trim($match["clock"]) == "") {
                            $matchSummary .= " half";
                        }
                        else {
                            $matchSummary .= " Q" . $match["quarter"] . " " . (trim($match["clock"]) && !($match["clock"] == "0.0") ? $match["clock"] : "end");
                        }
                    }
                }

                $totalLineValue = getFullLineStatRankingValue($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                $colorString = getCssColourStringForStatRankingValue($totalLineValue);

                $nameCellStyle = "background-color: " . $colorString . "; color: black !important;";
                $nameCellClass = "match-complete custom-color";

                $nameParts = nameParts(isset($player["name"]) ? $player["name"] : nameLastCommaFirstToFirstLast($player["DISPLAY_LAST_COMMA_FIRST"]));
                echo "<td class='name " . $nameCellClass . "' style='" . $nameCellStyle . "'>";
                echo "<span class='name'>";
                echo "<a href='player.php?nba_id=" . (isset($player["PERSON_ID"]) ? $player["PERSON_ID"] : $player["nba_id"]) . "'>" . substr($nameParts[0], 0, 1) . ". " . $nameParts[1] . "</a></span>";

                echo " <span class='match-info'>on " . $player["DATE"] . "</span>";

                echo "</td>" . PHP_EOL;

                if (isset($player["STATUS"]) && $player["STATUS"] != "") {
                    echo "<td class='data status' colspan='11'>" . $player["STATUS"] . "</td>" . PHP_EOL;
                }
                else if ($match["status"] == MATCH_STATUS_UPCOMING) {
                    $startTimeText = "soon";
                    if ($match["minutesUntilStart"]) {
                        if ($match["minutesUntilStart"] >= 60) {
                            $startTimeText = "in " . round($match["minutesUntilStart"] / 60) . "h";
                        }
                        else {
                            $startTimeText = "in " . $match["minutesUntilStart"] . "m";
                        }
                    }

                    echo "<td class='data status' colspan='11'>Game starting " . $startTimeText . "</td>" . PHP_EOL;
                }
                else if ((!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == 0) && $match["status"] == MATCH_STATUS_COMPLETE) {
                    echo "<td class='data status' colspan='11'>Never entered the game</td>" . PHP_EOL;
                }
                else if ((!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == 0) && $match["status"] == MATCH_STATUS_IN_PROGRESS) {
                    echo "<td class='data status' colspan='11'>Yet to enter the game</td>" . PHP_EOL;
                }
                else {
                    if ($user["show_colourful_view"]) {
                        printGameDataRowColourful($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                    }
                    else {
                        printGameDataRowSimple($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                    }
                }

                echo "</tr>" . PHP_EOL;
            }
        }

        echo "</table>";
    }

    function playerSort($player1, $player2) {
        global $matchesByTeam;
        global $user;

        $player1match = (isset($player1["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player1["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player1["TEAM_ABBREVIATION"])] : null);
        $player2match = (isset($player2["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player2["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player2["TEAM_ABBREVIATION"])] : null);

        // Create a little indexing system to determine the order

        $player1matchValue = 0;
        if ($player1match) {
            if (isset($player1["STATUS"]) && $player1["STATUS"] != "") { // Injury
                $player1matchValue = 1;
            }
            else if ((!isset($player1[STAT_ID_MIN]) || $player1[STAT_ID_MIN] == 0) && $player1match["status"] == MATCH_STATUS_COMPLETE) {
                $player1matchValue = 2;
            }
            else if ((!isset($player1[STAT_ID_MIN]) || $player1[STAT_ID_MIN] == 0) && $player1match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player1matchValue = 1001;
            }
            else if ($player1match["status"] == MATCH_STATUS_IN_PROGRESS && $player1[STAT_ID_MIN] == 0) {
                $player1matchValue = 2001;
            }
            else if ($player1match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player1matchValue = 3000 + getGameTimePercentage($player1match["quarter"], $player1match["clock"]);
            }
            else if ($player1match["status"] == MATCH_STATUS_COMPLETE) {
                $player1matchValue = 2000;
            }
            else if ($player1match["status"] == MATCH_STATUS_UPCOMING) {
                $player1matchValue = 1000;
            }
        }

        $player2matchValue = 0;
        if ($player2match) {
            if (isset($player2["STATUS"]) && $player2["STATUS"] != "") { // Injury
                $player2matchValue = 1;
            }
            else if ((!isset($player2[STAT_ID_MIN]) || $player2[STAT_ID_MIN] == 0) && $player2match["status"] == MATCH_STATUS_COMPLETE) {
                $player2matchValue = 2;
            }
            else if ((!isset($player2[STAT_ID_MIN]) || $player2[STAT_ID_MIN] == 0) && $player2match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player2matchValue = 1001;
            }
            else if ($player2match["status"] == MATCH_STATUS_IN_PROGRESS && $player2[STAT_ID_MIN] == 0) {
                $player2matchValue = 2001;
            }
            else if ($player2match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player2matchValue = 3000 + getGameTimePercentage($player2match["quarter"], $player2match["clock"]);
            }
            else if ($player2match["status"] == MATCH_STATUS_COMPLETE) {
                $player2matchValue = 2000;
            }
            else if ($player2match["status"] == MATCH_STATUS_UPCOMING) {
                $player2matchValue = 1000;
            }
        }

        if ($player1matchValue == $player2matchValue) {
            // These players are equally ranked in terms of game progress - rank in terms of line value
            $player1TotalLineValue = getFullLineStatRankingValue($player1, $user["show_turnovers"], null);
            $player2TotalLineValue = getFullLineStatRankingValue($player2, $user["show_turnovers"], null);

            if ($player1TotalLineValue > $player2TotalLineValue) {
                return -1;
            }
            else if ($player1TotalLineValue < $player2TotalLineValue) {
                return 1;
            }
            else {
                // Line value is also identical - go alphabetical
                $player1NameParts = nameParts($player1["name"]);
                $player2NameParts = nameParts($player2["name"]);
                return strcmp($player1NameParts[1], $player2NameParts[1]);
            }
        }
        else if ($player1matchValue > $player2matchValue) {
            return -1;
        }
        else {
            return 1;
        }
    }

    // Global variables

    $numberOfBestPlayersToShow = 30;
    $debugMode = (isset($_REQUEST["debug"]) && $_REQUEST["debug"] == "1");

    $user = getUser(getSessionParameter("user_id"));

    $userPlayers = getPlayersForCurrentUser(true);
    $userTeams = getTeamsForCurrentUser();

    $bestPlayers = array();

    // Work out which date we should be looking at
    if (isset($_REQUEST["date"]) && strtotime($_REQUEST["date"]) > 0) {
        $dateObject = new DateTime($_REQUEST["date"]);
    }
    else {
        $dateObject = getEffectiveTodayDateObject();
    }

    //$dateObject = new DateTime("2016-10-10", new DateTimeZone('America/Los_Angeles'));


    $dates = array();
    $date = getEffectiveTodayDateObject();
    while (strtotime($date->format("Y-m-d")) >= strtotime("2017-10-16")) {
        $dates[] = $date;
        $date = clone $date;
        $date->sub(new DateInterval("P1D"));
    }

    $allPlayerLines = array();
    $servicePlayerList = serviceRequestNbaGetPlayerListById();
    foreach ($dates as $date) {
        $serviceGamesData = serviceRequestNbaGetGamesData($date);
        if (count($serviceGamesData) > 0) {
            //$matchesByTeam = getMatchesByTeam($serviceGamesData);
            $servicePlayerDayData = serviceRequestNbaGetPlayerData($date, array_keys($serviceGamesData["games"]));

            $servicePlayerDayDataReKeyed = array();
            foreach ($servicePlayerDayData as $code => $player) {
                $servicePlayerDayDataReKeyed[$code . "." . $date->format("d/m")] = $player;
            }

            $allPlayerLines = array_merge($allPlayerLines, $servicePlayerDayDataReKeyed);
        }
    }

    //echo "<pre>";
    //print_r($allPlayerLines);

    $allPlayersRanked = rankPlayers($allPlayerLines);
    $allPlayersByCode = serviceRequestNbaGetPlayerListByCode();

    $allLinesBestPlayers = array();

    foreach ($allPlayersRanked as $key => $player) {
        list($playerCode, $date) = explode(".", $key);
        if (isset($allPlayersByCode[$playerCode])) {
            $allLinesBestPlayers[] = array_merge($player, $allPlayersByCode[$playerCode], array("DATE" => $date));
        }
    }

    /*echo PHP_EOL;
    echo "BEST PLAYERS";
    echo PHP_EOL;
    print_r($allLinesBestPlayers);

    echo PHP_EOL;
    echo PHP_EOL;
    echo "BEST PLAYERS - PRETTY";
    echo PHP_EOL;
    printPlayersTable($allLinesBestPlayers);*/







    // Get the game data

    $serviceGamesData = serviceRequestNbaGetGamesData($dateObject);
    $matchesByTeam = getMatchesByTeam($serviceGamesData);

    if ($debugMode) {
        echo str_replace(PHP_EOL, "<br />", str_replace(" ", "&nbsp;", print_r($serviceGamesData, true)));
    }

    $currentGames = false;
    foreach ($matchesByTeam as $match) {
        if ($match["status"] == MATCH_STATUS_IN_PROGRESS) {
            $currentGames = true;
        }
    }

    // Get the player data

    $errorLoadingGameData = false;
    $servicePlayerList = serviceRequestNbaGetPlayerListById();
    $servicePlayerDayData = array();
    $allPlayersByCode = array();

    if ($serviceGamesData && isset($serviceGamesData["games"])) {
        $servicePlayerDayData = serviceRequestNbaGetPlayerData($dateObject, array_keys($serviceGamesData["games"]));

        // Add the NBA stats data to the user player record, if they exist

        $combinedPlayers = array();
        foreach ($userPlayers as $player) {
            $newPlayer = $player;

            // Add the player details
            if (isset($servicePlayerList[$player["nba_id"]])) {
                $newPlayer = $newPlayer + $servicePlayerList[$player["nba_id"]];

                $playerCode = strtolower($servicePlayerList[$player["nba_id"]]["PLAYERCODE"]);

                // Now that we have the player code, add the day's stats (if they exist)
                if (isset($servicePlayerDayData[$playerCode])) {
                    $newPlayer = $newPlayer + $servicePlayerDayData[$playerCode];
                }
            }

            $combinedPlayers[] = $newPlayer;
        }
        $userPlayers = $combinedPlayers;


        // Determine the totals for each team of owned players

        $teamTotals = array();
        $statsConfig = getStatsConfig($user["show_turnovers"]);
        foreach ($userPlayers as $player) {
            $playerTeams = array_intersect($player["teams"], array_keys($userTeams));

            // If they are in at least one team and played today
            if ($player[STAT_ID_MIN] > 0 && count($playerTeams)) {

                // Add another "team line" - one to cover all owned players
                $playerTeams[] = "owned";

                // Go through each team for this player and each of their stats to the list
                foreach ($playerTeams as $playerTeam) {
                    if (!isset($teamTotals[$playerTeam])) {
                        $teamTotals[$playerTeam] = array(
                            STAT_ID_GP => 0
                        );
                    }

                    $teamTotals[$playerTeam][STAT_ID_GP]++;

                    foreach ($statsConfig as $statId => $statConfig) {
                        if (!isset($teamTotals[$playerTeam][$statId])) {
                            if ($statConfig["type"] == "percentage") {
                                $teamTotals[$playerTeam][$statConfig["madeTotalId"]] = 0;
                                $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]] = 0;
                            }
                            $teamTotals[$playerTeam][$statId] = 0;
                        }

                        if ($statConfig["type"] == "percentage") {
                            $teamTotals[$playerTeam][$statConfig["madeTotalId"]] += $player[$statConfig["madeTotalId"]];
                            $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]] += $player[$statConfig["attemptedTotalId"]];

                            $teamTotals[$playerTeam][$statId] = $teamTotals[$playerTeam][$statConfig["madeTotalId"]] / $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]];
                        }
                        else {
                            $teamTotals[$playerTeam][$statId] += $player[$statId];
                        }
                    }
                }
            }
        }


        // Extract the best {$numberOfBestPlayersToShow} performanes, based on overall line values
        // While games are in progress, the "best" tab order probably won't match the order of the colouring of those lines.
        // This is because the "best" ranking is based on the line as of right now, with no weighting for how long the game
        // has been in progress. If we wanted to add this, we could add the game time index to the getFullLineStatRankingValue()
        // call below. But it's probably better to show the "best" list based on final line value, otherwise when a player
        // scores a 3 in the first minute of the game it will show them as crazy good on a per-minute weighted rating.

        $allPlayersRanked = rankPlayers($servicePlayerDayData);
        $allPlayersByCode = serviceRequestNbaGetPlayerListByCode();

        foreach (array_slice($allPlayersRanked, 0, $numberOfBestPlayersToShow, true) as $playerCode => $player) {
            if (isset($allPlayersByCode[$playerCode])) {
                $bestPlayers[] = array_merge($player, $allPlayersByCode[$playerCode]);
            }
        }
    }
    else {
        $errorLoadingGameData = true;
    }

    printHeader();
?>
    <script type="text/javascript">

        $(document).ready(function() {

            // Cookie functions from http://www.quirksmode.org/js/cookies.html
            function createCookie(name,value,days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                }
                else var expires = "";
                document.cookie = name+"="+value+expires+"; path=/";
            }
            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
            function eraseCookie(name) {
                createCookie(name,"",-1);
            }

            // Manage filters

            var teamFilterCookieName = "teamFilter";
            var teamFilterTeamIdCookieName = "teamFilterTeamId";

            // Players tab filter
            $("#tab-players div.team-filter button").click(function() {
                var id = $(this).attr("id");

                // Save the cookie
                createCookie(teamFilterCookieName, id, 7);
                if (id == "team-filter-specific") {
                    createCookie(teamFilterTeamIdCookieName, $(this).attr("data-team"), 7);
                }

                // Update the button states
                $("#tab-players div.team-filter button").removeClass("btn-primary");
                $("#tab-players div.team-filter button").addClass("btn-default");
                $(this).addClass("btn-primary");

                // Re-hide all players & totals, and then show depending on the filter chosen

                $("table.playersData.main tr.player").hide();
                $("table.playersData.main tr.totals").hide();

                if (id == "team-filter-best") {
                    $("table.playersData.main tr.player[data-teams = 'best']").show();
                }
                else if (id == "team-filter-owned") {
                    $("table.playersData.main tr.player[data-teams != ''][data-teams != 'best']").show();
                    $("table.playersData.main tr.totals[data-teams = 'owned']").show();
                }
                else if (id == "team-filter-watch") {
                    $("table.playersData.main tr.player[data-teams = '']").show();
                }
                else if (id == "team-filter-specific") {
                    var teamId = $(this).attr("data-team");

                    // We have to loop through players manually. Load up all the players without a blank team list.
                    $("table.playersData.main tr.player[data-teams != '']").each(function() {
                        var teams = $(this).attr("data-teams");
                        var teamsArray = teams.split(",");
                        for (var i = 0; i < teamsArray.length; i++) {
                            if (teamsArray[i] == teamId) {
                                $(this).show();
                            }
                        }
                    });

                    // Team totals lines are marked individually
                    $("table.playersData.main tr.totals[data-teams = '" + teamId + "']").show();
                }
                else { // "all", being "all my list", including owned and watch. Show everything except "best".
                    $("table.playersData.main tr.player[data-teams != 'best']").show();
                }
            });

            // News tab filter
            $("#tab-news div.team-filter button").click(function() {
                var id = $(this).attr("id");

                // Save the cookie
                createCookie(teamFilterCookieName, id, 7);
                if (id == "team-filter-specific") {
                    createCookie(teamFilterTeamIdCookieName, $(this).attr("data-team"), 7);
                }

                // Update the button states
                $("#tab-news div.team-filter button").removeClass("btn-primary");
                $("#tab-news div.team-filter button").addClass("btn-default");
                $(this).addClass("btn-primary");

                var showRowsForPlayers = function(players) {
                    $("table.playersNews tr").each(function(index, element) {
                        for (var i = 0; i < players.length; i++) {
                            if (element.innerHTML.indexOf(players[i]) !== -1) {
                                $(this).show();
                            }
                        }
                    });
                };

                // Re-hide all players, and then show depending on the filter chosen
                $("table.playersNews tr").hide();

                if (id == "team-filter-all") { // "all", being "all my list", including owned and watch
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            echo $sep . "\"" . $player["name"] . "\"";
                            $sep = ", ";
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-owned") {
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            if (count($player["teams"]) > 0) {
                                echo $sep . "\"" . $player["name"] . "\"";
                                $sep = ", ";
                            }
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-watch") {
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            if (count($player["teams"]) == 0) {
                                echo $sep . "\"" . $player["name"] . "\"";
                                $sep = ", ";
                            }
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-specific") {
                    var teamId = $(this).attr("data-team");

                    var players = [];
                    <?php
                        $teamIds = array();
                        foreach ($userTeams as $team) {
                            echo "players[" . $team["id"] . "] = [];" . PHP_EOL;
                            $teamIds[] = $team["id"];
                        }

                        foreach ($userPlayers as $player) {
                            foreach ($player["teams"] as $teamId) {
                                // Protect against orphaned player_team records that below to deleted teams
                                if (in_array($teamId, $teamIds)) {
                                    echo "players[" . $teamId . "].push(\"" . $player["name"] . "\");" . PHP_EOL;
                                }
                            }
                        }
                    ?>

                    showRowsForPlayers(players[teamId]);
                }
                else {
                    // Entire league news
                    $("table.playersNews tr").show();
                }
            });

            // Handle expand/collapse of games
            $("#tab-games div.match").each(function(index, match) {
                $("div.match-summary", match).click(function() {
                    if ($("div.match-players", match).is(':visible')) {
                        // Hide the player list for this match if it's visible
                        $("div.match-players", match).hide();
                    }
                    else {
                        // Hide all of the player lists
                        $("#tab-games div.match-players").hide();
                        // Show the player list for this match
                        $("div.match-players", match).show();
                        // Scroll to the top of the game selected
                        $("html, body").animate({
                            scrollTop: $(this).offset().top
                        }, 500);
                    }
                });
            });

            // Set the default filters if the cookie exists, otherwise default to "owned"
            var teamFilterCookieValue = readCookie(teamFilterCookieName);
            if (teamFilterCookieValue == null) {
                teamFilterCookieValue = "team-filter-owned";
            }

            if (teamFilterCookieValue == "team-filter-specific") {
                var teamFilterTeamIdCookieValue = readCookie(teamFilterTeamIdCookieName);
                if (teamFilterTeamIdCookieValue != null) {
                    $("div.team-filter button[data-team = '" + teamFilterTeamIdCookieValue + "']").click();
                }
            }
            else {
                $("div.team-filter button#" + teamFilterCookieValue).click();
            }

            // Expand/collapse controls handling

            var collapseControlsCookieName = "collapseControls";

            $("#controls-expand-collapse").click(function() {
                if ($(".controls-collapsible").first().is(":visible")) {
                    $(".controls-collapsible").hide();
                    $("#controls-expand-collapse span")
                        .removeClass("glyphicon-resize-small")
                        .addClass("glyphicon-resize-full");
                    createCookie(collapseControlsCookieName, 1, 7);
                }
                else {
                    $("div.controls-collapsible").show();
                    $("#controls-expand-collapse span")
                        .removeClass("glyphicon-resize-full")
                        .addClass("glyphicon-resize-small");
                    createCookie(collapseControlsCookieName, 0, 7);
                }
            });

            var collapseControlsCookieValue = readCookie(collapseControlsCookieName);
            if (collapseControlsCookieValue == null) {
                collapseControlsCookieValue = 1;
            }

            if (collapseControlsCookieValue == 0) {
                $("div.controls-collapsible").show();
                $("#controls-expand-collapse span")
                    .removeClass("glyphicon-resize-full")
                    .addClass("glyphicon-resize-small");
            }

            // Auto-refresh handling

            (function() {

                function startTimer(refreshTime) {
                    var totalTimeElapsed = 0;

                    $(".auto-refresh .countdown").html(refreshTime);

                    var handle = setInterval(function() {
                        // Only increment the timer if the players tab is showing
                        if ($('#tab-players').is(":visible")) {
                            totalTimeElapsed++;

                            if ($(".auto-refresh input").prop('checked')) {
                                if (totalTimeElapsed >= refreshTime) {
                                    window.location.href = "home.php";
                                    clearInterval(handle);
                                }
                                else {
                                    $(".auto-refresh .countdown").html(refreshTime - totalTimeElapsed);
                                }
                            }
                            else {
                                clearInterval(handle);
                            }
                        }
                    }, 1000);
                }

                $("#refresh").click(function() {
                    window.location.href = "home.php";
                });

                var autoRefreshCookieName = "autoRefresh";

                var refreshTime = 60;
                $(".auto-refresh .countdown").html(refreshTime);

                var cookieValue = readCookie(autoRefreshCookieName);
                if (cookieValue == null) {
                    cookieValue = 0;
                }

                // Don't set the auto-refresh if there's no games in progress, but don't affect the cookie - it will take
                // effect again as soon as there are games in progress.
                <?php if ($currentGames) { ?>
                    if (cookieValue == 1) {
                        $(".auto-refresh input").prop('checked', true);
                        $(".auto-refresh .question-mark").hide();
                        startTimer(refreshTime);
                    }
                <?php } ?>

                $(".auto-refresh input").change(function() {
                    if ($(".auto-refresh input").prop('checked')) {
                        $(".auto-refresh .question-mark").hide();
                        createCookie(autoRefreshCookieName, 1, 7);
                        startTimer(refreshTime);
                    }
                    else {
                        createCookie(autoRefreshCookieName, 0, 7);
                        $(".auto-refresh .countdown").html(refreshTime);
                        $(".auto-refresh .question-mark").show();
                    }
                });

            })();

            // Check the cookie for the colourful view message
            /*if (readCookie("dismissedColourfulViewIntroMessage") != 1) {
                $(".colourfulViewIntroMessage").show();
                $(".colourfulViewIntroMessage button").click(function() {
                    createCookie("dismissedColourfulViewIntroMessage", 1, 30);
                });
            }*/

            // Check the cookie for the colourful view message
            /*if (readCookie("dismissedNewSeasonMessage") != 1) {
                $(".newSeasonMessage").show();
                $(".newSeasonMessage button").click(function() {
                    createCookie("dismissedNewSeasonMessage", 1, 365);
                });
            }*/

            // Set up the tabs
            var defaultTabCookieName = "defaultTab";

            $('#home-tabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            });
            $('#home-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                createCookie(defaultTabCookieName, e.target.hash, 7);
            });

            // Preset the tab selection based on the cookie
            var defaultTabCookieValue = readCookie(defaultTabCookieName);
            if (defaultTabCookieValue != null) {
                $('#home-tabs a[href="' + defaultTabCookieValue + '"]').tab('show');
            }

        });

    </script>

    <?php if (isset($_REQUEST["userCreated"]) && $_REQUEST["userCreated"] == 1): ?>
        <div class="alert alert-success alert-dismissible currentGamesAlert" role="alert">
            Your user account has been created!
        </div>
    <?php endif; ?>

    <?php if (count($userTeams) == 0 && count($userPlayers) == 0): ?>

        <h1 class="title-home clear">Daily dashboard</h1>

        <p>Welcome! This is your <strong>Flance</strong> dashboard where you will keep track of all your players&apos; daily stats.</p>
        <p>But before we can do that, we have to configure your fantasy teams. Go to <a href="editTeams.php">Edit my teams</a> to get started.</p>

    <?php elseif (count($userPlayers) == 0): ?>

        <h1 class="title-home clear">Daily dashboard</h1>

        <p>This is your <strong>Flance</strong> dashboard where you will keep track of your team&s daily stats.</p>
        <p>You&apos;ve set up your teams, so the last step is to set up the list of players you want to keep track of.
            Go to <a href="searchPlayers.php">Find a teams</a> to start adding players to your list.</p>

    <?php else: ?>

        <!-- Temporary message for start of a new season -->
        <div class="alert alert-info alert-dismissible newSeasonMessage" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Welcome to fantasy season 2016-17! To clean your <strong>Flance</strong> profile for your new teams, you can delete your
            old player list from the <a href="editPlayers.php">Edit players page</a>, and delete and set up your new teams on the
            <a href="editTeams.php">Edit teams page</a>. Good luck!
        </div>

        <h1 class="title-home">Seasons&#39;s best</h1>

        <div>

            <!-- Nav tabs -->
            <div class="row" id="home-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-players" aria-controls="tab-players" role="tab" data-toggle="tab">Players</a></li>
                </ul>
            </div>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="tab-players">

                    <div class="row">
                    <?php
                        printPlayersTable($allLinesBestPlayers, "main", true, $teamTotals);
                    ?>
                    </div>
                </div>

            </div>

        </div>

    <?php endif; ?>

<?php
    printFooter();
