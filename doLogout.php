<?php

    require_once("common.php");

    cleanSession();
    cleanCookies();

    header("Location: index.php");
