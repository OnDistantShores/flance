<?php

    require_once("common.php");

    $existingUser = (getSessionParameter("user_id") ? true : false);
    $email = isset($_REQUEST["email"]) ? $_REQUEST["email"] : null;
    $errorMessage = isset($_REQUEST["error"]) ? $_REQUEST["error"] : null;

    printHeader();
?>

    <h1>Create a Flance account</h1>

    <?php if ($existingUser): ?>
        <p>Flance has recently been updated. You will now need to log in with an email address and password instead of your Yahoo account. There's a whole lot of new features too!</p>
        <p>Please set up your new account details now.</p>
    <?php endif; ?>

    <?php if ($errorMessage): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            There was an error creating a user account: <?php echo $errorMessage; ?>
        </div>
    <?php endif; ?>

    <form class="form-horizontal well" method="post" action="doCreateUser.php">

        <div class="form-group">
          <label for="email" class="col-sm-2 control-label">Email address</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="email" name="email" placeholder="Your email address" value="<?php echo $email; ?>" />
            <p class="help-block">We won't use your email address for anything except important Flance-related messages. No marketing!</p>
          </div>
        </div>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="Your password" value="" />
            <p class="help-block">Passwords must be between 6 and 20 characters</p>
          </div>
        </div>
        <div class="form-group">
          <label for="confirm-password" class="col-sm-2 control-label">Confirm password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="Confirm your password" value="" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Continue</button>
          </div>
        </div>
    </form>

    <?php if (getSessionParameter("user_id")): ?>
        <p class="breadcrumbs"><a href="doLogout.php">I don't want to set up an account - log me out</a></p>
    <?php else: ?>
        <p class="breadcrumbs"><a href="index.php">&lt; Back to landing page</a></p>
    <?php endif; ?>

<?php
    printFooter();
