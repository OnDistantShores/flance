<?php

    require_once("common.php");

    ensureLoggedIn();

    $searchText = isset($_REQUEST["searchText"]) ? $_REQUEST["searchText"] : null;

    $allPlayers = serviceRequestNbaGetPlayerListByName();
    $currentPlayers = getPlayersForCurrentUser();
    $teams = getTeamsForCurrentUser();

    printHeader();
?>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#searchText").focus();

            $("form.playerSearch").submit(function() {
                return false;
            });

            var onSearchTextChange = function() {
                $("p.noSearch").hide();
                $("p.noMatches").hide();
                $("table.searchResults").hide();
                $("table.searchResults tr").hide();

                var text = $("#searchText").val().trim();
                if (text.length > 0) {
                    var matchesFound = 0;

                    $("table.searchResults tr").each(function(index, row) {
                        var name = $(row).data("name");
                        if (name && name.length > 0) {
                            if (name.toLowerCase().indexOf(text.toLowerCase()) > -1) {
                                $(row).show();
                                matchesFound++;
                            }
                        }
                    });

                    if (matchesFound) {
                        $("table.searchResults").show();
                        $("table.searchResults tr.header").show();
                    }
                    else {
                        $("p.noMatches").show();
                    }
                }
                else {
                    $("p.noSearch").show();
                }
            };

            $("#searchText").keyup(onSearchTextChange);
            onSearchTextChange();
        });

    </script>

    <h1>Find a player</h1>

    <form class="form-inline playerSearch">
        <div class="form-group">
            <input type="text" autocomplete="off" class="form-control" name="searchText" id="searchText" placeholder="Search for a player..." value="<?php echo $searchText; ?>" />
        </div>
    </form>

    <div class="row">

        <p class="noSearch" style="display: none;"><em>Start typing in the search box to find players.</em></p>

        <p class="noMatches" style="display: none;"><em>Sorry, no matches were found.</em></p>

        <table class="table searchResults" style="display: none;">
            <tr class="header">
                <th>Name</th>
                <th>Team</th>
                <th>Action</th>
            </tr>
            <?php

                /*echo "<pre>";
                print_r($allPlayers);
                exit;*/

                foreach ($allPlayers as $playerName => $player) {
                    if ($player["TO_YEAR"] >= 2020) {

                        echo "<tr data-name=\"" . $playerName . "  " . nameLastCommaFirstToFirstLast($playerName) . "\" style='display: none;'>" . PHP_EOL;

                        echo "<td><a href='player.php?referrer=" . urlencode("searchPlayers.php?searchText=" . urlencode($searchText)) . "&nba_id=" . $player["PERSON_ID"] . "'>" . $player["PLAYER_FIRST_NAME"] . " " . $player["PLAYER_LAST_NAME"] . "</a></td>";
                        echo "<td>" . $player["TEAM_ABBREVIATION"] . "</td>";

                        $onList = false;
                        foreach ($currentPlayers as $currentPlayer) {
                            if ($currentPlayer["nba_id"] == $player["PERSON_ID"]) {
                                $onList = true;
                                break;
                            }
                        }

                        if ($onList) {
                            echo "<td><em>Already on list</em></td>";
                        }
                        else {
                            echo "<td>";
                            foreach ($teams as $team) {
                                echo "<a href='doAddPlayer.php?teams[]=" . $team["id"] . "&nba_id=" . $player["PERSON_ID"] ."&name=" . urlencode($player["PLAYER_FIRST_NAME"] . " " . $player["PLAYER_LAST_NAME"]) . "'><button type='button' class='btn btn-primary btn-xs'>Add to " . $team["short_name"] . "</button></a> ";
                            }
                            echo "<a href='doAddPlayer.php?nba_id=" . $player["PERSON_ID"] ."&name=" . urlencode($player["PLAYER_FIRST_NAME"] . " " . $player["PLAYER_LAST_NAME"]) . "'><button type='button' class='btn btn-primary btn-xs'>Watch</button></a>";
                            echo "</td>";
                        }

                        echo "</tr>" . PHP_EOL;
                    }
                }
            ?>
        </table>
    </div>

    <p class="breadcrumbs"><a href="home.php">&lt; Back to Home</a></p>

<?php
    printFooter();
