<?php

require_once("common.php");

if (!isPlayerHiddenForToday($_REQUEST['nba_id'])) {
	setPlayerHiddenForToday($_REQUEST['nba_id'], true);
	header( 'Location: editPlayers.php?hidden=1' ) ;
}
else {
	setPlayerHiddenForToday($_REQUEST['nba_id'], false);
	header( 'Location: editPlayers.php?hidden=0' ) ;
}
