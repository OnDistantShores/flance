<?php

    require_once("common.php");

    $showTurnovers = isset($_REQUEST["show-turnovers"]) ? $_REQUEST["show-turnovers"] : null;
    $showColourfulView = isset($_REQUEST["show-colourful-view"]) ? $_REQUEST["show-colourful-view"] : null;
    $showMatchScores = isset($_REQUEST["show-match-scores"]) ? $_REQUEST["show-match-scores"] : null;
    $showRankingByTime = isset($_REQUEST["show-ranking-by-time"]) ? $_REQUEST["show-ranking-by-time"] : null;

    editSettings($showTurnovers, $showColourfulView, $showMatchScores, $showRankingByTime);

    header("Location: settings.php?edited=1");
