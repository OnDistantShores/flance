<?php

    require_once("common.php");

    ensureLoggedIn();

    $user = getUser(getSessionParameter("user_id"));
    $errorMessage = isset($_REQUEST["error"]) ? $_REQUEST["error"] : null;

    printHeader();
?>

    <?php if ($errorMessage): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            There was an error: <?php echo $errorMessage; ?>
        </div>
    <?php elseif (isset($_REQUEST["edited"])) : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Settings successfully updated
        </div>
    <?php elseif (isset($_REQUEST["passwordChanged"])) : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Password successfully updated
        </div>
    <?php endif; ?>

    <h1><?php echo $title; ?></h1>

    <form class="form-horizontal well" method="post" action="doEditSettings.php">
        <h3>Change your settings</h3>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="show-turnovers" value="1" <?php echo ($user["show_turnovers"] == 1 ? "checked" : ""); ?> /> <strong>Show turnovers?</strong>
                </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="show-colourful-view" value="1" <?php echo ($user["show_colourful_view"] == 1 ? "checked" : ""); ?> /> <strong>Show colourful view?</strong>
                    This view gives a more granular analysis of each stat and how valuable it is, as well as an overall ranking of the player's line. But it can be a bit bright!
                </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="show-ranking-by-time" value="1" <?php echo ($user["show_ranking_by_time"] == 1 ? "checked" : ""); ?> /> <strong>Show ranking by time?</strong>
                    Allow stat and full-line rankings to be weighted by how long the game has been going, to get a better idea of how the line is playing out through the game.
                    Obviously this unfairly favours starters.
                </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="show-match-scores" value="1" <?php echo ($user["show_match_scores"] == 1 ? "checked" : ""); ?> /> <strong>Show match scores?</strong>
                </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Save settings</button>
          </div>
        </div>
    </form>

    <form class="form-horizontal well" method="post" action="doEditPassword.php">
        <h3>Change your password</h3>
        <div class="form-group">
          <label for="password" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="password" name="password" placeholder="Your password" value="" />
            <p class="help-block">Passwords must be between 6 and 20 characters</p>
          </div>
        </div>
        <div class="form-group">
          <label for="confirm-password" class="col-sm-2 control-label">Confirm password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="Confirm your password" value="" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Save password</button>
          </div>
        </div>
    </form>

    <p class="breadcrumbs"><a href="home.php">&lt; Back to Home</a></p>

<?php
    printFooter();
