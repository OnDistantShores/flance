<?php

    require_once("common.php");

    ensureLoggedIn();

    $teams = getTeamsForCurrentUser();

    printHeader();
?>

    <?php
        if (isset($_REQUEST["added"]) || isset($_REQUEST["edited"]) || isset($_REQUEST["deleted"])) {

            $message = null;
            if (isset($_REQUEST["edited"])) {
                $message = "Team successfully edited.";
            }
            else if (isset($_REQUEST["deleted"])) {
                $message = "Team successfully deleted.";
            }
            else {
                $message = "Team successfully added.";
            }
    ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $message; ?>
        </div>
    <?php } else if (isset($_REQUEST["yahooError"])) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            There was a problem with the Yahoo! authentication process. Please try again.
        </div>
    <?php } ?>

    <h1>Edit my teams</h1>

    <?php if (count($teams) > 0): ?>
        <div class="row">
            <table class="table teams">
                <tr>
                    <th>Name</th>
                    <th>Short name</th>
                    <th>Action</th>
                </tr>
                <?php
                    foreach ($teams as $team) {

                        echo "<tr>" . PHP_EOL;

                        echo "<td>" . $team["name"] . "</td>";
                        echo "<td>" . $team["short_name"] . "</td>";
                        echo "<td>";
                            echo "<a href='editTeam.php?id=" . $team["id"] . "'><button type='button' class='btn btn-primary btn-xs'>Edit</button></a>";
                            echo " <a href='doDeleteTeam.php?id=" . $team["id"] . "'><button type='button' class='btn btn-danger btn-xs'>Delete</button></a>";
                        echo "</td>";

                        echo "</tr>" . PHP_EOL;
                    }
                ?>
            </table>
        </div>
    <?php endif; ?>

    <div class="action-buttons"><a href='editTeam.php'><button type='button' class='btn btn-primary btn-sm'>Add a new team manually</button></a></div>
    <div class="action-buttons"><a href='oauth.php?action=get-request-token'><button type='button' class='btn btn-primary btn-sm'>Get my teams from Yahoo</button></a></div>
    <div class="action-buttons"><a href='addTeamEspn.php'><button type='button' class='btn btn-primary btn-sm'>Add an ESPN team</button></a></div>

    <p class="breadcrumbs"><a href="home.php">&lt; Back to Home</a></p>

<?php
    printFooter();
