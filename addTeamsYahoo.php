<?php

    require_once("common.php");

    $userTeams = serviceRequestYahooSaveTeams();

    printHeader();
?>

    <h1>Adding Yahoo teams</h1>

    <?php

        if (count($userTeams) > 0) {
            echo '<div class="alert alert-success alert-dismissible" role="alert"><strong>Success!</strong> Some of your Yahoo teams have been automatically added.</div>';

            foreach ($userTeams as $userTeam) {
                if ($userTeam["id"]) {
                    echo "<p>The team <strong>" . $userTeam["name"] . "</strong> was added with the short name '<strong>" . $userTeam["shortName"] . "</strong>'. ";

                    if (count($userTeam["playersAdded"]) > 0) {
                        echo count($userTeam["playersAdded"]) . " player" . (count($userTeam["playersAdded"]) > 1 ? "s" : "") . " were matched and saved. ";
                    }

                    if (count($userTeam["playersNotFound"]) > 0) {
                        echo "But " . count($userTeam["playersNotFound"]) . " player" . (count($userTeam["playersNotFound"]) > 1 ? "s" : "")
                            . " (" . implode(", ", $userTeam["playersNotFound"]) . ") <strong>could not be automatically matched</strong>. Please try "
                            . "<a href='searchPlayers.php'>manually adding " . (count($userTeam["playersNotFound"]) > 1 ? "these players" : "this player") . "</a>.";
                    }

                    echo "</p>";
                }
                else {
                    echo "<p>The team <strong>" . $userTeam["name"] . "</strong> could not be saved. Please try again. ";
                }
            }

            echo "<p>Please note that your teams will not regularly synchronise with Yahoo. When your roster changes, this will need to be manually
                 updated in <strong>Flance</strong>.</p>";
        }
        else {
            echo '<div class="alert alert-warning alert-dismissible" role="alert">'
                . '<strong>Sorry</strong>, no teams could be automatically added at this time. Please try again, or try <a href="addTeam.php">manually adding your teams</a>.'
                . '</div>';
        }

    ?>

    <p class="breadcrumbs"><a href="editTeams.php">&lt; Back to Edit my teams</a></p>

<?php
    printFooter();
