<?php

    require_once("common.php");

    if (!isset($_REQUEST["nba_id"])) {
        header("Location: searchPlayers.php");
        exit;
    }

    $nbaId = isset($_REQUEST["nba_id"]) ? $_REQUEST["nba_id"] : null;
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : null;
    $teams = isset($_REQUEST["teams"]) ? $_REQUEST["teams"] : array();

    addPlayerToList($nbaId, $name, $teams);

    header("Location: editPlayers.php?added=1");
