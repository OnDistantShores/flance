<?php

require_once("constants.php");
require_once("dbconstants.php");
require_once("helper.php");
require_once("oauth_helper.php");

// Ensure sessions last for a month
$length = 60*60*24*30;
session_set_cookie_params($length);
ini_set('session.gc_maxlifetime', $length);
ini_set('session.cookie_lifetime', $length);
ini_set('session.cache_expire', $length);
ini_set('session.save_path', '/home3/invitat8/public_html/flance/sessions'); 

session_start();

//saveSessionToCookiesIfExists();
connectToDb();
