<?php
    $debugMode = (isset($_REQUEST["debug"]) && $_REQUEST["debug"] == "1");

    if ($debugMode) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    }

    require_once("common.php");

    ensureLoggedIn();

    updateUserLastLoad();

    // If the data file hasn't been touched in the last 2 minutes, force an update

    $lastTouchDateDataUpdateFile = filemtime("data/updateLog.log");
    if ($lastTouchDateDataUpdateFile < (time() - 120)) {
        $url = str_replace("home.php", "dataUpdate.php", $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']) . "?quiet=1";
        file_get_contents($url);
    }

    // Shared functions

    function ratingSort($playerOne, $playerTwo) {
        if ($playerOne["rating"] > $playerTwo["rating"]) {
            return -1;
        }
        else {
            return 1;
        }
    }

    function rankPlayers($players) {
        global $user;

        $playerRatings = array();

        // Default to the starting list in case we can't rank them
        $rankedPlayers = $players;

        // Create a list of all players in the list with their rating. Save the original "player" object inside.
        foreach ($players as $playerCode => $player) {
            if ($player[STAT_ID_MIN] > 0) {
              $totalLineValue = getFullLineStatRankingValue($player, $user["show_turnovers"]);
              $playerRatings[] = array(
                  "playerCode" => $playerCode,
                  "rating" => $totalLineValue,
                  "player" => $player,
              );
            }
        }

        if (count($playerRatings) > 0) {

            // Rank the players
            usort($playerRatings, "ratingSort");

            // Pull the original "player" object out of the rating object
            $rankedPlayers = array();
            foreach ($playerRatings as $playerRating) {
                $rankedPlayers[$playerRating["playerCode"]] = $playerRating["player"];
            }
        }

        return $rankedPlayers;
    }

    function printPlayersTable($players, $className = "", $showMatchInfo = true, $totalsLines = array()) {
        global $dateObject;
        global $user;
        global $matchesByTeam;
        global $userTeams;

        echo "<table class='table playersData " . $className . "'>";
        echo "<tr><th>Player</th>";
        printGameDataHeaders($user["show_turnovers"]);
        echo "</tr>";

        // Print the player lines

        foreach ($players as $player) {

            if (!isPlayerHiddenForToday($player['nba_id'])) {

                $gameTimeIndex = 0;

                $match = (isset($player["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player["TEAM_ABBREVIATION"])] : null);
                if ($match && isset($match["quarter"]) && isset($match["clock"])) {
                    $gameTimeIndex = getGameTimePercentage($match["quarter"], $match["clock"]);
                }

                $teamFilterString = (isset($player["teams"]) ? implode(",", array_intersect($player["teams"], array_keys($userTeams))) : "best");
                echo "<tr class='player " . (!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == "" ? "not-playing" : "") . "' data-teams='" . $teamFilterString . "'>" . PHP_EOL;

                $matchSummary = "";
                if ($match) {
                    $opponent = ($match["home"] ? "" : "@") . $match["opponent"];

                    if ($match["status"] == MATCH_STATUS_UPCOMING) {
                        $matchSummary = $opponent;
                    }
                    else if ($match["status"] == MATCH_STATUS_COMPLETE) {
                        $matchSummary = $opponent . " final";
                    }
                    else if ($match["status"] == MATCH_STATUS_IN_PROGRESS) {
                        $matchSummary = $opponent;

                        if ($match["quarter"] == 2 && trim($match["clock"]) == "") {
                            $matchSummary .= " half";
                        }
                        else {
                            $matchSummary .= " Q" . $match["quarter"] . " " . (trim($match["clock"]) && !($match["clock"] == "0.0") ? $match["clock"] : "end");
                        }
                    }
                }

                $totalLineValue = getFullLineStatRankingValue($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                $colorString = getCssColourStringForStatRankingValue($totalLineValue);

                $nameCellStyle = "";
                $nameCellClass = "";
                if (isset($match["status"]) && $match["status"] == MATCH_STATUS_UPCOMING && !isset($player[STAT_ID_MIN])) {
                    $nameCellClass = "match-upcoming";
                }
                else if ($showMatchInfo && isset($match["status"]) && $match["status"] == MATCH_STATUS_IN_PROGRESS) {
                    if ($user["show_colourful_view"] && $player[STAT_ID_MIN] > 0) {
                        $nameCellStyle = "background: linear-gradient(to right, " . $colorString . " " . $gameTimeIndex . "%, #ffffff " . $gameTimeIndex . "%);";
                        $nameCellClass = "match-in-progress custom-color";
                    }
                    else {
                        $nameCellStyle = "background: linear-gradient(to right, #bdbdbd " . $gameTimeIndex . "%, #ffffff " . $gameTimeIndex . "%);";
                        $nameCellClass = "match-in-progress default-color";
                    }
                }
                else if (isset($match["status"]) && ($match["status"] == MATCH_STATUS_COMPLETE || (!$showMatchInfo && $match["status"] == MATCH_STATUS_IN_PROGRESS) || (isset($player[STAT_ID_MIN]) && $player[STAT_ID_MIN] > 0))) {
                    if ($user["show_colourful_view"] && isset($player[STAT_ID_MIN]) && $player[STAT_ID_MIN] > 0) {
                        $nameCellStyle = "background-color: " . $colorString . "; color: black !important;";
                        $nameCellClass = "match-complete custom-color";
                    }
                    else {
                        $nameCellClass = "match-complete default-color";
                    }
                }

                if (isset($player["PLAYER_FIRST_NAME"]) && isset($player["PLAYER_LAST_NAME"])) {
                  $name = substr($player["PLAYER_FIRST_NAME"], 0, 1) . ". " . $player["PLAYER_LAST_NAME"];
                }
                else {
                  $nameParts = nameParts(isset($player["name"]) ? $player["name"] : nameLastCommaFirstToFirstLast($player["DISPLAY_LAST_COMMA_FIRST"]));
                  $name = substr($nameParts[0], 0, 1) . ". " . $nameParts[1];
                }

                echo "<td class='name " . $nameCellClass . "' style='" . $nameCellStyle . "'>";
                echo "<span class='name'>";
                echo "<a href='player.php?nba_id=" . (isset($player["PERSON_ID"]) ? $player["PERSON_ID"] : $player["nba_id"]) . "'>" . $name . "</a></span>";

                // Synced league data fields
                if (isset($player["leagueData"]) && count($player["leagueData"]) > 0) {
                    foreach ($player["leagueData"] as $teamKey => $leagueData) {
                        if ($leagueData["status"]) {
                            echo "<span class='health-status label label-danger' data-team-key='" . $teamKey . "'>" . $leagueData["status"] . "</span>";
                        }
                        if ($leagueData["isOnBench"]) {
                            echo "<span class='is-on-bench label label-warning' data-team-key='" . $teamKey . "'>BN</span>";
                        }
                    }
                }
                if (isset($player["isFAinAtLeastOneLeague"]) && $player["isFAinAtLeastOneLeague"]) {
                    echo "<span class='fa label label-primary'>FA</span>";
                }

                if ($showMatchInfo) {
                    echo " <span class='match-info'>";
                    echo $matchSummary . (isset($player["ON_COURT"]) && $player["ON_COURT"] ? " <img class='on-court' src='images/ball-icon-1.png' />" : "");
                    echo "</span>";

                    if ($user["show_match_scores"] && ($match["status"] == MATCH_STATUS_IN_PROGRESS || $match["status"] == MATCH_STATUS_COMPLETE)) {
                        echo " <span class='match-scores'>" . $match["score"] . "-" . $matchesByTeam[strtoupper($match["opponent"])]["score"] . "</span>";
                    }
                }
                else if ($player["ON_COURT"]) {
                    echo " <img class='on-court' src='images/ball-icon-1.png' />";
                }

                echo "</td>" . PHP_EOL;


                if (isset($player[STAT_ID_MIN]) && $player[STAT_ID_MIN] > 0) {
                    if ($user["show_colourful_view"]) {
                        printGameDataRowColourful($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                    }
                    else {
                        printGameDataRowSimple($player, $user["show_turnovers"], ($user["show_ranking_by_time"] ? $gameTimeIndex : null));
                    }
                }
                else if (isset($player["STATUS"]) && $player["STATUS"] != "") {
                    echo "<td class='data status' colspan='11'>" . $player["STATUS"] . "</td>" . PHP_EOL;
                }
                else if ($match["status"] == MATCH_STATUS_UPCOMING) {
                    $startTimeText = "soon";
                    if ($match["minutesUntilStart"]) {
                        if ($match["minutesUntilStart"] <= 0) {
                            $startTimeText = "imminently";
                        }
                        else if ($match["minutesUntilStart"] >= 60) {
                            $startTimeText = "in " . round($match["minutesUntilStart"] / 60) . "h";
                        }
                        else {
                            $startTimeText = "in " . $match["minutesUntilStart"] . "m";
                        }
                    }

                    echo "<td class='data status' colspan='11'>Game starting " . $startTimeText . "</td>" . PHP_EOL;
                }
                else if (!$match && !isset($player[STAT_ID_MIN])) {
                    echo "<td class='data status' colspan='11'>Team not playing today</td>" . PHP_EOL;
                }
                else if ((!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == 0) && $match["status"] == MATCH_STATUS_COMPLETE) {
                    echo "<td class='data status' colspan='11'>Never entered the game</td>" . PHP_EOL;
                }
                else if ((!isset($player[STAT_ID_MIN]) || $player[STAT_ID_MIN] == 0) && $match["status"] == MATCH_STATUS_IN_PROGRESS) {
                    echo "<td class='data status' colspan='11'>Yet to enter the game</td>" . PHP_EOL;
                }

                echo "</tr>" . PHP_EOL;
            }
        }

        // Print the totals lines

        foreach ($totalsLines as $totalsTeamId => $totalsLine) {
            echo "<tr class='totals' data-teams='" . $totalsTeamId . "'>" . PHP_EOL;

            $totalLineValue = getFullLineStatRankingValue($totalsLine, $user["show_turnovers"], null, $totalsLine[STAT_ID_GP]);
            $colorString = getCssColourStringForStatRankingValue($totalLineValue);

            $nameCellStyle = "";
            $nameCellClass = "";
            if ($user["show_colourful_view"] && isset($player[STAT_ID_MIN]) && $player[STAT_ID_MIN] > 0) {
                $nameCellStyle = "background-color: " . $colorString . "; color: black !important;";
                $nameCellClass = "match-complete custom-color";
            }
            else {
                $nameCellClass = "match-complete default-color";
            }

            echo "<td class='name " . $nameCellClass . "' style='" . $nameCellStyle . "'>";
            echo "<span class='name'>Total (" . $totalsLine[STAT_ID_GP] . " games)</span>";
            echo "</td>" . PHP_EOL;

            if ($user["show_colourful_view"]) {
                printGameDataRowColourful($totalsLine, $user["show_turnovers"], null, $totalsLine[STAT_ID_GP]);
            }
            else {
                printGameDataRowSimple($totalsLine, $user["show_turnovers"], null, $totalsLine[STAT_ID_GP]);
            }

            echo "</tr>" . PHP_EOL;
        }

        echo "</table>";
    }

    function playerSort($player1, $player2) {
        global $matchesByTeam;
        global $user;

        $player1match = (isset($player1["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player1["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player1["TEAM_ABBREVIATION"])] : null);
        $player2match = (isset($player2["TEAM_ABBREVIATION"]) && isset($matchesByTeam[strtoupper($player2["TEAM_ABBREVIATION"])]) ? $matchesByTeam[strtoupper($player2["TEAM_ABBREVIATION"])] : null);

        // Create a little indexing system to determine the order

        $player1matchValue = 0;
        if ($player1match) {
            if (isset($player1["STATUS"]) && $player1["STATUS"] != "") { // Injury
                $player1matchValue = 1;
            }
            else if ((!isset($player1[STAT_ID_MIN]) || $player1[STAT_ID_MIN] == 0) && $player1match["status"] == MATCH_STATUS_COMPLETE) {
                $player1matchValue = 2;
            }
            else if ((!isset($player1[STAT_ID_MIN]) || $player1[STAT_ID_MIN] == 0) && $player1match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player1matchValue = 1001;
            }
            else if ($player1match["status"] == MATCH_STATUS_IN_PROGRESS && $player1[STAT_ID_MIN] == 0) {
                $player1matchValue = 2001;
            }
            else if ($player1match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player1matchValue = 3000 + getGameTimePercentage($player1match["quarter"], $player1match["clock"]);
            }
            else if ($player1match["status"] == MATCH_STATUS_COMPLETE) {
                $player1matchValue = 2000;
            }
            else if ($player1match["status"] == MATCH_STATUS_UPCOMING) {
                $player1matchValue = 1000;
            }
        }

        $player2matchValue = 0;
        if ($player2match) {
            if (isset($player2["STATUS"]) && $player2["STATUS"] != "") { // Injury
                $player2matchValue = 1;
            }
            else if ((!isset($player2[STAT_ID_MIN]) || $player2[STAT_ID_MIN] == 0) && $player2match["status"] == MATCH_STATUS_COMPLETE) {
                $player2matchValue = 2;
            }
            else if ((!isset($player2[STAT_ID_MIN]) || $player2[STAT_ID_MIN] == 0) && $player2match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player2matchValue = 1001;
            }
            else if ($player2match["status"] == MATCH_STATUS_IN_PROGRESS && $player2[STAT_ID_MIN] == 0) {
                $player2matchValue = 2001;
            }
            else if ($player2match["status"] == MATCH_STATUS_IN_PROGRESS) {
                $player2matchValue = 3000 + getGameTimePercentage($player2match["quarter"], $player2match["clock"]);
            }
            else if ($player2match["status"] == MATCH_STATUS_COMPLETE) {
                $player2matchValue = 2000;
            }
            else if ($player2match["status"] == MATCH_STATUS_UPCOMING) {
                $player2matchValue = 1000;
            }
        }

        if ($player1matchValue == $player2matchValue) {
            // These players are equally ranked in terms of game progress - rank in terms of line value
            $player1TotalLineValue = getFullLineStatRankingValue($player1, $user["show_turnovers"], null);
            $player2TotalLineValue = getFullLineStatRankingValue($player2, $user["show_turnovers"], null);

            if ($player1TotalLineValue > $player2TotalLineValue) {
                return -1;
            }
            else if ($player1TotalLineValue < $player2TotalLineValue) {
                return 1;
            }
            else {
                // Line value is also identical - go alphabetical
                $player1NameParts = nameParts($player1["name"]);
                $player2NameParts = nameParts($player2["name"]);
                return strcmp($player1NameParts[1], $player2NameParts[1]);
            }
        }
        else if ($player1matchValue > $player2matchValue) {
            return -1;
        }
        else {
            return 1;
        }
    }

    $forceUpdateMode = (isset($_REQUEST["forceUpdate"]) && $_REQUEST["forceUpdate"] == "1");
    if ($forceUpdateMode) {
        require_once("dataUpdate.php");
    }

    // Global variables

    $numberOfBestPlayersToShow = 30;

    $user = getUser(getSessionParameter("user_id"));

    $userPlayers = getPlayersForCurrentUser(true);
    $userTeams = getTeamsForCurrentUser();
    $userSyncedLeagueData = getSyncedLeagueData($userTeams);

    if ($debugMode) {
        echo "userSyncedLeagueData:<br />";
        echo "<pre>";
        print_r($userSyncedLeagueData);
        echo "</pre>";
    }

    $bestPlayers = array();

    // Work out which date we should be looking at
    if (isset($_REQUEST["date"]) && strtotime($_REQUEST["date"]) > 0) {
        $dateObject = new DateTime($_REQUEST["date"]);
    }
    else {
        $dateObject = getEffectiveTodayDateObject();
    }

    //$dateObject = new DateTime("2016-10-10", new DateTimeZone('America/Los_Angeles'));

    // Get the game data

    $serviceGamesData = serviceRequestNbaGetGamesData($dateObject);
    $matchesByTeam = getMatchesByTeam($serviceGamesData);

    if ($debugMode) {
        echo "serviceGamesData:<br />";
        echo str_replace(PHP_EOL, "<br />", str_replace(" ", "&nbsp;", print_r($serviceGamesData, true)));
        echo "matchesByTeam:<br />";
        echo str_replace(PHP_EOL, "<br />", str_replace(" ", "&nbsp;", print_r($matchesByTeam, true)));
    }

    $currentGames = false;
    foreach ($matchesByTeam as $match) {
        if ($match["status"] == MATCH_STATUS_IN_PROGRESS) {
            $currentGames = true;
        }
    }

    // Get the player data

    $errorLoadingGameData = false;
    $servicePlayerList = serviceRequestNbaGetPlayerListById();
    $servicePlayerDayData = array();
    $allPlayersByCode = array();

    if ($serviceGamesData && isset($serviceGamesData["games"])) {
        $servicePlayerDayData = serviceRequestNbaGetPlayerData($dateObject, array_keys($serviceGamesData["games"]));

        if ($debugMode) {
            echo "servicePlayerDayData:<br />";
            echo str_replace(PHP_EOL, "<br />", str_replace(" ", "&nbsp;", print_r($servicePlayerDayData, true)));
        }

        // Add the NBA stats data to the user player record, if they exist
        // Also add the league sync data, if relevant

        $combinedPlayers = array();
        foreach ($userPlayers as $player) {
            $newPlayer = $player;

            // Add the player details
            if (isset($servicePlayerList[$player["nba_id"]])) {

                $newPlayer = $newPlayer + $servicePlayerList[$player["nba_id"]];

                // Now that we have the player code, add the day's stats (if they exist)
                if (isset($servicePlayerDayData[$player["nba_id"]])) {
                    //$newPlayer = $newPlayer + $servicePlayerDayData[$player["nba_id"]];
                    $newPlayer = array_merge($newPlayer, $servicePlayerDayData[$player["nba_id"]]);
                }
            }

            // Add the league data
            $newPlayer["leagueData"] = array();
            foreach ($userSyncedLeagueData as $teamKey => $leagueData) {
                $userTeamPlayer = $leagueData["userTeamPlayers"][$player["nba_id"]];
                if ($userTeamPlayer) {
                    $newPlayer["leagueData"][$teamKey] = array(
                        "status" => $userTeamPlayer["status"],
                        "isOnBench" => $userTeamPlayer["isOnBench"],
                    );
                }
            }

            $combinedPlayers[] = $newPlayer;
        }
        $userPlayers = $combinedPlayers;

        if ($debugMode) {
            echo "userPlayers:<br />";
            echo "<pre>";
            print_r($userPlayers);
            echo "</pre>";
        }

        // Determine the totals for each team of owned players

        $teamTotals = array();
        $statsConfig = getStatsConfig($user["show_turnovers"]);
        foreach ($userPlayers as $player) {
            $playerTeams = array_intersect($player["teams"], array_keys($userTeams));

            // If they are in at least one team and played today
            if ($player[STAT_ID_MIN] > 0 && count($playerTeams)) {

                // Add another "team line" - one to cover all owned players
                $playerTeams[] = "owned";

                // Go through each team for this player and each of their stats to the list
                foreach ($playerTeams as $playerTeam) {
                    $playerShouldCountToTeamTotals = true;

                    $userTeam = $userTeams[$playerTeam];
                    if ($userTeam) {
                        if ($userTeam["yahoo_team_key"]) {
                          if ($player["leagueData"][$userTeam["yahoo_team_key"]]) {
                              if ($player["leagueData"][$userTeam["yahoo_team_key"]]["isOnBench"]) {
                                  $playerShouldCountToTeamTotals = false;
                              }
                          }
                        }
                    }

                    if ($playerShouldCountToTeamTotals) {
                        if (!isset($teamTotals[$playerTeam])) {
                            $teamTotals[$playerTeam] = array(
                                STAT_ID_GP => 0
                            );
                        }

                        $teamTotals[$playerTeam][STAT_ID_GP]++;

                        foreach ($statsConfig as $statId => $statConfig) {
                            if (!isset($teamTotals[$playerTeam][$statId])) {
                                if ($statConfig["type"] == "percentage") {
                                    $teamTotals[$playerTeam][$statConfig["madeTotalId"]] = 0;
                                    $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]] = 0;
                                }
                                $teamTotals[$playerTeam][$statId] = 0;
                            }

                            if ($statConfig["type"] == "percentage") {
                                $teamTotals[$playerTeam][$statConfig["madeTotalId"]] += $player[$statConfig["madeTotalId"]];
                                $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]] += $player[$statConfig["attemptedTotalId"]];

                                if ($teamTotals[$playerTeam][$statConfig["attemptedTotalId"]] == 0) {
                                    $teamTotals[$playerTeam][$statId] = 0;
                                }
                                else {
                                    $teamTotals[$playerTeam][$statId] = $teamTotals[$playerTeam][$statConfig["madeTotalId"]] / $teamTotals[$playerTeam][$statConfig["attemptedTotalId"]];
                                }
                            }
                            else {
                                $teamTotals[$playerTeam][$statId] += $player[$statId];
                            }
                        }
                    }
                }
            }
        }


        // Extract the best {$numberOfBestPlayersToShow} performanes, based on overall line values
        // While games are in progress, the "best" tab order probably won't match the order of the colouring of those lines.
        // This is because the "best" ranking is based on the line as of right now, with no weighting for how long the game
        // has been in progress. If we wanted to add this, we could add the game time index to the getFullLineStatRankingValue()
        // call below. But it's probably better to show the "best" list based on final line value, otherwise when a player
        // scores a 3 in the first minute of the game it will show them as crazy good on a per-minute weighted rating.

        $allPlayersRanked = rankPlayers($servicePlayerDayData);
        $allPlayersByCode = serviceRequestNbaGetPlayerListById();

        foreach (array_slice($allPlayersRanked, 0, $numberOfBestPlayersToShow, true) as $playerCode => $player) {
            if (isset($allPlayersByCode[$playerCode])) {
                $combinedPlayer = $player + $allPlayersByCode[$playerCode];

                // Add the league data
                $combinedPlayer["leagueFAdata"] = array();
                $combinedPlayer["isFAinAtLeastOneLeague"] = false;
                foreach ($userSyncedLeagueData as $teamKey => $leagueData) {
                    $combinedPlayer["leagueFAdata"][$teamKey] = !in_array($playerCode, $leagueData["ownedPlayers"]);
                    if (!in_array($playerCode, $leagueData["ownedPlayers"])) {
                        $combinedPlayer["isFAinAtLeastOneLeague"] = true;
                    }
                }

                $bestPlayers[] = $combinedPlayer;
            }
        }
    }
    else {
        $errorLoadingGameData = true;
    }

    printHeader();
?>
    <script type="text/javascript">

        $(document).ready(function() {

            // Cookie functions from http://www.quirksmode.org/js/cookies.html
            function createCookie(name,value,days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                }
                else var expires = "";
                document.cookie = name+"="+value+expires+"; path=/";
            }
            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
            function eraseCookie(name) {
                createCookie(name,"",-1);
            }

            // Manage filters

            var teamFilterCookieName = "teamFilter";
            var teamFilterTeamIdCookieName = "teamFilterTeamId";

            // Players tab filter
            $("#tab-players div.team-filter button").click(function() {
                var id = $(this).attr("id");

                // Save the cookie
                createCookie(teamFilterCookieName, id, 7);
                if (id == "team-filter-specific") {
                    createCookie(teamFilterTeamIdCookieName, $(this).attr("data-team"), 7);
                }

                // Update the button states
                $("#tab-players div.team-filter button").removeClass("btn-primary");
                $("#tab-players div.team-filter button").addClass("btn-default");
                $(this).addClass("btn-primary");

                // Re-hide all players & totals, and then show depending on the filter chosen

                $("table.playersData.main tr.player").hide();
                $("table.playersData.main tr.totals").hide();

                if (id == "team-filter-best") {
                    $("table.playersData.main tr.player[data-teams = 'best']").show();

                    $("div.sync-options").hide();
                }
                else if (id == "team-filter-owned") {
                    $("table.playersData.main tr.player[data-teams != ''][data-teams != 'best']").show();
                    $("table.playersData.main tr.totals[data-teams = 'owned']").show();

                    var yahooTeamKey = $(this).attr("data-yahoo");
                    if (yahooTeamKey) {
                        $("div.sync-options").show();
                        $("#league-sync").attr('href', 'leagueSync.php?teamKey=' + yahooTeamKey);
                    }
                    else {
                        $("div.sync-options").hide();
                    }
                }
                else if (id == "team-filter-watch") {
                    $("table.playersData.main tr.player[data-teams = '']").show();
                }
                else if (id == "team-filter-specific") {
                    var teamId = $(this).attr("data-team");
                    var yahooTeamKey = $(this).attr("data-yahoo");

                    // We have to loop through players manually. Load up all the players without a blank team list.
                    $("table.playersData.main tr.player[data-teams != '']").each(function() {
                        var teams = $(this).attr("data-teams");
                        var teamsArray = teams.split(",");
                        for (var i = 0; i < teamsArray.length; i++) {
                            if (teamsArray[i] == teamId) {
                                $(this).show();
                            }
                        }
                    });

                    // Team totals lines are marked individually
                    $("table.playersData.main tr.totals[data-teams = '" + teamId + "']").show();

                    if (yahooTeamKey) {
                        $("div.sync-options").show();
                        $("#league-sync").attr('href', 'leagueSync.php?teamKey=' + yahooTeamKey);
                    }
                    else {
                        $("div.sync-options").hide();
                    }
                }
                else { // "all", being "all my list", including owned and watch. Show everything except "best".
                    $("table.playersData.main tr.player[data-teams != 'best']").show();
                }
            });

            // News tab filter
            $("#tab-news div.team-filter button").click(function() {
                var id = $(this).attr("id");

                // Save the cookie
                createCookie(teamFilterCookieName, id, 7);
                if (id == "team-filter-specific") {
                    createCookie(teamFilterTeamIdCookieName, $(this).attr("data-team"), 7);
                }

                // Update the button states
                $("#tab-news div.team-filter button").removeClass("btn-primary");
                $("#tab-news div.team-filter button").addClass("btn-default");
                $(this).addClass("btn-primary");

                var showRowsForPlayers = function(players) {
                    $("table.playersNews tr").each(function(index, element) {
                        for (var i = 0; i < players.length; i++) {
                            if (element.innerHTML.indexOf(players[i]) !== -1) {
                                $(this).show();
                            }
                        }
                    });
                };

                // Re-hide all players, and then show depending on the filter chosen
                $("table.playersNews tr").hide();

                if (id == "team-filter-all") { // "all", being "all my list", including owned and watch
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            echo $sep . "\"" . $player["name"] . "\"";
                            $sep = ", ";
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-owned") {
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            if (count($player["teams"]) > 0) {
                                echo $sep . "\"" . $player["name"] . "\"";
                                $sep = ", ";
                            }
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-watch") {
                    var players = [<?php
                        $sep = "";
                        foreach ($userPlayers as $player) {
                            if (count($player["teams"]) == 0) {
                                echo $sep . "\"" . $player["name"] . "\"";
                                $sep = ", ";
                            }
                        }
                    ?>];
                    showRowsForPlayers(players);
                }
                else if (id == "team-filter-specific") {
                    var teamId = $(this).attr("data-team");

                    var players = [];
                    <?php
                        $teamIds = array();
                        foreach ($userTeams as $team) {
                            echo "players[" . $team["id"] . "] = [];" . PHP_EOL;
                            $teamIds[] = $team["id"];
                        }

                        foreach ($userPlayers as $player) {
                            foreach ($player["teams"] as $teamId) {
                                // Protect against orphaned player_team records that below to deleted teams
                                if (in_array($teamId, $teamIds)) {
                                    echo "players[" . $teamId . "].push(\"" . $player["name"] . "\");" . PHP_EOL;
                                }
                            }
                        }
                    ?>

                    showRowsForPlayers(players[teamId]);
                }
                else {
                    // Entire league news
                    $("table.playersNews tr").show();
                }
            });

            // Handle expand/collapse of games
            $("#tab-games div.match").each(function(index, match) {
                $("div.match-summary", match).click(function() {
                    if ($("div.match-players", match).is(':visible')) {
                        // Hide the player list for this match if it's visible
                        $("div.match-players", match).hide();
                    }
                    else {
                        // Hide all of the player lists
                        $("#tab-games div.match-players").hide();
                        // Show the player list for this match
                        $("div.match-players", match).show();
                        // Scroll to the top of the game selected
                        $("html, body").animate({
                            scrollTop: $(this).offset().top
                        }, 500);
                    }
                });
            });

            // Handle expand/collapse of league teams
            $("#tab-leagues div.league-team").each(function(index, leagueTeam) {
                $("div.league-team-summary", leagueTeam).click(function() {
                    if ($("div.league-team-players", leagueTeam).is(':visible')) {
                        // Hide the player list for this league team if it's visible
                        $("div.league-team-players", leagueTeam).hide();
                    }
                    else {
                        // Hide all of the player lists
                        $("#tab-leagues div.league-team-players").hide();
                        // Show the player list for this match
                        $("div.league-team-players", leagueTeam).show();
                        // Scroll to the top of the game selected
                        $("html, body").animate({
                            scrollTop: $(this).offset().top
                        }, 500);
                    }
                });
            });

            // Set the default filters if the cookie exists, otherwise default to "owned"
            var teamFilterCookieValue = readCookie(teamFilterCookieName);
            if (teamFilterCookieValue == null) {
                teamFilterCookieValue = "team-filter-owned";
            }

            if (teamFilterCookieValue == "team-filter-specific") {
                var teamFilterTeamIdCookieValue = readCookie(teamFilterTeamIdCookieName);
                if (teamFilterTeamIdCookieValue != null) {
                    $("div.team-filter button[data-team = '" + teamFilterTeamIdCookieValue + "']").click();
                }
            }
            else {
                $("div.team-filter button#" + teamFilterCookieValue).click();
            }

            // Expand/collapse controls handling

            var collapseControlsCookieName = "collapseControls";

            $("#controls-expand-collapse").click(function() {
                if ($(".controls-collapsible").first().is(":visible")) {
                    $(".controls-collapsible").hide();
                    $("#controls-expand-collapse span")
                        .removeClass("glyphicon-resize-small")
                        .addClass("glyphicon-resize-full");
                    createCookie(collapseControlsCookieName, 1, 7);
                }
                else {
                    $("div.controls-collapsible").show();
                    $("#controls-expand-collapse span")
                        .removeClass("glyphicon-resize-full")
                        .addClass("glyphicon-resize-small");
                    createCookie(collapseControlsCookieName, 0, 7);
                }
            });

            var collapseControlsCookieValue = readCookie(collapseControlsCookieName);
            if (collapseControlsCookieValue == null) {
                collapseControlsCookieValue = 1;
            }

            if (collapseControlsCookieValue == 0) {
                $("div.controls-collapsible").show();
                $("#controls-expand-collapse span")
                    .removeClass("glyphicon-resize-full")
                    .addClass("glyphicon-resize-small");
            }

            // Auto-refresh handling

            (function() {

                function startTimer(refreshTime) {
                    var totalTimeElapsed = 0;

                    $(".auto-refresh .countdown").html(refreshTime);

                    var handle = setInterval(function() {
                        // Only increment the timer if the players tab is showing
                        if ($('#tab-players').is(":visible")) {
                            totalTimeElapsed++;

                            if ($(".auto-refresh input").prop('checked')) {
                                if (totalTimeElapsed >= refreshTime) {
                                    window.location.href = window.location.href;
                                    clearInterval(handle);
                                }
                                else {
                                    $(".auto-refresh .countdown").html(refreshTime - totalTimeElapsed);
                                }
                            }
                            else {
                                clearInterval(handle);
                            }
                        }
                    }, 1000);
                }

                $("#refresh").click(function() {
                    window.location.href = window.location.href;
                });

                var autoRefreshCookieName = "autoRefresh";

                var refreshTime = 60;
                $(".auto-refresh .countdown").html(refreshTime);

                var cookieValue = readCookie(autoRefreshCookieName);
                if (cookieValue == null) {
                    cookieValue = 0;
                }

                // Don't set the auto-refresh if there's no games in progress, but don't affect the cookie - it will take
                // effect again as soon as there are games in progress.
                <?php if ($currentGames) { ?>
                    if (cookieValue == 1) {
                        $(".auto-refresh input").prop('checked', true);
                        $(".auto-refresh .question-mark").hide();
                        startTimer(refreshTime);
                    }
                <?php } ?>

                $(".auto-refresh input").change(function() {
                    if ($(".auto-refresh input").prop('checked')) {
                        $(".auto-refresh .question-mark").hide();
                        createCookie(autoRefreshCookieName, 1, 7);
                        startTimer(refreshTime);
                    }
                    else {
                        createCookie(autoRefreshCookieName, 0, 7);
                        $(".auto-refresh .countdown").html(refreshTime);
                        $(".auto-refresh .question-mark").show();
                    }
                });

            })();

            // Check the cookie for the colourful view message
            /*if (readCookie("dismissedColourfulViewIntroMessage") != 1) {
                $(".colourfulViewIntroMessage").show();
                $(".colourfulViewIntroMessage button").click(function() {
                    createCookie("dismissedColourfulViewIntroMessage", 1, 30);
                });
            }*/

            // Check the cookie for the colourful view message
            /*if (readCookie("dismissedNewSeasonMessage") != 1) {
                $(".newSeasonMessage").show();
                $(".newSeasonMessage button").click(function() {
                    createCookie("dismissedNewSeasonMessage", 1, 365);
                });
            }*/

            // Set up the tabs
            var defaultTabCookieName = "defaultTab";

            $('#home-tabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            });
            $('#home-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                createCookie(defaultTabCookieName, e.target.hash, 7);
            });

            // Preset the tab selection based on the cookie
            var defaultTabCookieValue = readCookie(defaultTabCookieName);
            if (defaultTabCookieValue != null) {
                $('#home-tabs a[href="' + defaultTabCookieValue + '"]').tab('show');
            }

        });

    </script>

    <?php if (isset($_REQUEST["userCreated"]) && $_REQUEST["userCreated"] == 1): ?>
        <div class="alert alert-success alert-dismissible currentGamesAlert" role="alert">
            Your user account has been created!
        </div>
    <?php endif; ?>

    <?php if (count($userTeams) == 0 && count($userPlayers) == 0): ?>

        <h1 class="title-home clear">Daily dashboard</h1>

        <p>Welcome! This is your <strong>Flance</strong> dashboard where you will keep track of all your players&apos; daily stats.</p>
        <p>But before we can do that, we have to configure your fantasy teams. Go to <a href="editTeams.php">Edit my teams</a> to get started.</p>

    <?php elseif (count($userPlayers) == 0): ?>

        <h1 class="title-home clear">Daily dashboard</h1>

        <p>This is your <strong>Flance</strong> dashboard where you will keep track of your team&s daily stats.</p>
        <p>You&apos;ve set up your teams, so the last step is to set up the list of players you want to keep track of.
            Go to <a href="searchPlayers.php">Find a teams</a> to start adding players to your list.</p>

    <?php else: ?>

        <!-- Temporary message for start of a new season -->
        <div class="alert alert-info alert-dismissible newSeasonMessage" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Welcome to fantasy season 2016-17! To clean your <strong>Flance</strong> profile for your new teams, you can delete your
            old player list from the <a href="editPlayers.php">Edit players page</a>, and delete and set up your new teams on the
            <a href="editTeams.php">Edit teams page</a>. Good luck!
        </div>

        <?php
            if ($errorLoadingGameData) {
                echo '<div class="alert alert-danger alert-dismissible currentGamesAlert" role="alert">There was an error loading game data. Please try again later.</div>';
            }
            else {
                // Show the "no games in progress" message only if the user is looking at today
                $todayDateObject = getEffectiveTodayDateObject();
                if (!$currentGames && $todayDateObject->format("Y-m-d") == $dateObject->format("Y-m-d")) {
                    echo '<div class="alert alert-info alert-dismissible currentGamesAlert" role="alert">No games are in progress. Please check again later.</div>';
                }
            }
        ?>

        <h1 class="title-home">Daily dashboard</h1>

        <div>

            <!-- Nav tabs -->
            <div class="row" id="home-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-players" aria-controls="tab-players" role="tab" data-toggle="tab">Players</a></li>
                    <li role="presentation"><a href="#tab-games" aria-controls="tab-games" role="tab" data-toggle="tab">Games</a></li>
                    <li role="presentation"><a href="#tab-news" aria-controls="tab-news" role="tab" data-toggle="tab">News</a></li>
                    <?php if (count($userSyncedLeagueData) > 0) { ?>
                        <li role="presentation"><a href="#tab-leagues" aria-controls="tab-leagues" role="tab" data-toggle="tab">Leagues</a></li>
                    <?php } ?>
                </ul>
            </div>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="tab-players">

                    <div class="list-controls">
                        <div class="btn-group btn-group-sm team-filter" role="group" aria-label="Team filter">
                            <button type="button" class="btn btn-primary" id="team-filter-all">My list</button>
                            <button type="button" class="btn btn-default" id="team-filter-owned" data-yahoo="<?php if (count($userTeams) == 1 && $userTeams[0]["yahoo_team_key"]) echo $team["yahoo_team_key"]; ?>">Owned</button>
                            <?php
                                if (count($userTeams) > 1) {
                                    foreach ($userTeams as $team) {
                                        ?>
                                        <button type="button" class="btn btn-default" id="team-filter-specific" data-team="<?php echo $team["id"]; ?>" data-yahoo="<?php echo $team["yahoo_team_key"]; ?>"><?php echo ($team["short_name"] ? $team["short_name"] : strtoupper(substr($team["name"], 0, 4))); ?></button>
                                        <?php
                                    }
                                }
                            ?>
                            <button type="button" class="btn btn-default" id="team-filter-watch">Watch</button>
                            <?php if (count($bestPlayers) > 0): ?>
                                <button type="button" class="btn btn-default" id="team-filter-best">Best</button>
                            <?php endif; ?>
                        </div>

                        <button type="button" class="btn btn-sm btn-success" id="controls-expand-collapse">
                            <span class="glyphicon glyphicon-resize-full" aria-hidden="true"></span>
                        </button>
                    </div>

                    <div class="list-controls controls-collapsible">
                        <div class="btn-group btn-group-sm date-options" role="group" aria-label="Date options">
                            <?php
                                $dateObjectToIterate = getEffectiveTodayDateObject();
                                $dateObjectToIterate->sub(new DateInterval("P3D"));

                                for ($i = 0; $i < 3; $i++) {
                                    $isSelected = ($dateObjectToIterate->format("Y-m-d") == $dateObject->format("Y-m-d"));
                                    echo "<a href='home.php?date=" . $dateObjectToIterate->format("Y-m-d") . "' class='btn " . ($isSelected ? "btn-primary" : "btn-default") . "' id='date-" . $dateObjectToIterate->format("Y-m-d") . "''>" . $dateObjectToIterate->format("m/d") . "</a>" . PHP_EOL;
                                    $dateObjectToIterate->add(new DateInterval("P1D"));
                                }

                                // The last one will be today - this is also the default
                                $isSelected = ($dateObjectToIterate->format("Y-m-d") == $dateObject->format("Y-m-d"));
                                echo "<a href='home.php?date=" . $dateObjectToIterate->format("Y-m-d") . "' class='btn " . ($isSelected ? "btn-primary" : "btn-default") . "' id='date-" . $dateObjectToIterate->format("Y-m-d") . "''>" . $dateObjectToIterate->format("m/d") . "</a>" . PHP_EOL;
                            ?>
                        </div>
                    </div>

                    <div class="list-controls controls-collapsible">
                        <div class="refresh-options">
                            <button type="button" class="btn btn-primary btn-sm" id="refresh">Refresh</button>
                            <span class="auto-refresh"><input type="checkbox" /> Auto-refresh in <span class="countdown"></span> seconds<span class="question-mark">?</span></span>
                        </div>
                    </div>

                    <div class="row">
                    <?php
                        usort($userPlayers, "playerSort");
                        printPlayersTable(array_merge($userPlayers, $bestPlayers), "main", true, $teamTotals);
                    ?>
                    </div>

                    <div class="row sync-options" style="display: hidden; text-align: center; padding: 10px;">
                        <a href="leagueSync.php?teamKey=" id="league-sync" style="">
                          <button type="button" class="btn btn-primary btn-sm">Update data for this league from Yahoo</button>
                        </a>
                    </div>

            </div>

            <div role="tabpanel" class="tab-pane" id="tab-news">

                <div class="btn-group btn-group-sm team-filter" role="group" aria-label="Team filter">
                    <button type="button" class="btn btn-primary" id="team-filter-all">My list</button>
                    <button type="button" class="btn btn-default" id="team-filter-owned">Owned</button>
                    <?php
                        if (count($userTeams) > 1) {
                            foreach ($userTeams as $team) {
                                ?>
                                <button type="button" class="btn btn-default" id="team-filter-specific" data-team="<?php echo $team["id"]; ?>"><?php echo ($team["short_name"] ? $team["short_name"] : strtoupper(substr($team["name"], 0, 4))); ?></button>
                                <?php
                            }
                        }
                    ?>
                    <button type="button" class="btn btn-default" id="team-filter-watch">Watch</button>
                    <button type="button" class="btn btn-default" id="team-filter-league">All</button>
                </div>

                <div class="row">
                    <table class="table playersNews">
                        <?php
                            foreach (getRotoworldPlayerNewsFeed() as $newsItem) {
                                echo "<tr>" . PHP_EOL;
                                echo "<td>" . PHP_EOL;
                                echo "<span class='title'>" . $newsItem["title"] . "</span>" . PHP_EOL;
                                echo "<span class='description'>" . $newsItem["description"] . "</span>" . PHP_EOL;
                                echo "<span class='link'><a href='" . $newsItem["link"] . "' target='blank'>More</a></span>" . PHP_EOL;
                                echo "</td>" . PHP_EOL;
                                echo "</tr>" . PHP_EOL;
                            }
                        ?>
                    </table>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane" id="tab-games">
                <div class="row">
                    <?php
                        $matches = array();
                        foreach ($matchesByTeam as $teamCode => $matchForTeam) {
                            if ($matchForTeam["home"]) {
                                $match = array_merge($matchForTeam, array(
                                    "home" => $teamCode,
                                    "away" => $matchForTeam["opponent"],
                                    "homeScore" => $matchForTeam["score"],
                                    "awayScore" => $matchesByTeam[strtoupper($matchForTeam["opponent"])]["score"],
                                ));

                                $match["homePlayers"] = array();
                                $match["awayPlayers"] = array();

                                // Go through every player we know of
                                foreach ($allPlayersByCode as $playerCode => $player) {
                                    // Do we have some game data for this player?
                                    if (isset($servicePlayerDayData[$playerCode])) {
                                        if ($servicePlayerDayData[$playerCode][STAT_ID_MIN] > 0) {

                                            $combinedPlayer = array_merge($player, $servicePlayerDayData[$playerCode]);

                                            // Add the league data
                                            $combinedPlayer["leagueFAdata"] = array();
                                            $combinedPlayer["isFAinAtLeastOneLeague"] = false;
                                            foreach ($userSyncedLeagueData as $teamKey => $leagueData) {
                                                $combinedPlayer["leagueFAdata"][$teamKey] = !in_array($playerCode, $leagueData["ownedPlayers"]);
                                                if (!in_array($playerCode, $leagueData["ownedPlayers"])) {
                                                    $combinedPlayer["isFAinAtLeastOneLeague"] = true;
                                                }
                                            }

                                            // Do they play for one of the two teams in this game? If so, combine the player + game data.
                                            if ($player["TEAM_ABBREVIATION"] == $teamCode) {
                                                $match["homePlayers"][] = $combinedPlayer;
                                            }
                                            else if ($player["TEAM_ABBREVIATION"] == $matchForTeam["opponent"]) {
                                                $match["awayPlayers"][] = $combinedPlayer;
                                            }
                                        }
                                    }
                                }

                                // Ensure the players show in rank order
                                $match["homePlayers"] = rankPlayers($match["homePlayers"]);
                                $match["awayPlayers"] = rankPlayers($match["awayPlayers"]);

                                $matches[] = $match;
                            }
                        }

                        foreach ($matches as $match) {
                            echo "<div class='match'>" . PHP_EOL;

                                $awayImageUrl = getTeamImageUrl($match["away"]);
                                $homeImageUrl = getTeamImageUrl($match["home"]);

                                $matchTime = "";
                                $matchTimeClass = "singleLine";
                                $hasScores = true;

                                if ($match["status"] == MATCH_STATUS_UPCOMING) {
                                    if ($match["minutesUntilStart"]) {
                                        if ($match["minutesUntilStart"] < -30) {
                                            $matchTime = "Delayed";
                                        }
                                        else if ($match["minutesUntilStart"] <= 0) {
                                            $matchTime = "Now";
                                        }
                                        else if ($match["minutesUntilStart"] >= 60) {
                                            $matchTime = "in " . round($match["minutesUntilStart"] / 60) . "h";
                                        }
                                        else {
                                            $matchTime = "in " . $match["minutesUntilStart"] . "m";
                                        }
                                    }
                                    else {
                                        $matchTime = "Upcoming";
                                    }
                                    $hasScores = false;
                                }
                                else if ($match["status"] == MATCH_STATUS_COMPLETE) {
                                    $matchTime = "Final";
                                }
                                else if ($match["status"] == MATCH_STATUS_IN_PROGRESS) {
                                    if ($match["quarter"] == 2 && trim($match["clock"]) == "") {
                                        $matchTime = "Half";
                                    }
                                    else {
                                        $matchTime = "Q" . $match["quarter"] . "<br />" . (trim($match["clock"]) && !($match["clock"] == "0.0") ? $match["clock"] : "end");
                                        $matchTimeClass = "twoLine";
                                    }
                                }

                                echo "<div class='match-summary'>" . PHP_EOL;
                                    echo "<div class='team away'>" . PHP_EOL;
                                        echo "<div class='logo'><img src='" . $awayImageUrl . "' /></div>";
                                        echo "<div class='nameAndScore'>" . PHP_EOL;
                                            echo "<span class='name'>" . $match["away"] . "</span>";
                                            echo "<span class='score'>" . ($hasScores && $user["show_match_scores"] ? $match["awayScore"] : "-") . "</span>";
                                        echo "</div>" . PHP_EOL;
                                    echo "</div>" . PHP_EOL;

                                    echo "<div class='time " . $matchTimeClass . "'>" . $matchTime . "</div>";

                                    echo "<div class='team home'>" . PHP_EOL;
                                        echo "<div class='nameAndScore'>" . PHP_EOL;
                                            echo "<span class='name'>" . $match["home"] . "</span>";
                                            echo "<span class='score'>" . ($hasScores && $user["show_match_scores"] ? $match["homeScore"] : "-") . "</span>";
                                        echo "</div>" . PHP_EOL;
                                        echo "<div class='logo'><img src='" . $homeImageUrl . "' /></div>";
                                    echo "</div>" . PHP_EOL;

                                echo "</div>" . PHP_EOL;

                                echo "<div class='match-players' style='display: none;'>" . PHP_EOL;
                                    echo "<h1>" . $match["away"] . "</h1>" . PHP_EOL;
                                    printPlayersTable($match["awayPlayers"], "game-team", false);

                                    echo "<h1>" . $match["home"] . "</h1>" . PHP_EOL;
                                    printPlayersTable($match["homePlayers"], "game-team", false);
                                echo "</div>" . PHP_EOL;

                            echo "</div>" . PHP_EOL;
                        }
                    ?>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="tab-leagues">
                <div class="row">
                    <?php
                        foreach ($userSyncedLeagueData as $userTeamKey => $leagueData) {
                            foreach ($leagueData["otherTeams"] as $leagueTeamName => $leagueTeamPlayers) {
                                echo "<div class='league-team'>" . PHP_EOL;

                                    $players = array();
                                    $totalsLine = array(); // TODO

                                    foreach ($leagueTeamPlayers as $leagueTeamPlayerId) {
                                        $player = array("nba_id" => $leagueTeamPlayerId);
                                        if (isset($servicePlayerList[$leagueTeamPlayerId])) {
                                            $player = array_merge($player, $servicePlayerList[$leagueTeamPlayerId]);
                                            if (isset($servicePlayerDayData[$leagueTeamPlayerId])) {
                                                $player = array_merge($player, $servicePlayerDayData[$leagueTeamPlayerId]);
                                            }
                                            $players[] = $player;
                                        }

                                        // TODO add to totals
                                    }

                                    //$players = rankPlayers($players);
                                    usort($players, "playerSort");

                                    echo "<div class='league-team-summary'>" . PHP_EOL;
                                        echo "<h1 class='league-team-name'>" . $leagueTeamName . "</h1>" . PHP_EOL;
                                    echo "</div>" . PHP_EOL;

                                    echo "<div class='league-team-players' style='display: none;'>" . PHP_EOL;
                                        printPlayersTable($players, "league-team", true, $totalsLine);
                                    echo "</div>" . PHP_EOL;

                                echo "</div>" . PHP_EOL;
                            }
                        }
                    ?>
                </div>
            </div>

        </div>

    <?php endif; ?>

<?php
    printFooter();
