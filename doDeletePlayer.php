<?php

    require_once("common.php");

    if (!isset($_REQUEST["id"])) {
        header("Location: editPlayers.php");
        exit;
    }

    deletePlayer($_REQUEST["id"]);

    header("Location: editPlayers.php?deleted=1");
