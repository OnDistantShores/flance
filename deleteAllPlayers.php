<?php

    require_once("common.php");

    printHeader();
?>

    <h1>Delete entire player list</h1>

    <p>This will delete all players on your list, including those on your teams and your watch list.</p>
    <p><strong>This cannot be undone! Are you sure?</strong></p>

    <p><a href='editPlayers.php'>No, take me back</a></p>
    <p><a href='doDeleteAllPlayers.php'><button type="submit" class="btn btn-danger">Yes, I'm sure - delete all my players</button></a></p>

    <p class="breadcrumbs"><a href="editPlayers.php">&lt; Back</a></p>

<?php
    printFooter();
